import React from 'react';
import {
    TouchableOpacity, View,
    Image, Text,
    StyleSheet
} from 'react-native'

import * as Helpers from '../../helpers/Exporter'
import { WP as wp } from '../../helpers/Exporter'


export const Header = (props) => {
    return (
        <View
            style={[styles.container, Helpers.GLOBAL_SHEET.boxWithShadow]}>
            <TouchableOpacity
                onPress={() => props.navigation.goBack()}
                style={styles.backBtn}>
                <Image
                    source={Helpers.Images.backArrow}
                    style={styles.backBtn_Img}
                />
            </TouchableOpacity>
            <Text style={[styles.titleTxt, Helpers.Typography.four]}>{props.title}</Text>


            {
                props.rightsection != undefined ? [
                    <View style={styles.rightSec}>
                        {props.rightsection}
                    </View>
                ] : [

                    ]
            }

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: wp(12), width: '100%', flexDirection: 'row',
        backgroundColor: Helpers.Theme.light,
        alignItems: 'center',
        paddingHorizontal: wp(5)
    },

    backBtn: {
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'
    },
    backBtn_Img: {
        height: wp(6), width: wp(6), resizeMode: 'contain'

    },

    titleTxt: { marginLeft: wp(2), fontWeight: Helpers.Typography.weight600 },

    rightSec: {
        position: 'absolute', right: wp(5)
    }

})
