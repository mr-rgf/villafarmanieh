import React from 'react';
import {
    TouchableOpacity,
    Image
} from 'react-native'

import * as Helpers from '../../helpers/Exporter'
import { styles } from '../../modules/search/Mapviewlist.style'


export const BackButtonRound = (props) => {
    return (
        <TouchableOpacity
            onPress={() => props.navigation.goBack()}
            style={[styles.header_backBtn, Helpers.GLOBAL_SHEET.boxWithShadow]}
        >
            <Image
                source={Helpers.Images.backArrow}
                style={[styles.header_backImg,{transform: [{ rotate: '180deg' }]}]}
            />
        </TouchableOpacity>
    )
}