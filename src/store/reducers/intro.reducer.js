import TYPES from '../actions/Types';

const initialState = {
    settings: null,
    loading:false
    
}
const reducer = (state = initialState, actions) => {
    switch (actions.type) {
       
        case TYPES.SAVE_SETTINGS:
            return {
                ...state,
                settings: actions.data
            }

        case TYPES.LOADING:
            return {
                ...state,
                loading: actions.data
            }
        default:
            return state

    }

}
export default reducer