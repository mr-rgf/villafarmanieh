import TYPES from '../actions/Types';

const initialState = {

    filtersresponse: null,

    searchparams: { "by_title": "", "property-type": "", "offer-type": "", "label-type": "", "location-by": "", "currency-type": "", "min-price": "", "max-price": "", "min-area": "", "max-area": "", "type-beds": "", "type-bath": "", "prop-amens": "", "latt": "", "long": "", "distance": "", "sort_by": "", "next_page": "" }


}
const reducer = (state = initialState, actions) => {
    switch (actions.type) {


        case TYPES.UPDATE_FILTERS:
            return {
                ...state,
                filtersresponse: actions.data
            }

        case TYPES.UPDATE_SEARCH_PARAMS:
            return {
                ...state,
                searchparams: actions.data
            }

        default:
            return state

    }

}
export default reducer