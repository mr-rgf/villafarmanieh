import TYPES from '../actions/Types';

const initialState = {
    user: null,
    
}
const reducer = (state = initialState, actions) => {
    switch (actions.type) {
       
        case TYPES.ADD_USER:
            return {
                ...state,
                user: actions.user
            }
        default:
            return state

    }

}
export default reducer