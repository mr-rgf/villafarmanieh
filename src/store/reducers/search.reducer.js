import TYPES from '../actions/Types';

const initialState = {
    searchresponse: {},
    searchlist: [],

    listingDetail: null

}
const reducer = (state = initialState, actions) => {
    switch (actions.type) {

        case TYPES.UPDATE_LIST:
            return {
                ...state,
                searchlist: actions.data.listings,
                searchresponse: actions.data
            }

        case TYPES.UPDATE_LIST_PAGINATION:
            return {
                ...state,
                searchlist: state.searchlist.concat(actions.data.listings),
                searchresponse: actions.data
            }

        case TYPES.UPDATE_LISTING_DETAIL:
            return {
                ...state,
                listingDetail: actions.data,
            }

    
        default:
            return state

    }

}
export default reducer