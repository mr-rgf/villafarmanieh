import TYPES from '../Types';




export const updateFilters = (data) => {
    return {
        type: TYPES.UPDATE_FILTERS,
        data: data
    }
}


export const updateSearchParams = (data) => {
    return {
        type: TYPES.UPDATE_SEARCH_PARAMS,
        data: data
    }
}

