import React, { useState } from 'react'
import { View, TextInput,Text, StyleSheet } from 'react-native'
import { WP, Theme } from '../helpers/Exporter'

export const CustomInput = (props) => {
    const [txtinput, setTxtInput] = useState("")

    return (
        <View style={styles.container}>
            <TextInput
                placeholder={props.placeholder}
                placeholderTextColor={Theme.dark}
                onChangeText={(text) => setTxtInput(text)}

            />

            <Text>{txtinput}</Text>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        height: WP(10),
        width: WP(20),
        backgroundColor: Theme.light
    }
})