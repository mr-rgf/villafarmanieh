import { Platform, Alert } from 'react-native'
import { BASE_URL } from './Config'
import Storage from '../storage/Storage'
import * as Helpers from '../helpers/Exporter'
import * as Actions from '../store/actions/allActions'

class Api {
  static headers() {
    return {
      'Content-Type': 'application/json',
    }

  }

  static get(route) {
    return this.func(route, null, 'GET');
  }

  static put(route, params) {
    return this.func(route, params, 'PUT')
  }

  static post(route, params) {
    return this.func(route, params, 'POST')
  }

  static delete(route, params) {
    return this.func(route, params, 'DELETE')
  }




  static async func(route, params, verb) {
    // dispatch(Actions.loading(true))
    const url = `${BASE_URL}/${route}`
    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null);
    options.headers = Api.headers()



    return fetch(url, options).then(resp => {


      let json = resp.json();

      if (resp.ok) {
        return json
      }
      if (resp.status == 500) {
        Helpers.showToast('Internal Server Error')
        return { success: false }
      }

      return json.then(err => { throw err });
    }).catch(json => {
      console.log('error is ', json)
      // Helpers.showToast('Error')
      // this.showAlert()


      return;
    });
  }

  static showAlert() {
    Alert.alert(
      'Network Error!',
      'Click Ok To Restart App.',
      [

        { text: 'OK', onPress: () => RNRestart.Restart() },
      ],
      { cancelable: false },
    );
  }




  static async postImage(route, key, image) {
    let { orderStore } = Store;
    const url = `${host}/${route}`;
    const formData = new FormData();
    let options = {};

    const extension = image.mime.substring(image.mime.indexOf("/") + 1, image.mime.length);

    const photo = {

      uri: image.path,
      type: image.mime,
      name: "test." + extension,
    };


    formData.append(key, photo);




    // const hash = new Buffer(`${username}:${password}`).toString('base64');
    // options.headers['Authorization'] = `Basic ${hash}`;


    return fetch(url, options).then(resp => {

      let json = resp.json();

      if (resp.ok) {
        return json
      }
      return json.then(err => { throw err });
    }).then(json => {

      return json;
    });

  }

  static async postImageMulti(route, key, image, paramKey, paramValue) {
    const url = `${host}/${route}`;
    const formData = new FormData();
    formData.append(paramKey, paramValue + "");

    let options = {};
    // console.log('here image is', image.length)
    if (image.length != undefined) {
      for (var i = 0; i < image.length; i++) {
        // console.log('size=>', image[i].size);

        const extension = image[i].mime.substring(image[i].mime.indexOf("/") + 1, image[i].mime.length);

        const photo = {

          uri: image[i].path,
          type: image[i].mime,
          name: "test." + extension,
        };
        formData.append(key + i, photo);

      }
    } else {
      const extension = image.mime.substring(image.mime.indexOf("/") + 1, image.mime.length);

      const photo = {

        uri: image.path,
        type: image.mime,
        name: "test." + extension,
      };
      formData.append(key + i, photo);

    }



    // const hash = new Buffer(`${username}:${password}`).toString('base64');
    // options.headers['Authorization'] = `Basic ${hash}`;


    return fetch(url, options).then(resp => {
      // console.log('Api response is ------------->>>>>>', resp);

      let json = resp.json();

      if (resp.ok) {
        return json
      }
      return json.then(err => { throw err });
    }).then(json => {
      // console.log('Api response is ------------->>>>>>', json);

      return json;
    });

  }

}
export default Api