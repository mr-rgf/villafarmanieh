import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native'
import { connect } from 'react-redux';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { AnimatedTabBarNavigator } from "react-native-animated-nav-tab-bar";
import Icon from 'react-native-vector-icons/Feather';



import * as Helpers from '../helpers/Exporter'


import Tab1 from '../modules/tabs/Tab1'
import Tab2 from '../modules/tabs/Tab2'
import Tab3 from '../modules/tabs/Tab3'
import Tab4 from '../modules/tabs/Tab4'

const Tab = createBottomTabNavigator();
const Tabs = AnimatedTabBarNavigator();

class BottomTabs extends Component {
    constructor(props) {
        super(props);
    }


render() {
    return (
        <Tab.Navigator
            tabBarOptions={{ style: styles.tabBar }}
            screenOptions={({ route }) => ({
                tabBarLabel: ({ focused, color, position, }) => (
                    <Text style={[focused ? styles.focusedTabText : styles.unfocusedTabText]}>{route.name}</Text>
                ),
                tabBarIcon: ({ focused, color, size, }) => (
                    <MaterialCommunityIcons
                        name={route.key == 'tab1' ?
                            'home' : route.key == 'tab2' ?
                                'account-circle' : route.key == 'tab3' ?
                                    'home' : route.key == 'tab4' ?
                                        'account-circle' : 'home'
                        }
                        color={focused ? Helpers.Theme.primary : Helpers.Theme.grey}
                        size={focused?Helpers.Typography.six:Helpers.Typography.four} />
                ),
            })}
        >


            <Tab.Screen name="Home" component={Tab1} key="tab1" />
            <Tab.Screen name="Bla Bla" component={Tab2} key="tab2" />
            <Tab.Screen name="Blash" component={Tab3} key="tab3" />
            <Tab.Screen name="Settings" component={Tab4} key="tab4" />

        </Tab.Navigator>

    );
}


}

stateToProps = (state) => {
    return {}
}

const styles = StyleSheet.create({
    tabBar: {
        borderTopEndRadius: Helpers.WP(5),
        borderTopStartRadius: Helpers.WP(5),

        // backgroundColor: "#fff",
        position: 'absolute',
        bottom: 0,
        width: Helpers.WP(100),
        height: Helpers.WP(12),
        zIndex: 8
    },
    focusedTabText: {
        fontSize: 4,
        color: Helpers.Theme.primary
    },
    unfocusedTabText: {
        fontSize: 4,
        color: Helpers.Theme.dark
    },


})

export default connect(stateToProps, null)(BottomTabs);


// render() {
//     return (
//         <Tabs.Navigator
//             tabBarOptions={{
//                 activeTintColor: "#2F7C6E",
//                 inactiveTintColor: "#222222", activeBackgroundColor: '#FFCF64',
//             }}
//         >
//             <Tabs.Screen
//                 name="Home"
//                 component={Tab1}
//                 options={{
//                     tabBarIcon: ({ focused, color, size }) => (
//                         <MaterialCommunityIcons
//                             name={'home'}
//                             color={focused ? Helpers.Theme.primary : Helpers.Theme.grey}
//                             size={focused ? Helpers.Typography.six : Helpers.Typography.four} />
//                     )
//                 }}
//             />

//             <Tabs.Screen
//                 name="Account"
//                 component={Tab2}
//                 options={{
//                     tabBarIcon: ({ focused, color, size }) => (
//                         <MaterialCommunityIcons
//                             name={'account-circle'}
//                             color={focused ? Helpers.Theme.primary : Helpers.Theme.grey}
//                             size={focused ? Helpers.Typography.six : Helpers.Typography.four} />
//                     )
//                 }}
//             />
//             <Tabs.Screen
//                 name="Third"
//                 component={Tab3}
//                 options={{
//                     tabBarIcon: ({ focused, color, size }) => (
//                         <MaterialCommunityIcons
//                             name={'account-circle'}
//                             color={focused ? Helpers.Theme.primary : Helpers.Theme.grey}
//                             size={focused ? Helpers.Typography.six : Helpers.Typography.four} />
//                     )
//                 }}
//             />

//             <Tabs.Screen
//                 name="Fourth"
//                 component={Tab4}
//                 options={{
//                     tabBarIcon: ({ focused, color, size }) => (
//                         <MaterialCommunityIcons
//                             name={'account-circle'}
//                             color={focused ? Helpers.Theme.primary : Helpers.Theme.grey}
//                             size={focused ? Helpers.Typography.six : Helpers.Typography.four} />
//                     )
//                 }}
//             />
             
//         </Tabs.Navigator>

//     );
// }