import 'react-native-gesture-handler';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NavigationContainer, DarkTheme } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/FontAwesome';


import Main from '../modules/main/Main'
import IntroMain from '../modules/intro/MainIntro'
import LocationShare from '../modules/intro/LocationShare'

import BottomTabs from './BottomTabs'
import DrawerRouter from './Drawer'
import Filter from '../modules/filter/Filter';
import Searchlist from '../modules/search/Searchlist';
import Mapviewlist from '../modules/search/Mapviewlist';
import ListingDetail from '../modules/detail/ListingDetail';
import LocationMap from '../modules/detail/locationmap/LocationMap';
import VirtualTour from '../modules/detail/virtualtour/VirtualTour';
import WhatsNearby from '../modules/detail/whatsnearby/WhatsNearby';
import FloorPlans from '../modules/detail/floorplans/FloorPlans';
import MortgageCalculator from '../modules/detail/mortgagecalculator/MortgageCalculator';
import ScheduleTour from '../modules/detail/scheduletour/ScheduleTour';
import ReviewRating from '../modules/detail/reviewrating/ReviewRating';
import WriteReview from '../modules/detail/writereview/WriteReview';
import ContactSeller from '../modules/detail/contactseller/ContactSeller';
import Attachments from '../modules/detail/attachments/Attachments';
import Splash from '../modules/intro/Splash';


const Stack = createStackNavigator();
const headeroptions = { headerShown: false }
class Router extends Component {
    constructor(props) {
        super(props);
        Icon.loadFont()
    }


    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator>
                    <Stack.Screen options={headeroptions} name="splash" component={Splash} />

                    <Stack.Screen options={headeroptions} name="introMain" component={IntroMain} />
                    <Stack.Screen options={headeroptions} name="locationShare" component={LocationShare} />
                    <Stack.Screen options={headeroptions} name="filter" component={Filter} />
                    <Stack.Screen options={headeroptions} name="searchlist" component={Searchlist} />
                    <Stack.Screen options={headeroptions} name="mapviewlist" component={Mapviewlist} />

                    <Stack.Screen options={headeroptions} name="listingdetail" component={ListingDetail} />
                    <Stack.Screen options={headeroptions} name="locationmap" component={LocationMap} />
                    <Stack.Screen options={headeroptions} name="virtualtour" component={VirtualTour} />
                    <Stack.Screen options={headeroptions} name="whatsnearby" component={WhatsNearby} />
                    <Stack.Screen options={headeroptions} name="floorplans" component={FloorPlans} />
                    <Stack.Screen options={headeroptions} name="mortgagecalculator" component={MortgageCalculator} />
                    <Stack.Screen options={headeroptions} name="scheduletour" component={ScheduleTour} />
                    <Stack.Screen options={headeroptions} name="reviewrating" component={ReviewRating} />
                    <Stack.Screen options={headeroptions} name="writereview" component={WriteReview} />
                    <Stack.Screen options={headeroptions} name="contactseller" component={ContactSeller} />
                    <Stack.Screen options={headeroptions} name="attachments" component={Attachments} />


                </Stack.Navigator>
            </NavigationContainer>

        );
    }
}

stateToProps = (state) => {
    return {}
}

export default connect(stateToProps, null)(Router);