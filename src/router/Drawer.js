import React, { Component } from 'react';
import { StyleSheet,View } from 'react-native'
import { connect } from 'react-redux';
import { createDrawerNavigator } from '@react-navigation/drawer';
import * as Helpers from '../helpers/Exporter'

import Second from '../modules/main/Second'
import BottomTabs from './BottomTabs'

const Drawer = createDrawerNavigator();


class DrawerRouter extends Component {
    constructor(props) {
        super(props);
        this.state={
            initRender:true
        }
    }
    componentDidMount(){
        this.setState({initRender:false})
    }

    drawerContent=()=>{
        return(
            <View style={styles.container}>
                    <View style={styles.top}>

                    </View>

                    <View style={styles.top}>
                        
                    </View>
            </View>
        )
    }

    render() {
        return (
            <Drawer.Navigator initialRouteName="main" 
            drawerContent={this.drawerContent} 
            drawerStyle={{ width: this.state.initRender ? null : "70%" }}  //To stop the flickering
            >
                <Drawer.Screen name="main" component={BottomTabs} />
                <Drawer.Screen name="second" component={Second} />
            </Drawer.Navigator>
        );
    }
}

stateToProps = (state) => {
    return {}
}

const styles = StyleSheet.create({
   container:{
       flex:1,
       backgroundColor:Helpers.Theme.light
   },
   top:{
       flex:0.15,
       backgroundColor:Helpers.Theme.error
   },
   body:{}

})

export default connect(stateToProps, null)(DrawerRouter);