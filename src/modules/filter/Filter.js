import React, { Component } from 'react';
import {
    View, Text, TouchableOpacity,
    TextInput, FlatList,
    Image, ScrollView, PermissionsAndroid
} from 'react-native'

import * as Actions from '../../store/actions/allActions'
import * as Helpers from '../../helpers/Exporter'
import { styles } from './Filter.style'
import Api from '../../network/Api'

import { connect } from 'react-redux'
import RangeSlider from 'rn-range-slider';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import RNPickerSelect from 'react-native-picker-select';
import ModalBox from 'react-native-modalbox';
import CheckBox from '@react-native-community/checkbox';
//import Geolocation from 'react-native-geolocation-service';


class Filter extends Component {

    constructor(props) {
        super(props);
        // console.log('thi.',this.props)
        this.state = {
            selectedIndex: this.props.filters.selectedIndex,


            subCategoryList: this.props.filters.subCategoryList,
            currencyTypes: this.props.filters.currencyTypes,
            locationsArray: this.props.filters.locationsArray,
            showsubcategory: this.props.filters.showsubcategory,
            selectedPropertyIndex: this.props.filters.selectedPropertyIndex,
            selectedSubPropertyIndex: this.props.filters.selectedSubPropertyIndex,
            selectedPropertyStatusIndex: this.props.filters.selectedPropertyStatusIndex,

            subcategoryLoading: this.props.filters.subcategoryLoading,

            minprice: this.props.filters.minprice,
            maxprice: this.props.filters.maxprice,
            minimumLowValue: this.props.filters.minimumLowValue,
            maximumHighValue: this.props.filters.maximumHighValue,

            minarea: this.props.filters.minarea,
            maxarea: this.props.filters.maxarea,
            minimumLowValueArea: this.props.filters.minimumLowValueArea,
            maximumHighValueArea: this.props.filters.maximumHighValueArea,


            featureFilter: this.props.filters.featureFilter,

            selectedRoomId: this.props.filters.selectedRoomId,
            selectedBathId: this.props.filters.selectedBathId,

            loadingLocations: this.props.filters.loadingLocations,
            currentCurreny: this.props.filters.currentCurreny,

            shownearme: this.props.filters.shownearme,

            searchtxt: this.props.filters.searchtxt,
            minrange: this.props.filters.minrange,
            maxrange: this.props.filters.maxrange,

            currentLocation: this.props.filters.currentLocation

        };


    }



    handlePropertyCategoryChange = async (item, index) => {
        var response

        if (index == this.state.selectedPropertyIndex) {
            this.setState({
                ...this.state,
                selectedPropertyIndex: null,
                selectedSubPropertyIndex: null,
                showsubcategory: false,
                subCategoryList: []
            });
        } else {
            this.setState({ subcategoryLoading: true, selectedPropertyIndex: index, })
            let param = {
                "slug": item.slug
            }
            response = await Api.post("more-types", param)
            if (response.success) {
                this.setState({
                    showsubcategory: true,
                    subcategoryLoading: false,
                    subCategoryList: response.data.option_dropdown

                });
            } else {
                this.setState({
                    showsubcategory: true,
                    subcategoryLoading: false,
                    subCategoryList: []

                });
            }
        }

    }


    loadmoreLocations = async (item) => {
        var response;

        this.setState({ loadingLocations: true })

        let param = {
            "slug": item.slug
        }
        response = await Api.post("more-locations", param)

        if (response.success) {
            if (response.data.option_dropdown.length != 0) {
                this.setState({ loadingLocations: false, locationsArray: response.data.option_dropdown })

            } else {
                this.refs.locationModal.close()
                this.setState({ loadingLocations: false, currentLocation: item })

            }

        } else {
            this.setState({ loadingLocations: false })

        }


    }

    handlesubCategoryChange = index => {


        this.setState({
            ...this.state,
            selectedSubPropertyIndex: index,
        });
    }

    handlePropertyStatusChange = index => {
        if (index == this.state.selectedPropertyStatusIndex) {
            this.setState({
                ...this.state,
                selectedPropertyStatusIndex: null,
            });
        } else {
            this.setState({
                ...this.state,
                selectedPropertyStatusIndex: index,
            });
        }


    }

    handleIndexChangeOfferType = index => {
        this.setState({
            ...this.state,
            selectedIndex: index
        });
    };



    handleBedChange = (index) => {
        if (index == this.state.selectedRoomId) {
            this.setState({
                selectedRoomId: null,
            });
        } else {
            this.setState({
                selectedRoomId: index,
            });
        }

    }
    handleBathChange = (id) => {
        if (id == this.state.selectedBathId) {
            this.setState({
                selectedBathId: null,
            });
        } else {
            this.setState({
                selectedBathId: id,
            });
        }

    }

    handleCheckBox = (ind) => {
        var newtemp = this.props.settings.features.option_dropdown
        newtemp.forEach((item, index) => {
            if (index == ind) {
                newtemp[index].checked = !newtemp[index].checked
            }
        })
        this.setState({ featureFilter: newtemp })
    }

    handleChangeText = (fields, value) => {
        if (fields == 'minprice' || fields == 'maxprice') {
            this.setRangeSlideValues('price', fields, value)

        } else if (fields == 'minarea' || fields == 'maxarea') {
            this.setRangeSlideValues('area', fields, value)
        }

        this.setState({ [fields]: value })
    }

    setRangeSlideValues = (type, fields, value) => {
        if (type == 'price') {
            if (fields == 'minprice' && (+value >= this.state.minimumLowValue && +value <= this.state.maximumHighValue && +value < this.state.maxprice)) {
                this._rangeSliderPrice.setLowValue(parseInt(value))
            }
            if (fields == 'maxprice' && (+value <= this.state.maximumHighValue && +value >= this.state.minimumLowValue && +value > this.state.minprice)) {
                this._rangeSliderPrice.setHighValue(parseInt(value))
            }
        }
        if (type == 'area') {
            if (fields == 'minarea' && (+value >= this.state.minimumLowValueArea && +value <= this.state.maximumHighValueArea && +value < this.state.maxarea)) {
                this._rangeSliderArea.setLowValue(parseInt(value))
            }
            if (fields == 'maxarea' && (+value <= this.state.maximumHighValueArea && +value >= this.state.minimumLowValueArea && +value > this.state.minarea)) {
                this._rangeSliderArea.setHighValue(parseInt(value))
            }
        }
    }


    openLocations = () => {
        this.setState({ locationsArray: this.props.settings.location.option_dropdown })
        this.refs.locationModal.open()
    }

    handleNearMe = () => {
        this.setState({ shownearme: !this.state.shownearme })
    }

    reset = () => {
        let dummyfeatures = this.props.settings.features.option_dropdown
        dummyfeatures.forEach((item) => {
            item.checked = false
        })
        // console.log('this.props.settings.price.max_price',this.props.settings.price)
        var param = {
            selectedIndex: 0,

            subCategoryList: [],
            currencyTypes: [],
            locationsArray: this.props.settings.location.option_dropdown,
            showsubcategory: false,
            selectedPropertyIndex: null,
            selectedSubPropertyIndex: null,
            selectedPropertyStatusIndex: null,

            subcategoryLoading: false,

            minprice: this.props.settings.price.min_price,
            maxprice: this.props.settings.price.max_price,
            minimumLowValue: this.props.settings.price.min_price,
            maximumHighValue: this.props.settings.price.max_price,

            minarea: this.props.settings.area.min_area_unit,
            maxarea: this.props.settings.area.max_area_unit,

            minimumLowValueArea: this.props.settings.area.min_area_unit,
            maximumHighValueArea: this.props.settings.area.max_area_unit,


            featureFilter: dummyfeatures,

            selectedRoomId: 0,
            selectedBathId: 0,

            loadingLocations: false,
            currentCurreny: undefined,

            shownearme: false,

            searchtxt: "",
            minrange: 0,
            maxrange: 500


        }
        this.setState(param);
        this.handleChangeText('minprice', this.props.settings.price.min_price)
        this.handleChangeText('maxprice', this.props.settings.price.max_price)
        this.handleChangeText('minarea', this.props.settings.area.min_area_unit)
        this.handleChangeText('maxarea', this.props.settings.area.max_area_unit)
        this.props.updateSP(param)

        this.props.savefilters(param)



    }

    searchResults = async () => {
        let currentDetail = this.props.settings
        let currentfeatures = this.state.featureFilter
        var proptype = "";
        let featurestemp = []
        let propertystatus = this.state.selectedPropertyStatusIndex == null ? "" : currentDetail.status.option_dropdown[this.state.selectedPropertyStatusIndex].label_slug
        var distance = ""
        var lat = ""
        var long = ""
        var params = ""


        if (this.state.selectedSubPropertyIndex != null) {
            proptype = this.state.subCategoryList[this.state.selectedSubPropertyIndex].slug
        } else if (this.state.selectedPropertyIndex != null) {
            proptype = currentDetail.property_type.option_dropdown[this.state.selectedPropertyIndex].slug
        }

        currentfeatures.forEach((item) => {
            if (item.checked == true) {
                featurestemp.push(item.features_slug)
            }
        })
        if (this.state.shownearme) {
            /*distance = this.state.minrange
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            )
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {


                Geolocation.getCurrentPosition(info => {
                    console.log('fetch location ', info)
                    params = {
                        "by_title": this.state.searchtxt,
                        "property-type": proptype,
                        "offer-type": currentDetail.offertype.option_dropdown[this.state.selectedIndex].offer_slug,
                        "label-type": propertystatus,
                        "location-by": this.state.currentLocation.slug,
                        "currency-type": this.state.currentCurreny == undefined ? "" : this.state.currentCurreny,
                        "min-price": this.state.minprice,
                        "max-price": this.state.maxprice,
                        "min-area": this.state.minarea,
                        "max-area": this.state.maxarea,
                        "type-beds": currentDetail.bedrooms.option_dropdown[this.state.selectedRoomId].value,
                        "type-bath": currentDetail.bathrooms.option_dropdown[this.state.selectedBathId].value,
                        "prop-amens": featurestemp.length == 0 ? "" : featurestemp,
                        "latt": info.coords.latitude,
                        "long": info.coords.longitude,
                        "distance": distance,
                        "sort_by": ""
                    }

                    this.fetchSearchData(params)
                },
                    error => {
                        console.log('couldn`t fetch location ', error)

                        params = {
                            "by_title": this.state.searchtxt,
                            "property-type": proptype,
                            "offer-type": currentDetail.offertype.option_dropdown[this.state.selectedIndex].offer_slug,
                            "label-type": propertystatus,
                            "location-by": this.state.currentLocation.slug,
                            "currency-type": this.state.currentCurreny == undefined ? "" : this.state.currentCurreny,
                            "min-price": this.state.minprice,
                            "max-price": this.state.maxprice,
                            "min-area": this.state.minarea,
                            "max-area": this.state.maxarea,
                            "type-beds": currentDetail.bedrooms.option_dropdown[this.state.selectedRoomId].value,
                            "type-bath": currentDetail.bathrooms.option_dropdown[this.state.selectedBathId].value,
                            "prop-amens": featurestemp.length == 0 ? "" : featurestemp,
                            "latt": "",
                            "long": "",
                            "distance": distance,
                            "sort_by": ""
                        }
                        this.fetchSearchData(params)

                    },

                    { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
                );
            }*/
        } else {

            params = {
                "by_title": this.state.searchtxt,
                "property-type": proptype,
                "offer-type": currentDetail.offertype.option_dropdown[this.state.selectedIndex].offer_slug,
                "label-type": propertystatus,
                "location-by": this.state.currentLocation.slug,
                "currency-type": this.state.currentCurreny == undefined ? "" : this.state.currentCurreny,
                "min-price": this.state.minprice,
                "max-price": this.state.maxprice,
                "min-area": this.state.minarea,
                "max-area": this.state.maxarea,
                "type-beds": currentDetail.bedrooms.option_dropdown[this.state.selectedRoomId].value,
                "type-bath": currentDetail.bathrooms.option_dropdown[this.state.selectedBathId].value,
                "prop-amens": featurestemp.length == 0 ? "" : featurestemp,
                "latt": "",
                "long": "",
                "distance": distance,
                "sort_by": ""
            }
            this.fetchSearchData(params)

            // console.log('parasn', params)


        }



        // console.log('currentedet',currentDetail)
    }

    fetchSearchData = async (param) => {
        var response
        this.props.setloading(true)

        response = await Api.post("search", param)

        if (response.success == true) {
            await this.props.updatelist({ data: response.data.final_content })
            this.updatefilters(param)

            this.props.navigation.goBack()
        } else {
            await this.props.updatelist({ data: response.data })
            this.updatefilters(param)
            this.props.navigation.goBack()
        }
        this.props.setloading(false)
    }

    updatefilters = (param) => {
        this.props.savefilters(this.state)
        this.props.updateSP(param)

    }



    render() {
        let currentDetail = this.props.settings
        return (
            <View style={styles.container}>

                <View style={styles.header}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.navigation.goBack()
                        }}
                    >
                        <Helpers.leftArrow color={Helpers.Theme.light} />
                    </TouchableOpacity>
                    <Text style={[styles.headingTxt, Helpers.Typography.fourPointFive]}>{currentDetail.filters}</Text>

                    <TouchableOpacity
                        onPress={() => this.reset()}
                        style={styles.resetBtn}>
                        <Text style={[styles.resetTxt, Helpers.Typography.four]}>{currentDetail.reset}</Text>
                    </TouchableOpacity>

                </View>
                {

                    this.props.settings == null ? [] : [
                        <KeyboardAwareScrollView style={styles.scrollVw}>

                            <View style={styles.topVw}>
                                <Text style={[Helpers.Typography.five, styles.topVwTxt]}>{currentDetail.search_filters}</Text>
                            </View>
                            {/* NEAR ME SLIDER START */}
                            {
                                this.state.shownearme ? [
                                    <View style={styles.shownearme}>
                                        <View style={styles.keywordupperVw}>
                                            <Image
                                                source={Helpers.Images.map}
                                                style={styles.searchIcon}
                                            />
                                            <Text style={[Helpers.Typography.four, styles.keywordTxt]}>{currentDetail.current_radius}</Text>
                                        </View>

                                        <RangeSlider
                                            rangeEnabled={false}
                                            ref={component => this._nearmeSlider = component}
                                            style={styles.rangesliderstyle2}
                                            min={0}
                                            max={500}
                                            step={20}
                                            initialLowValue={this.state.minrange}
                                            initialHighValue={this.state.maxrange}
                                            labelGapHeight={Helpers.WP(-3)}
                                            labelTailHeight={Helpers.WP(2)}
                                            selectionColor={Helpers.Theme.blueBtnBg}
                                            thumbBorderColor={Helpers.Theme.blueBtnBg}
                                            labelBackgroundColor={Helpers.Theme.blueBtnBg}
                                            labelBorderColor={Helpers.Theme.blueBtnBg}
                                            blankColor={'#d8d8d8'}
                                            onValueChanged={(low, high, fromUser) => {
                                                this.setState({ minrange: "" + low, maxrange: "" + high })
                                            }} />
                                    </View>

                                ] : []
                            }
                            {/* NEAR ME SLIDER END */}


                            {/* OFFER TYPES START */}

                            <View style={styles.propertytypeVw}>

                                <FlatList
                                    data={currentDetail.offertype.option_dropdown}
                                    keyExtractor={item => item.id}
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <TouchableOpacity
                                                activeOpacity={1}
                                                onPress={() => this.handleIndexChangeOfferType(index)}
                                                style={[styles.segmentBtn, this.state.selectedIndex == index ? styles.selectedsegmentBtn : {}]}>
                                                <Text style={[this.state.selectedIndex == index ? styles.lightTxt : styles.darkTxt]}>{item.offer}</Text>
                                            </TouchableOpacity>
                                        )

                                    }}
                                />



                            </View>

                            {/* OFFER TYPES END */}

                            <View style={styles.keywordVw}>
                                <View style={styles.keywordupperVw}>
                                    <Image
                                        source={Helpers.Images.search}
                                        style={styles.searchIcon}
                                    />
                                    <Text style={[Helpers.Typography.four, styles.keywordTxt]}>{currentDetail.keyword.field_title}</Text>
                                </View>
                                <View style={styles.keywordInputVw}>
                                    <TextInput
                                        placeholder={currentDetail.keyword.field_placeholder}
                                        style={{ height: '100%', width: '100%',fontFamily:'IRANSansWeb(FaNum)',fontSize:13,marginTop:2 }}
                                        value={this.state.searchtxt}
                                        onChangeText={(text) => this.handleChangeText('searchtxt', text)}
                                    />
                                </View>
                            </View>

                            <View style={styles.porpertycategoriesVw}>
                                <View style={styles.keywordupperVw}>
                                    <Image
                                        source={Helpers.Images.home}
                                        style={styles.searchIcon}
                                    />
                                    <Text style={[Helpers.Typography.four, styles.keywordTxt]}>{currentDetail.property_type.field_title}</Text>
                                </View>

                                <FlatList
                                    data={currentDetail.property_type.option_dropdown}
                                    keyExtractor={item => item.id}
                                    horizontal
                                    showsHorizontalScrollIndicator={false}
                                    style={styles.porpertycategoriesList}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <TouchableOpacity
                                                key={item}

                                                activeOpacity={1}
                                                onPress={() => this.handlePropertyCategoryChange(item, index)}
                                                style={styles.porpertycategoriesListMainVw}>
                                                <View style={styles.porpertycategoriesListSubVw}>
                                                    {
                                                        this.state.selectedPropertyIndex != null && index == this.state.selectedPropertyIndex ?
                                                            [
                                                                <View style={styles.porpertycategoriesListSelecteditemVw}>
                                                                    <Image
                                                                        source={Helpers.Images.tick}
                                                                        style={styles.porpertycategoriesListSelecteditemImg}
                                                                    />
                                                                </View>
                                                            ] : []
                                                    }


                                                    <Image
                                                        source={{ uri: item.type_icon }}
                                                        style={styles.porpertycategoriesListitemImg}
                                                    />

                                                </View>
                                                <Text style={styles.porpertycategoriesListitemTxt}>{item.name}</Text>

                                            </TouchableOpacity>
                                        )

                                    }}
                                />
                                {
                                    this.state.subcategoryLoading ? [
                                        <>
                                            <View style={{ marginTop: Helpers.WP(3) }} />
                                            <Helpers.Lumper lumper={true} color={Helpers.Theme.blueBtnBg} />

                                        </>
                                    ] : [

                                            this.state.showsubcategory ? [
                                                <FlatList
                                                    data={this.state.subCategoryList}
                                                    keyExtractor={item => item.id}
                                                    horizontal
                                                    style={[styles.porpertycategoriesList, styles.upspace]}
                                                    renderItem={({ item, index }) => {
                                                        return (
                                                            <TouchableOpacity
                                                                key={item}

                                                                activeOpacity={1}
                                                                onPress={() => this.handlesubCategoryChange(index)}
                                                                style={[styles.subcatlistBtn, this.state.selectedSubPropertyIndex != null && index == this.state.selectedSubPropertyIndex ? { backgroundColor: Helpers.Theme.blueBtnBg } : {}]}>
                                                                <Text style={[this.state.selectedSubPropertyIndex != null && index == this.state.selectedSubPropertyIndex ? { color: Helpers.Theme.light } : { color: Helpers.Theme.dark }, Helpers.Typography.three]}>{item.name}</Text>
                                                            </TouchableOpacity>
                                                        )

                                                    }}
                                                />
                                            ] : []
                                        ]
                                }



                            </View>


                            <View style={styles.keywordVw}>
                                <View style={styles.keywordupperVw}>
                                    <Image
                                        source={Helpers.Images.gear}
                                        style={styles.searchIcon}
                                    />
                                    <Text style={[Helpers.Typography.four, styles.keywordTxt]}>برچسب</Text>
                                </View>
                                <View style={styles.offerTypeVw}>
                                    <FlatList
                                        data={currentDetail.status.option_dropdown}
                                        keyExtractor={item => item.id}
                                        horizontal
                                        renderItem={({ item, index }) => {
                                            return (
                                                <TouchableOpacity
                                                    key={item}
                                                    activeOpacity={1}
                                                    onPress={() => this.handlePropertyStatusChange(index)}
                                                    style={[styles.subcatlistOfferBtn, this.state.selectedPropertyStatusIndex != null && index == this.state.selectedPropertyStatusIndex ? { backgroundColor: Helpers.Theme.blueBtnBg } : {}]}>
                                                    <Text style={[this.state.selectedPropertyStatusIndex != null && index == this.state.selectedPropertyStatusIndex ? { color: Helpers.Theme.light } : { color: Helpers.Theme.dark }, Helpers.Typography.three]}>{item.label}</Text>
                                                </TouchableOpacity>
                                            )

                                        }}
                                    />
                                </View>
                            </View>

                            {/* Price range START*/}
                            <View >
                                <View style={styles.pricerangeTopVw}>
                                    <View style={styles.keywordupperVw}>
                                        <Image
                                            source={Helpers.Images.tag}
                                            style={styles.searchIcon}
                                        />
                                        <Text style={[Helpers.Typography.four, styles.keywordTxt]}>{currentDetail.price.field_title}</Text>

                                        <View style={{ position: 'absolute', right: Helpers.WP(2) }}>
                                            <RNPickerSelect
                                                onValueChange={value => {
                                                    this.setState({
                                                        currentCurreny: value,
                                                    });
                                                }}
                                                style={
                                                    {
                                                        inputIOS: {
                                                            color: 'black',
                                                        },
                                                        inputAndroid: {
                                                            color: 'black'
                                                        }
                                                    }
                                                }
                                            
                                                items={currentDetail.currency.option_dropdown}
                                                placeholder={{ label: currentDetail?.currency?.placeholder?.label || 'تومان', value: null }}
                                                useNativeAndroidPickerStyle={false}
                                                value={this.state.currentCurreny}

                                            />


                                        </View>


                                    </View>
                                    <View style={[styles.offerTypeVw, styles.pricerangeVws]}>
                                        <View style={styles.pricerangesingleVw}>
                                            <TextInput
                                                placeholder={currentDetail.price.field_placeholder_1}
                                                value={this.state.minprice + ""}
                                                keyboardType="numeric"
                                                returnKeyType="done"
                                                style={{ height: '100%', width: '100%', fontFamily: 'IRANSansWeb(FaNum)' }}
                                                onChangeText={(text) => this.handleChangeText('minprice', text)}
                                            />
                                        </View>

                                        <Text style={styles.marginleft}>-</Text>

                                        <View style={[styles.pricerangesingleVw, styles.marginleft]}>
                                            <TextInput
                                                placeholder={currentDetail.price.field_placeholder_2}
                                                value={this.state.maxprice + ""}
                                                keyboardType="numeric"
                                                returnKeyType="done"
                                                style={[Helpers.GLOBAL_SHEET.maxHW,{ fontFamily: 'IRANSansWeb(FaNum)'}]}
                                                onChangeText={(text) => this.handleChangeText('maxprice', text)}
                                            />
                                        </View>
                                    </View>




                                </View>

                                <RangeSlider
                                    ref={component => this._rangeSliderPrice = component}
                                    style={styles.rangesliderstyle}
                                    min={currentDetail.price.min_price}
                                    max={currentDetail.price.max_price}
                                    initialLowValue={this.state.minprice != null && this.state.minprice != "" ? this.state.minprice : this.props.searchparams.minprice}
                                    initialHighValue={this.state.maxprice != null && this.state.maxprice != "" ? this.state.maxprice : this.props.searchparams.maxprice}
                                    step={20}
                                    labelGapHeight={Helpers.WP(-3)}
                                    labelTailHeight={Helpers.WP(-3)}
                                    selectionColor={Helpers.Theme.blueBtnBg}
                                    thumbBorderColor={Helpers.Theme.blueBtnBg}
                                    labelBackgroundColor={Helpers.Theme.blueBtnBg}
                                    labelBorderColor={Helpers.Theme.blueBtnBg}
                                    blankColor={'#d8d8d8'}
                                    onValueChanged={(low, high, fromUser) => {
                                        this.setState({ minprice: "" + low, maxprice: "" + high })
                                    }} />
                            </View>
                            {/* Price range END */}

                            {/* Area size START*/}
                            <View >
                                <View style={styles.pricerangeTopVw}>
                                    <View style={styles.keywordupperVw}>
                                        <Image
                                            source={Helpers.Images.area}
                                            style={styles.searchIcon}
                                        />
                                        <Text style={[Helpers.Typography.four, styles.keywordTxt]}>متراژ زمین</Text>
                                    </View>
                                    <View style={[styles.offerTypeVw, styles.pricerangeVws]}>
                                        <View style={styles.pricerangesingleVw}>
                                            <TextInput
                                                placeholder={currentDetail.area.field_placeholder_1}
                                                value={this.state.minarea + ""}
                                                keyboardType="numeric"
                                                returnKeyType="done"
                                                style={{ height: '100%', width: '100%', fontFamily: 'IRANSansWeb(FaNum)' }}
                                                onChangeText={(text) => this.handleChangeText('minarea', text)}
                                            />
                                        </View>

                                        <Text style={styles.marginleft}>-</Text>

                                        <View style={[styles.pricerangesingleVw, styles.marginleft]}>
                                            <TextInput
                                                placeholder={currentDetail.area.field_placeholder_2}
                                                value={this.state.maxarea + ""}
                                                keyboardType="numeric"
                                                returnKeyType="done"
                                                style={[Helpers.GLOBAL_SHEET.maxHW,{ fontFamily: 'IRANSansWeb(FaNum)'}]}
                                                onChangeText={(text) => this.handleChangeText('maxarea', text)}
                                            />
                                        </View>
                                    </View>




                                </View>

                                <RangeSlider
                                    ref={component => this._rangeSliderArea = component}
                                    style={styles.rangesliderstyle}
                                    min={currentDetail.area.min_area_unit}
                                    max={currentDetail.area.max_area_unit}
                                    initialLowValue={this.state.minarea != null && this.state.minarea != "" ? this.state.minarea : this.props.searchparams.minarea}
                                    initialHighValue={this.state.maxarea != null && this.state.maxarea != "" ? this.state.maxarea : this.props.searchparams.maxarea}
                                    step={20}
                                    labelGapHeight={Helpers.WP(-3)}
                                    labelTailHeight={Helpers.WP(-3)}
                                    selectionColor={Helpers.Theme.blueBtnBg}
                                    thumbBorderColor={Helpers.Theme.blueBtnBg}
                                    labelBackgroundColor={Helpers.Theme.blueBtnBg}
                                    labelBorderColor={Helpers.Theme.blueBtnBg}
                                    blankColor={'#d8d8d8'}
                                    onValueChanged={(low, high, fromUser) => {
                                        this.setState({ minarea: "" + low, maxarea: "" + high })
                                    }} />
                            </View>
                            {/* Area Size END */}

                            {/* Location START*/}
                            <View>
                                <View style={styles.pricerangeTopVw}>
                                    <View style={styles.keywordupperVw}>
                                        <Image
                                            source={Helpers.Images.pinpoint}
                                            style={styles.searchIcon}
                                        />
                                        <Text style={[Helpers.Typography.four, styles.keywordTxt]}>موقعیت</Text>
                                    </View>
                                    <View style={[styles.offerTypeVw, styles.pricerangeVws, { alignItems: 'center' }]}>
                                        <TouchableOpacity
                                            onPress={() => this.openLocations()}
                                            style={styles.locationVw}>
                                            <Text>{this.state.currentLocation == "" || this.state.currentLocation == null ? currentDetail.location.field_title : this.state.currentLocation.name}</Text>

                                            <Image
                                                source={Helpers.Images.downArrow}
                                                style={{ height: Helpers.WP(3), width: Helpers.WP(3), position: "absolute", right: Helpers.WP(2), alignSelf: 'center' }}
                                            />
                                        </TouchableOpacity>


                                    </View>
                                </View>


                            </View>
                            {/* Location END */}

                            {/* Bedrooms START*/}
                            <View style={[styles.pricerangeTopVw, styles.bedtopVw]}>
                                <View style={styles.keywordupperVw}>
                                    <Image
                                        source={Helpers.Images.bed}
                                        style={styles.searchIcon}
                                    />
                                    <Text style={[Helpers.Typography.four, styles.keywordTxt]}>{currentDetail.bedrooms.field_title}</Text>
                                </View>
                                <View style={[styles.pricerangeVws, { alignItems: 'center' }]}>

                                    <FlatList
                                        data={currentDetail.bedrooms.option_dropdown}
                                        keyExtractor={item => item.title}
                                        horizontal
                                        style={[styles.porpertycategoriesList]}
                                        renderItem={({ item, index }) => {
                                            return (
                                                <TouchableOpacity
                                                    onPress={() => this.handleBedChange(index)}
                                                    style={[styles.beditemBtn, index == 0 ? styles.beditemBtnFirst : {}
                                                        , index == currentDetail.bedrooms.option_dropdown.length - 1 ? styles.beditemBtnLast : {},
                                                    this.state.selectedRoomId == index ? {
                                                        backgroundColor: Helpers.Theme.blueBtnBg
                                                    } : {}

                                                    ]}
                                                >
                                                    <Text style={[this.state.selectedRoomId == index ? { color: Helpers.Theme.light } : { color: Helpers.Theme.black }]}>{item.title}</Text>
                                                </TouchableOpacity>
                                            )

                                        }}
                                    />

                                </View>
                            </View>

                            {/* Bedrooms END */}

                            {/* Bathrooms START*/}
                            <View style={[styles.pricerangeTopVw, styles.bedtopVw]}>
                                <View style={styles.keywordupperVw}>
                                    <Image
                                        source={Helpers.Images.bathtub}
                                        style={styles.searchIcon}
                                    />
                                    <Text style={[Helpers.Typography.four, styles.keywordTxt]}>{currentDetail.bathrooms.field_title}</Text>
                                </View>
                                <View style={[styles.pricerangeVws, { alignItems: 'center' }]}>

                                    <FlatList
                                        data={currentDetail.bathrooms.option_dropdown}
                                        keyExtractor={item => item.title}
                                        horizontal
                                        style={[styles.porpertycategoriesList]}
                                        renderItem={({ item, index }) => {
                                            return (
                                                <TouchableOpacity
                                                    onPress={() => this.handleBathChange(index)}
                                                    style={[styles.beditemBtn, index == 0 ? styles.beditemBtnFirst : {}
                                                        , index == currentDetail.bathrooms.option_dropdown.length - 1 ? styles.beditemBtnLast : {},
                                                    this.state.selectedBathId == index ? {
                                                        backgroundColor: Helpers.Theme.blueBtnBg
                                                    } : {}

                                                    ]}
                                                >
                                                    <Text style={[this.state.selectedBathId == index ? { color: Helpers.Theme.light } : { color: Helpers.Theme.black }]}>{item.title}</Text>
                                                </TouchableOpacity>
                                            )

                                        }}
                                    />

                                </View>
                            </View>
                            {/* Bathroom END */}

                            {/* Features START*/}
                            <View style={[styles.featureFilterVw, styles.bedtopVw]}>
                                <View style={styles.keywordupperVw}>

                                    <Text style={[Helpers.Typography.four, { marginLeft: 0 }]}>امکانات</Text>
                                </View>
                                <View style={[{ alignItems: 'center', }]}>

                                    <FlatList
                                        data={this.state.featureFilter}
                                        keyExtractor={item => item.id}
                                        numColumns={2}
                                        style={[styles.porpertycategoriesList]}
                                        renderItem={({ item, index }) => {
                                            return (
                                                <View style={styles.featureitemBtn}>
                                                    <CheckBox
                                                        disabled={false}
                                                        tintColors={{ true: currentDetail.primary_clr }}
                                                        boxType='square'
                                                        style={styles.checkbox}
                                                        onCheckColor={currentDetail.primary_clr}
                                                        value={item.checked}
                                                        onValueChange={() => this.handleCheckBox(index)}
                                                    />
                                                    <Text style={styles.checkboxTxt}>{item.features}</Text>
                                                </View>

                                            )

                                        }}
                                    />

                                </View>
                            </View>
                            {/* Features END */}


                        </KeyboardAwareScrollView>
                    ]
                }
                <TouchableOpacity
                    onPress={() => this.searchResults()}
                    style={[styles.searchBtn,{marginBottom:10}]}>
                    {
                        this.props.loading ? [
                            <Helpers.Lumper lumper={true} color={Helpers.Theme.light} />
                        ] : [
                                <Text style={{ color: Helpers.Theme.light }}>نمایش نتایج</Text>

                            ]
                    }
                </TouchableOpacity>
                <ModalBox
                    style={styles.modalBoxStyle}
                    position={"bottom"}
                    ref={"locationModal"}>
                    <View style={styles.modalContentContainer}>
                        <View style={[styles.row]}>
                            <Text style={[styles.subHeadingText]}>{currentDetail.location.field_title}</Text>




                            {
                                this.state.subcategoryShownOnce == true ? [
                                    <TouchableOpacity
                                        onPress={() => this.closeModal()}
                                        style={{ position: 'absolute', right: 5 }}>
                                        <Text style={{ color: orderStore.appColor }}>Done</Text>
                                    </TouchableOpacity>

                                ] : []
                            }
                        </View>
                        <View style={[styles.modaBoxInnerContainer]}  >
                            {

                                this.state.loadingLocations ?
                                    <View style={{ position: 'absolute', right: 0, left: 0, bottom: 0, top: 0 }}>
                                        <Helpers.Lumper lumper={true} color={currentDetail.primary_clr} />

                                    </View> : [
                                        <ScrollView contentContainerStyle={[styles.modalScrollviewContainer]}>
                                            {
                                                this.state.locationsArray.map((item, i) => {

                                                    return (

                                                        <View key={i} style={styles.modalItemContainer}>
                                                            <TouchableOpacity
                                                                onPress={() => this.loadmoreLocations(item)}
                                                                style={[styles.modalDataTextContainer]}>
                                                                <Text style={[styles.modalText, { color: "#000" }]}>{item.name}</Text>
                                                            </TouchableOpacity>
                                                        </View>

                                                    );


                                                })


                                            }

                                        </ScrollView>
                                    ]
                            }

                        </View>


                    </View>


                </ModalBox>
            </View>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.intro.loading,
        filters: state.filters.filtersresponse,
        settings: state.intro.settings,

        searchparams: state.filters.searchparams


    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        updateFilters: params => dispatch(Actions.updateFilters(params)),
        setloading: params => dispatch(Actions.loading(params)),
        updatelist: params => dispatch(Actions.updateList(params)),
        savefilters: params => dispatch(Actions.updateFilters(params)),
        updateSP: params => dispatch(Actions.updateSearchParams(params)),


    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Filter);
