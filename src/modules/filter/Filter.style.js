import {StyleSheet} from 'react-native'
import * as Helpers from '../../helpers/Exporter'
import {WP} from '../../helpers/Exporter'

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Helpers.Theme.light,

    },
    shownearme:{
        width: '100%', paddingHorizontal: Helpers.WP(5), paddingBottom: Helpers.WP(5)
    },
    featureitemBtn:{
        flexDirection: 'row',alignItems:'center',
        width:Helpers.WP(45),height:Helpers.WP(10),
        marginLeft:Helpers.WP(1)
    },
    checkbox:{
        height: Helpers.WP(5), width: Helpers.WP(5)
    },
    checkboxTxt:{
        width:Helpers.WP(30),marginLeft:Helpers.WP(2)
    },

    searchBtn:{
        height:Helpers.WP(12),
        width:'90%',
        //
        position:"absolute",bottom:WP(0),

        //
        borderRadius:Helpers.WP(1),
        // marginTop:WP(2),
        alignSelf:'center',
        backgroundColor:Helpers.Theme.blueBtnBg,
        alignItems:'center',justifyContent:'center'
    },
    // searchBtn:{
    //     height:Helpers.WP(12),
    //     width:'90%',
    //     //
    //     position:"absolute",bottom:WP(1),

    //     //
    //     borderRadius:Helpers.WP(1),
    //     marginTop:WP(2),marginBottom:Helpers.WP(5),
    //     alignSelf:'center',
    //     backgroundColor:Helpers.Theme.blueBtnBg,
    //     alignItems:'center',justifyContent:'center'
    // },

    modalBoxStyle: {
        alignItems: 'center',
        height: 300,
        borderRadius: 10,
        padding:WP(3),

    },
    bedtopVw: {
        marginTop: Helpers.WP(3), marginLeft: Helpers.WP(6), width: '88%'
    },
    beditemBtn: {
        height: Helpers.WP(10), width: Helpers.WP(14)
        , borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        backgroundColor: Helpers.Theme.introBg, justifyContent: 'center', alignItems: 'center'
    },
    beditemBtnFirst: {
        borderTopLeftRadius: Helpers.WP(1), borderBottomLeftRadius: Helpers.WP(1)
    },
    beditemBtnLast: {
        borderTopRightRadius: Helpers.WP(1), borderBottomRightRadius: Helpers.WP(1)
    },
    modalContentContainer: {
        width: '95%',
        backgroundColor: 'white',
        paddingBottom: 15,
    },

    subHeadingText: {
        color: '#999999',
        fontSize: 16,
        marginTop: 10,
        fontWeight: '400',

    }, row: {
        flexDirection: 'row',
        // padding:WP(5)
    },
    pricerangeTopVw: {

        height: Helpers.WP(22),

        width: '90%',
        marginTop: Helpers.WP(6),
        marginHorizontal: Helpers.WP(5),

    },
    featureFilterVw: {


        width: '90%',
        marginTop: Helpers.WP(6),
        marginHorizontal: Helpers.WP(5),
        marginBottom:WP(15)

    },
    rangesliderstyle: {
        width: Helpers.WP(80), height: Helpers.WP(7),
        alignSelf: 'center'
    },
    rangesliderstyle2: {
        width: Helpers.WP(80), height: Helpers.WP(12),
        alignSelf: 'center'
    },
    pricerangeVws: {
        height: '60%',
        flexDirection: 'row', alignItems: "center"
    },
    pricerangesingleVw: {
        height: Helpers.WP(12), width: Helpers.WP(40),
        borderRadius: Helpers.WP(1), borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        backgroundColor: Helpers.Theme.introBg,
        paddingHorizontal: Helpers.WP(2)
    },

    locationVw: {
        height: Helpers.WP(10), width: Helpers.WP(85),
        borderRadius: Helpers.WP(1), borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        backgroundColor: Helpers.Theme.introBg,
        paddingHorizontal: Helpers.WP(2),
        alignItems: "center", flexDirection: 'row'
    },
    marginleft: {
        marginLeft: Helpers.WP(2)
    },
    dorpDownRow: {
        paddingTop: 5,
        paddingStart: 15,
        width: '100%',
        justifyContent: 'center',
    },

    dorpDownStyle: {
        width: '92%',
        height: 250,
        marginStart: -15,
        elevation: 1,
        shadowOpacity: 0.1,
    },

    header: {
        height: Helpers.WP(12),
        width: '100%',
        backgroundColor: Helpers.Theme.black,
        flexDirection: 'row',
        alignItems: 'center',
        paddingHorizontal: Helpers.WP(4)
    },
    headingTxt: {
        color: Helpers.Theme.light,
        marginLeft: Helpers.WP(5),
        fontWeight: '600'

    },
    resetTxt: {
        color: Helpers.Theme.blueBtnBg,
        fontWeight: '600'

    },
    resetBtn: {
        position: 'absolute', right: Helpers.WP(5)
    },

    scrollVw: {
        width: '100%'
    },
    topVw: {
        flexDirection: 'row',
        width: '100%',
        padding: Helpers.WP(5)
    },

    topVwTxt: {
        fontWeight: '500'
    },
    nearmeBtn: {
        backgroundColor: Helpers.Theme.green,
        position: "absolute", right: Helpers.WP(5),
        alignSelf: 'center',
        paddingVertical: Helpers.WP(1.5),
        paddingHorizontal: Helpers.WP(5),
        borderRadius: Helpers.WP(1)
    },
    nearmeTxt: {
        color: Helpers.Theme.light,
        fontWeight: '700'
    },
    propertytypeVw: {
        height: Helpers.WP(14), width: "100%",
        borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        backgroundColor: Helpers.Theme.introBg,
        marginTop: Helpers.WP(2),
        flexDirection: 'row', alignItems: 'center'
    },
    segmentBtn: {
        width: Helpers.WP(33),height:Helpers.WP(14),
        alignItems: 'center',
        justifyContent: 'center'
    },
    selectedsegmentBtn: {
        backgroundColor: Helpers.Theme.blueBtnBg,
        height: '100%', borderRadius: Helpers.WP(1),
        width: Helpers.WP(33),
        alignItems: 'center', justifyContent: 'center',


    },
    darkTxt: {
        color: Helpers.Theme.black,
        textAlign: "center"
    },
    lightTxt: {
        color: Helpers.Theme.light,
        textAlign: "center",
        fontWeight: '600'

    },

    keywordVw: {
        height: Helpers.WP(10), width: '90%',
        marginVertical: Helpers.WP(6),
        marginHorizontal: Helpers.WP(5),
    },
    keywordupperVw: {
        flexDirection: 'row',
        alignItems: "center"
    },
    searchIcon: {
        height: Helpers.WP(4.5),
        width: Helpers.WP(4.5),
        resizeMode: "contain",
        tintColor:"#B3B6B7"
    },
    keywordTxt: {
        marginLeft: Helpers.WP(2),
        fontWeight: '400'
    },
    keywordInputVw: {
        marginTop: Helpers.WP(2),
        height: '100%',
        width: '100%',
        backgroundColor: Helpers.Theme.introBg,
        borderColor: Helpers.Theme.blueBtnBorder,
        borderWidth: 1,
        borderRadius: Helpers.WP(1),
        paddingHorizontal: Helpers.WP(2),

    },
    offerTypeVw: {
        marginTop: Helpers.WP(2),
        height: '100%',
        width: '100%',
        paddingHorizontal: Helpers.WP(2),
        paddingVertical: Helpers.WP(1),
    },
    porpertycategoriesVw: {
        width: '100%',
        backgroundColor: Helpers.Theme.introBg,
        marginTop: Helpers.WP(7),
        borderWidth: 1,
        borderColor: Helpers.Theme.blueBtnBorder,
        padding: Helpers.WP(5)
    },

    porpertycategoriesList: {
        width: '100%',
        marginTop: Helpers.WP(3)
    },
    porpertycategoriesListMainVw: {
        width: Helpers.WP(23),
        marginRight: Helpers.WP(2),
        alignItems: 'center',
    },
    porpertycategoriesListSubVw: {
        height: Helpers.WP(16),
        backgroundColor: "#fff",
        borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        width: Helpers.WP(16), borderRadius: Helpers.WP(8),
        justifyContent: 'center', alignItems: 'center'
    },
    porpertycategoriesListSelecteditemVw: {
        height: Helpers.WP(6), width: Helpers.WP(6),
        backgroundColor: Helpers.Theme.blueBtnBg,
        borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        borderRadius: Helpers.WP(3),
        position: 'absolute', right: Helpers.WP(-2), top: Helpers.WP(-0.5),
        zIndex: 1, justifyContent: 'center', alignItems: 'center'
    },
    porpertycategoriesListSelecteditemImg: {
        height: Helpers.WP(3),
        width: Helpers.WP(3)
    },

    porpertycategoriesListitemImg: {
        height: Helpers.WP(8),
        width: Helpers.WP(8)
    },
    porpertycategoriesListitemTxt: {
        fontSize: Helpers.WP(3),
        marginTop: Helpers.WP(1),
        textAlign: 'center'
    },
    subcatlistBtn: {
        height: Helpers.WP(8),
        backgroundColor: Helpers.Theme.light,
        marginRight: Helpers.WP(2),
        paddingHorizontal: Helpers.WP(3),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Helpers.WP(4),
        borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
    },

    subcatlistOfferBtn: {
        height: Helpers.WP(8),
        backgroundColor: Helpers.Theme.introBg,
        marginRight: Helpers.WP(2),
        paddingHorizontal: Helpers.WP(3),
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: Helpers.WP(4),
        borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
    },
    upspace: {
        marginTop: Helpers.WP(5)
    },

    modaBoxInnerContainer:{
        marginTop:WP(5),
        height:200,
      },
      modalScrollviewContainer:{
         width:'100%',
        //  paddingBottom:15,
        
       },
       modalItemContainer:{
        marginTop:WP(3),
        flex:1,
         
       },

       modalDataTextContainer:{
        // marginTop:15,
        // paddingBottom:5,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'flex-start',
      },

      modalText:{

        fontSize:WP(4),
        alignSelf:'center',
        // color:Appearences.Colors.grey,
        
      
      },
})
