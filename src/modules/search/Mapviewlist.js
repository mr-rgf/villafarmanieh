import React, { Component } from 'react';
import {View} from 'react-native'

/*import * as Actions from '../../store/actions/allActions'
import * as Helpers from '../../helpers/Exporter'
import { WP as wp } from '../../helpers/Exporter'
import { connect } from 'react-redux'
// import { Marker, Callout } from 'react-native-maps';
// import { ClusterMap } from 'react-native-cluster-map';
import RNPickerSelect from 'react-native-picker-select';
import { styles } from './Mapviewlist.style'
import MapView from "react-native-map-clustering";
import { Marker, Callout } from "react-native-maps";*/


class Mapviewlist extends Component {
    /*constructor(props) {
        super(props)
        this.state = {
            currenyTypes: [
                { label: 'USD', value: 'USD' },
                { label: 'CAD', value: 'CAD' },
            ],
            defaultRegion: {
                latitude: 39.743943,
                longitude: -105.020089,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421,
            },
            maplist: [],
            showBottom: false,
            currentItem: null
        }

    }
    componentDidMount() {
        this.fetchData()

    }
    fetchData = () => {
        var maplistx = this.props.searchlist.filter((item) => item.latt != null)
        this.setState({ maplist: maplistx })
    }

    goback = () => {
        this.props.navigation.goBack()
    }

    markerPressed = (item) => {

        this.setState({ currentItem: item, showBottom: true })
    }
    openExternalApp = (which, val) => {
        switch (which) {
            case 'whatsapp':
                Linking.openURL(`whatsapp://send?phone=${val}`)
                return
            case 'phone':
                Linking.openURL(`tel:${val}`)
                return
            default:
                return
        }

    }

    navigate = (where, id) => {
        switch (where) {
            case 'filter':
                this.props.navigation.navigate(where)
                return
            case 'listingdetail':
                this.props.navigation.navigate(where, { listingId: id })
                return
            case 'mapviewlist':
                this.props.navigation.navigate(where)
                return
        }
    }
*/
    render() {

        return (
            <View>
               {/*  Header start
                <View style={styles.header}>
                    <Helpers.BackButtonRound navigation={this.props.navigation} />

                </View>
                 Header end

                 Map start
                 {
                    this.props.loading ? [] : [

                <MapView
                    onPress={() => this.setState({ showBottom: false })}
                    initialRegion={this.state.maplist.length > 0 ?
                        {
                            latitude: parseFloat(this.state.maplist[0].latt),
                            longitude: parseFloat(this.state.maplist[0].lon),
                            latitudeDelta: 0.0922,
                            longitudeDelta: 0.0421,
                        } :
                        this.state.defaultRegion}

                    style={Helpers.GLOBAL_SHEET.maxHW}>
                    {
                        this.state.maplist.map((item) => {
                            return (
                                <Marker
                                    key={item.property_id}
                                    coordinate={{ latitude: parseFloat(item.latt), longitude: parseFloat(item.lon) }}
                                    onPress={() => this.markerPressed(item)}
                                >
                                    <Image
                                        source={Helpers.Images.customMarker}
                                        style={{ height: Helpers.WP(12), width: Helpers.WP(12) }}
                                    />
                                     <Callout>
                                        <View style={{ height: Helpers.WP(30), width: Helpers.WP(80), alignItems: 'center', backgroundColor: Helpers.Theme.light }}>
                                            <Text >
                                            <Image
                                                source={{uri:item.property_img}}
                                                style={{ width: 50, height: 100 }}                                            />
                                            </Text>
                                            <Text>{item.title}</Text>

                                        </View>
                                    </Callout>
                                </Marker>)
                        })
                    }


                </MapView>
                {
                    this.state.showBottom ? [
                        <View style={styles.listBtn}>
                            <TouchableOpacity
                                onPress={() => this.navigate('listingdetail', this.state.currentItem.property_id)}
                                style={styles.listBtn_leftVw}>
                                <Image
                                    source={{ uri: this.state.currentItem.property_img }}
                                    style={Helpers.GLOBAL_SHEET.maxHW}
                                />

                                <View style={[styles.listBtn_leftVw_lblVw, { backgroundColor: this.state.currentItem.offer_color }]}>
                                    <Text style={[{ color: Helpers.Theme.light }, Helpers.Typography.three]}>{this.state.currentItem.offer}</Text>
                                </View>
                                {
                                    this.state.currentItem.is_featured == '1' ? [
                                        <Image
                                            source={Helpers.Images.featuredBadge}
                                            style={styles.listBtn_leftVw_featureImg}

                                        />
                                    ] : []
                                }

                            </TouchableOpacity>

                            <View style={styles.listBtn_rightVw}>

                                <TouchableOpacity
                                    style={styles.listBtn_rightVw_heartBtn}>
                                    <Image
                                        source={this.state.currentItem.isfav ? Helpers.Images.heartfilled : Helpers.Images.heartEmpty}
                                        style={[styles.listBtn_rightVw_heartBtn_Img, this.state.currentItem.isfav ? { tintColor: Helpers.Theme.red } : {}]}
                                    />
                                </TouchableOpacity>

                                <Text style={[Helpers.Typography.four, { color: this.props.settings.primary_clr }]}>{this.state.currentItem.main_price}
                                    <Text style={[Helpers.Typography.three, { color: Helpers.Theme.black }]}> /{this.state.currentItem.after_prefix}</Text>
                                </Text>
                                <Text style={[Helpers.Typography.four, styles.listBtn_rightVw_titleTxt]}>
                                    {this.state.currentItem.title}
                                </Text>
                                {
                                    this.state.currentItem.address != null ? [
                                        <View style={styles.listBtn_rightVw_addressVw}>
                                            <Image
                                                source={Helpers.Images.pinpoint}
                                                style={{ height: wp(2.5), width: wp(2.5), tintColor: this.props.settings.primary_clr, resizeMode: 'contain' }}
                                            />
                                            <Text style={[Helpers.Typography.three, { color: Helpers.Theme.darkgrey, width: wp(45), marginLeft: wp(1) }]}>
                                                {this.state.currentItem.address}
                                            </Text>
                                        </View>

                                    ] : []
                                }


                                <View style={styles.listBtn_rightVw_bedbathVw}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image
                                            source={Helpers.Images.bedsSearchList}
                                            style={[styles.listBtn_rightVw_bedbathVw_Img, { tintColor: this.props.settings.primary_clr }]}
                                        />
                                        <Text style={[Helpers.Typography.three, { color: Helpers.Theme.darkgrey, }]}>
                                            {this.state.currentItem.rooms}
                                        </Text>
                                    </View>
                                    <View style={styles.listBtn_rightVw_bedbathVw_twoVws}>
                                        <Image
                                            source={Helpers.Images.bathsSearchList}
                                            style={[styles.listBtn_rightVw_bedbathVw_Img, { tintColor: this.props.settings.primary_clr }]}
                                        />
                                        <Text style={[Helpers.Typography.three, styles.listBtn_rightVw_bedbathVw_bedsTxt]}>
                                            {this.state.currentItem.baths}
                                        </Text>
                                    </View>
                                    <View style={styles.listBtn_rightVw_bedbathVw_twoVws}>
                                        <Image
                                            source={Helpers.Images.sqft}
                                            style={[styles.listBtn_rightVw_bedbathVw_Img, { tintColor: this.props.settings.primary_clr }]}
                                        />

                                        <Text style={[Helpers.Typography.three, styles.listBtn_rightVw_bedbathVw_bedsTxt]}>
                                            {this.state.currentItem.area}
                                        </Text>
                                    </View>

                                </View>

                                <View style={styles.listBtn_rightVw_buttonsVw}>
                                    {
                                        this.state.currentItem.has_whatsapp_number ? [
                                            <TouchableOpacity
                                                onPress={() => this.openExternalApp('phone', this.state.currentItem.whatsapp_number.number)}

                                                style={styles.listBtn_rightVw_buttonsVw_firstBtn}>
                                                <Image
                                                    source={Helpers.Images.whatsapp}
                                                    style={styles.listBtn_rightVw_buttonsVw_firstBtn_Img}
                                                />
                                                <Text style={{ color: Helpers.Theme.whatsapp }}>{this.state.currentItem.whatsapp_number.text}</Text>
                                            </TouchableOpacity>
                                        ] : []
                                    }
                                    {
                                        this.state.currentItem.has_mobile_number ? [
                                            <TouchableOpacity
                                                onPress={() => this.openExternalApp('phone', this.state.currentItem.mobile_number.number)}
                                                style={[styles.listBtn_rightVw_buttonsVw_secondBtn, { backgroundColor: this.props.settings.primary_clr }]}>
                                                <Image
                                                    source={Helpers.Images.phonecall}
                                                    style={styles.listBtn_rightVw_buttonsVw_secondBtn_Img}
                                                />
                                                <Text style={{ color: Helpers.Theme.light }}>{this.state.currentItem.mobile_number.text}</Text>
                                            </TouchableOpacity>
                                        ] : []
                                    }

                                </View>
                            </View>

                        </View>

                    ] : []
                }


                 ]
                 }


                 Map end */}
            </View>
        );
    }
}


/*const mapStateToProps = (state) => {
    return {
        loading: state.intro.loading,
        settings: state.intro.settings,
        searchlist: state.search.searchlist,
        maplist: state.search.maplist,
        searchresponse: state.search.searchresponse

    }

}

const mapDispatchToProps = (dispatch) => {
    return {
        updateMaplist: params => dispatch(Actions.updateMapList(params))
    }
}*/

export default Mapviewlist;
