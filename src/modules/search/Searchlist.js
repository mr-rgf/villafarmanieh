import React, {Component} from 'react';
import {
    View, Text, TouchableOpacity,
    FlatList, Image, Linking,
} from 'react-native';

import * as Actions from '../../store/actions/allActions';
import * as Helpers from '../../helpers/Exporter';
import {Typography, WP, WP as wp} from '../../helpers/Exporter';
import {connect} from 'react-redux';
import {styles} from './Searchlist.style';
import Api from '../../network/Api';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import ModalBox from 'react-native-modalbox';
import Strings from '../../assets/string';

class Searchlist extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchlist: [
                {
                    id: 0,
                    title: 'Diamond Manor Apartment',
                    price: '70,000',
                    address: 'Hackersnack, AX, USA',
                    listingType: 'Apartment',
                    baths: '02',
                    beds: '03',
                    sqft: '1200',
                    isFeatured: false,
                    showLabel: true,
                    label: 'Open house',
                    currency: 'PKR',
                    img: Helpers.Images.dummylistItem_one,

                    isfav: true,
                    label_background: 'purple',


                },
                {
                    id: 1,
                    title: 'Diamond Manor Apartment',
                    price: '70,000',
                    address: 'Hackersnack, AX, USA',
                    listingType: 'Apartment',
                    baths: '02',
                    beds: '03',
                    sqft: '2400',
                    isFeatured: false,
                    showLabel: true,
                    label: 'Hot Offer',

                    currency: 'PKR',
                    img: Helpers.Images.dummylistItem_two,

                    isfav: false,
                    label_background: 'red',


                },
                {
                    id: 2,
                    title: 'Diamond Manor Apartment',
                    price: '70,000',
                    address: 'Hackersnack, AX, USA',
                    listingType: 'Apartment',
                    baths: '02',
                    beds: '03',
                    sqft: '1450',
                    isFeatured: false,
                    showLabel: true,
                    label: 'Hot Offer',
                    currency: 'PKR',
                    img: Helpers.Images.dummylistItem_one,

                    isfav: true,
                    label_background: 'red',


                },


            ],


        };
        this.fetchData();
    }

    fetchData = async (page) => {
        var response;
        var pagination = false;
        this.props.setloading(true);
        let param = this.props.searchparams;

        if (page == undefined) {
            param.next_page = '';
            response = await Api.post('search', param);
        } else {
            param.next_page = page;

            response = await Api.post('search', param);
            pagination = true;
        }
        if (response.success == true) {
            this.props.updatelist({data: response.data.final_content, pagination: pagination});
        }
        this.props.setloading(false);
    };

    navigate = (where, id) => {
        switch (where) {
            case 'filter':
                this.props.navigation.navigate(where);
                return;
            case 'listingdetail':
                this.props.navigation.navigate(where, {listingId: id});
                return;
            case 'mapviewlist':
                this.props.navigation.navigate(where);
                return;
        }
    };
    loadMore = () => {
        let {pagination} = this.props.searchresponse;

        if (pagination.has_next_page) {
            this.fetchData(++pagination.current_page);
        }
    };
    onRefresh = () => {
        this.fetchData();
    };

    openExternalApp = (which, val) => {
        switch (which) {
            case 'whatsapp':
                Linking.openURL(`whatsapp://send?phone=${val}`);
                return;
            case 'phone':
                Linking.openURL(`tel:${val}`);
                return;
            default:
                return;
        }

    };

    openSortingModel = () => {
        this.refs.sortingModal.open();
    };


    onSortPress = (item) => {
        this.refs.sortingModal.close();
        var param = {
            'sort_by': item.key,
        };
        this.fetchSearchData(param);
    };


    fetchSearchData = async (param) => {
        var response;
        this.props.setloading(true);

        let params = this.props.searchparams;
        params.sort_by = param.sort_by;

        response = await Api.post('search', params);

        if (response.success == true) {
            await this.props.updatelist({data: response.data.final_content});
            this.props.updateSP(params);

        } else {
            await this.props.updatelist({data: response.data});
        }
        this.props.setloading(false);

    };

    render() {
        // if (this.props.loading) {
        //     return (
        //         <View style={[Helpers.GLOBAL_SHEET.maxHWC, styles.container]}>
        //             <View style={[styles.mainHeader, styles.boxWithShadow]}>

        //                 <View style={styles.header}>
        //                     <TouchableOpacity
        //                         onPress={() => {
        //                             this.props.navigation.goBack()
        //                         }}
        //                         style={styles.backBtn}
        //                     >
        //                         <Helpers.leftArrow color={Helpers.Theme.black} />
        //                     </TouchableOpacity>

        //                     <Image
        //                         source={Helpers.Images.propertyaLogo}
        //                         style={styles.mainlogo}
        //                     />


        //                 </View>

        //                 <View style={[styles.header, styles.secondpartHeader]}>


        //                 </View>


        //             </View>
        //             <FlatList
        //                 data={this.state.searchlist}
        //                 keyExtractor={item => item.id}
        //                 showsVerticalScrollIndicator={false}

        //                 renderItem={({ item, index }) => {
        //                     return (
        //                         <SkeletonPlaceholder style={Helpers.GLOBAL_SHEET.maxHWCC}>


        //                             <View>
        //                                 <SkeletonPlaceholder.Item height={wp(46)} width={wp(98)} >
        //                                     <View style={styles.listBtn}>
        //                                         <View
        //                                             style={styles.listBtn_leftVw}>
        //                                             <View
        //                                                 style={Helpers.GLOBAL_SHEET.maxHW}
        //                                             />


        //                                         </View>
        //                                         <View style={{ marginLeft: wp(2), marginTop: wp(2) }}>
        //                                             <SkeletonPlaceholder.Item height={wp(5)} width={wp(40)} />
        //                                             <SkeletonPlaceholder.Item height={wp(5)} width={wp(30)} marginTop={wp(2)} />
        //                                             <SkeletonPlaceholder.Item height={wp(5)} width={wp(20)} marginTop={wp(2)} />

        //                                         </View>
        //                                         <View style={{ flexDirection: 'row', position: 'absolute', bottom: wp(1), right: wp(2) }}>
        //                                             {/* <View style={{height:wp(32),width:wp}}></View> */}
        //                                             <View style={[styles.listBtn_rightVw_buttonsVw_firstBtn, { borderColor: '#fff', marginRight: wp(1) }]}>

        //                                             </View>
        //                                             <View style={[styles.listBtn_rightVw_buttonsVw_firstBtn, { borderColor: '#fff' }]}>

        //                                             </View>
        //                                         </View>


        //                                     </View>


        //                                 </SkeletonPlaceholder.Item>

        //                             </View>

        //                         </SkeletonPlaceholder>

        //                     )
        //                 }} />


        //         </View>

        //     )
        // }
        return (
            <View style={[Helpers.GLOBAL_SHEET.maxHWC, styles.container]}>
                {/* Header start */}
                <View style={[styles.mainHeader, styles.boxWithShadow]}>
                    <View style={[styles.header, {marginBottom: wp(2)}]}>
                        <TouchableOpacity
                            style={[styles.backBtn, {paddingTop: wp(2)}]}>
                            <Image
                                source={Helpers.Images.propertyaLogo}
                                style={styles.mainlogo}
                            />
                        </TouchableOpacity>

                        <View style={[styles.backBtn2, {paddingTop: wp(3)}]}>
                            <View style={{
                                display: 'flex',
                                flexDirection: 'row',
                            }}>

                                <TouchableOpacity
                                    onPress={() => this.openSortingModel()}
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginRight: wp(2),
                                    }}>

                                    <Image
                                        source={Helpers.Images.sortIcon}
                                        style={[styles.secondpartHeader_BtnVw_firstBtn_Img, {
                                            height: wp(7),
                                            width: wp(7),
                                        }]}
                                    />

                                </TouchableOpacity>

                                <TouchableOpacity
                                    style={{
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center',
                                        marginRight: wp(2),
                                    }}
                                    onPress={() => {
                                        this.refs.contactModal.open();
                                    }}>
                                    <Image
                                        source={Helpers.Images.callICon}
                                        style={[styles.secondpartHeader_BtnVw_firstBtn_Img, {
                                            height: wp(7),
                                            width: wp(7),
                                        }]}
                                    />
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onPress={() => this.navigate('filter')}
                                    style={[styles.secondpartHeader_BtnVw_secondBtn, {backgroundColor: this.props.settings.primary_clr}]}
                                >
                                    <Text
                                        style={styles.secondpartHeader_BtnVw_secondBtn_Txt}>{Strings.filter}</Text>
                                </TouchableOpacity>


                            </View>

                        </View>

                    </View>
                </View>

                {/* Header end */}
                {
                    this.props.searchlist == undefined ? [
                        <View style={[Helpers.GLOBAL_SHEET.maxHWCC, {height: '85%'}]}>
                            <Text>{this.props.searchresponse.message}</Text>
                        </View>,
                    ] : [
                        <View style={styles.mainbody}>
                            {/* List start */}
                            <FlatList
                                data={this.props.searchlist}
                                keyExtractor={item => item.id}
                                onEndReached={() => this.loadMore()}
                                onRefresh={() => this.onRefresh()}
                                refreshing={this.props.loading}

                                renderItem={({item, index}) => {
                                    return (
                                        <View style={styles.listBtn}>
                                            <TouchableOpacity
                                                onPress={() => this.navigate('listingdetail', item.property_id)}
                                                style={styles.listBtn_leftVw}>
                                                <Image
                                                    source={{uri: item.property_img}}
                                                    style={Helpers.GLOBAL_SHEET.maxHW}
                                                />

                                                {item.offer ? <View
                                                    style={[styles.listBtn_leftVw_lblVw, {backgroundColor: item.offer_color}]}>
                                                    <Text
                                                        style={[{color: Helpers.Theme.light}, Helpers.Typography.three]}>{item.offer}</Text>
                                                </View> : []}
                                                {
                                                    item.is_featured == '1' ? [
                                                        <Image
                                                            source={Helpers.Images.featuredBadge}
                                                            style={styles.listBtn_leftVw_featureImg}
                                                        />,
                                                    ] : []
                                                }

                                            </TouchableOpacity>

                                            <View style={styles.listBtn_rightVw}>


                                                <TouchableOpacity
                                                    onPress={() => this.navigate('listingdetail', item.property_id)}
                                                    style={[styles.listBtn_rightVw_titleTxt, {marginBottom: 10}]}>
                                                    <Text style={[Helpers.Typography.four]}>
                                                        {item.title}
                                                    </Text>
                                                </TouchableOpacity>

                                                <View style={{
                                                    backgroundColor: '#f7f7f7',
                                                    alignSelf: 'flex-start',
                                                    paddingRight: wp(1),
                                                    paddingLeft: wp(1),
                                                    paddingBottom: wp(.5),
                                                }}>
                                                    <Text
                                                        style={[{
                                                            color: '#555555',
                                                            backgroundColor: '#f7f7f7',
                                                            fontSize: WP(3.5),
                                                        }]}
                                                    >{item.main_price}
                                                        <Text
                                                            style={[Helpers.Typography.three, {color: Helpers.Theme.black}]}> {item.after_prefix}</Text>
                                                    </Text>
                                                </View>


                                                {
                                                    item.address != null ? [
                                                        <View style={styles.listBtn_rightVw_addressVw}>
                                                            <Image
                                                                source={Helpers.Images.map_flag}
                                                                style={{
                                                                    height: wp(2.5),
                                                                    width: wp(2.5),
                                                                    tintColor: this.props.settings.primary_clr,
                                                                    resizeMode: 'contain',
                                                                }}
                                                            />
                                                            <Text style={[Helpers.Typography.three, {
                                                                color: Helpers.Theme.darkgrey,
                                                                width: wp(45),
                                                                marginLeft: wp(1),
                                                            }]}>
                                                                {item.address}
                                                            </Text>
                                                        </View>,

                                                    ] : []
                                                }


                                                <View style={[styles.listBtn_rightVw_bedbathVw, {marginTop: wp(5)}]}>
                                                    <View style={{flexDirection: 'row'}}>
                                                        <Image
                                                            source={Helpers.Images.menu}
                                                            style={[styles.listBtn_rightVw_bedbathVw_Img, {tintColor: this.props.settings.primary_clr}]}
                                                        />
                                                        <Text
                                                            style={[Helpers.Typography.three, {color: Helpers.Theme.darkgrey}]}>
                                                            {item.type}
                                                        </Text>
                                                    </View>

                                                    <View style={styles.listBtn_rightVw_bedbathVw_twoVws}>
                                                        <Image
                                                            source={Helpers.Images.area_select}
                                                            style={[styles.listBtn_rightVw_bedbathVw_Img, {tintColor: this.props.settings.primary_clr}]}
                                                        />

                                                        <Text
                                                            style={[Helpers.Typography.three, styles.listBtn_rightVw_bedbathVw_bedsTxt]}>
                                                            {item.area}
                                                        </Text>
                                                    </View>

                                                </View>

                                            </View>

                                        </View>

                                    );
                                }}
                            />

                            {/* List end */}


                        </View>,

                    ]
                }


                <ModalBox
                    style={[styles.modalBoxStyle,{height:wp(40)}]}
                    position={'center'}
                    ref={'sortingModal'}>
                    <View style={styles.modalContentContainer}>
                        <View style={[styles.modaBoxInnerContainer]}>
                            {

                                this.state.loadingLocations ?
                                    <View style={{position: 'absolute', right: 0, left: 0, bottom: 0, top: 0}}>
                                        <Helpers.Lumper lumper={true} color={currentDetail.primary_clr}/>
                                    </View> : [
                                        <View>
                                            {
                                                this.props.settings.sorting.option_dropdown.map((item, i) => {
                                                    return (
                                                        <View key={i} style={{marginTop: wp(1)}}>
                                                            <TouchableOpacity
                                                                onPress={() => this.onSortPress(item)}
                                                                style={[styles.modalDataTextContainer]}>
                                                                <Text
                                                                    style={[styles.modalText, {color: '#000'}]}>{item.value}</Text>
                                                            </TouchableOpacity>
                                                        </View>

                                                    );


                                                })
                                            }
                                        </View>,
                                    ]
                            }

                        </View>
                    </View>
                </ModalBox>

                <ModalBox
                    style={[styles.modalBoxStyle, {height: wp(95)}]}
                    position={'center'}
                    ref={'contactModal'}>
                    <View style={styles.modalContentContainer}>
                        <Image style={{
                            width: wp(30),
                            height: wp(30),
                            resizeMode: 'contain',
                        }} source={Helpers.Images.modalLogo}/>

                        <View style={styles.listBtn_rightVw_addressVw}>
                            <Image
                                source={Helpers.Images.locationIcon}
                                style={{
                                    height: wp(5),
                                    width: wp(5),
                                    resizeMode: 'contain',
                                }}
                            />
                            <Text style={[Helpers.Typography.three, {
                                color: Helpers.Theme.black,
                                width: wp(45),
                                marginLeft: wp(1),
                                fontSize: wp(3),
                            }]}>
                                {Strings.address}
                            </Text>
                        </View>

                        <View style={{marginTop: wp(6)}}>
                            <TouchableOpacity
                                onPress={() => {
                                    Linking.openURL('tel://01144672814');
                                }}
                                style={[styles.secondpartHeader_BtnVw_firstBtn, {
                                    alignItems: 'center',
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row',
                                    width: wp(50),
                                    marginBottom: wp(2),
                                    paddingTop: wp(4),
                                    backgroundColor: '#f7f7f7',
                                    paddingBottom: wp(4),
                                    borderWidth: 0,
                                    paddingLeft: wp(2),
                                }]}>
                                <Image
                                    source={Helpers.Images.phone}
                                    style={[styles.secondpartHeader_BtnVw_firstBtn_Img, {
                                        height: wp(5),
                                        width: wp(5),
                                    }]}
                                />
                                <Text
                                    style={[Helpers.Typography.three,
                                        {
                                            marginLeft: wp(1),
                                            width: '100%',
                                            position: 'absolute',
                                            left: 0,
                                            right: 0,
                                            textAlign: 'center',
                                        },
                                    ]}>{Strings.ContactSales}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    Linking.openURL('tel://09111275018');
                                }}
                                style={[styles.secondpartHeader_BtnVw_firstBtn, {
                                    alignItems: 'center',
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row',
                                    width: wp(50),
                                    marginBottom: wp(2),
                                    paddingTop: wp(4),
                                    backgroundColor: '#f7f7f7',
                                    paddingBottom: wp(4),
                                    borderWidth: 0,
                                    paddingLeft: wp(2),
                                }]}>
                                <Image
                                    source={Helpers.Images.ContactSupport}
                                    style={[styles.secondpartHeader_BtnVw_firstBtn_Img, {

                                        height: wp(5),
                                        width: wp(5),
                                    }]}
                                />
                                <Text
                                    style={[Helpers.Typography.three,
                                        {
                                            marginLeft: wp(1),
                                            width: '100%',
                                            position: 'absolute',
                                            left: 0,
                                            right: 0,
                                            textAlign: 'center',
                                        },
                                    ]}>{Strings.ContactSupport}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    Linking.openURL('tel://09113200794');
                                }}
                                style={[styles.secondpartHeader_BtnVw_firstBtn, {
                                    alignItems: 'center',
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row',
                                    width: wp(50),
                                    marginBottom: wp(2),
                                    paddingTop: wp(4),
                                    backgroundColor: '#f7f7f7',
                                    paddingBottom: wp(4),
                                    borderWidth: 0,
                                    paddingLeft: wp(2),
                                }]}>
                                <Image
                                    source={Helpers.Images.manager}
                                    style={[styles.secondpartHeader_BtnVw_firstBtn_Img, {
                                        height: wp(5),
                                        width: wp(5),
                                    }]}
                                />
                                <Text
                                    style={[Helpers.Typography.three,
                                        {
                                            marginLeft: wp(1),
                                            width: '100%',
                                            position: 'absolute',
                                            left: 0,
                                            right: 0,
                                            textAlign: 'center',
                                        },
                                    ]}>{Strings.ContactAdmin}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => {
                                    Linking.openURL('https://instagram.com/villa.farmanieh1');
                                }}
                                style={[styles.secondpartHeader_BtnVw_firstBtn, {
                                    alignItems: 'center',
                                    justifyContent: 'flex-start',
                                    flexDirection: 'row',
                                    width: wp(50),
                                    marginBottom: wp(2),
                                    paddingTop: wp(4),
                                    backgroundColor: '#f7f7f7',
                                    paddingBottom: wp(4),
                                    borderWidth: 0,
                                    paddingLeft: wp(2),
                                }]}>
                                <Image
                                    source={Helpers.Images.instagramIcon}
                                    style={[styles.secondpartHeader_BtnVw_firstBtn_Img, {
                                        height: wp(5),
                                        width: wp(5),
                                    }]}
                                />
                                <Text
                                    style={[Helpers.Typography.three, {
                                        marginLeft: wp(1),
                                        width: '100%',
                                        position: 'absolute',
                                        left: 0,
                                        right: 0,
                                        textAlign: 'center',
                                    }]}>{Strings.instagram}</Text>

                            </TouchableOpacity>
                        </View>


                    </View>

                </ModalBox>

            </View>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        loading: state.intro.loading
        ,
        settings: state.intro.settings
        ,
        searchlist: state.search.searchlist
        ,
        searchresponse: state.search.searchresponse
        ,

        filters: state.filters.filtersresponse
        ,
        searchparams: state.filters.searchparams
        ,

    };

};

const mapDispatchToProps = (dispatch) => {
    return {
        saveUser: params => dispatch(Actions.saveUser(params)),
        setloading: params => dispatch(Actions.loading(params)),
        updatelist: params => dispatch(Actions.updateList(params)),
        updateSP: params => dispatch(Actions.updateSearchParams(params)),


    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Searchlist);
