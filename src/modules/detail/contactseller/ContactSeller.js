import React, { Component } from 'react';
import {
    View, Text, StyleSheet, TextInput
} from 'react-native'
import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import CountryPicker, { FlagButton } from 'react-native-country-picker-modal'
import { TouchableOpacity } from 'react-native-gesture-handler';
import { styles } from './ContactSeller.style'
import Api from '../../../network/Api'

class ContactSeller extends Component {
    constructor(props) {
        super(props);
        this.state = {
            countryCode: 'PK',
            country: 0,
            cca2: 'PK',
            isVisible: false,

            name:'',
            email:'',
            phone:'',
            message:'',

            showerror:false,
            loading:false
        }
    }
    onSelect = (country) => {

        this.setState({
            countryCode: country.cca2,
            country: country,
            isVisible: false
        })
    };

    handleInputChange=(key,value)=>{
        this.setState({[key]:value})
    }

    contactSeller = async () => {
        var isvalid = this.validate()
        var response
        
        var {property_id} = this.props.route.params


        if (isvalid) {
            let param = {
                "property_id": property_id,
                "c_username": this.state.name,
                "c_email": this.state.email,
                "contact-no": this.state.phone,
                "c_msg": this.state.message,
              
            }
            this.setState({loading:true})
            response = await Api.post("contact-author", param)
            Helpers.showToast(response.data.message+"")
            
            this.setState({loading:false,name:'',email:'',phone:'',message:""})

            
        } else {
            this.setState({ showerror: true })
            Helpers.showToast(this.props.settings.required_fields)

        }
    }

    validate = () => {
       
        if (this.state.name == '') {
            return false
        }
        if (this.state.email == '') {
            return false
        }
        if (this.state.phone == '') {
            return false
        }
        if (this.state.message == '') {
            return false
        }
        return true
    }




    render() {

        let { value, title } = this.props.route.params

        return (
            <View style={Helpers.GLOBAL_SHEET.maxHWC}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}

                />
                <View style={[Helpers.GLOBAL_SHEET.maxHW, styles.container]}>

                    <View style={styles.firstrowVw}>
                        <View>
                            <Text>{value.name.field_title}</Text>
                            <TextInput
                                style={[styles.firstrowInput,this.state.showerror && this.state.name==''?{borderColor:'red'}:{}]}
                                value={this.state.name}
                                onChangeText={(value)=>this.handleInputChange('name',value)}
                            />
                        </View>

                        <View>
                            <Text>{value.email.field_title}</Text>
                            <TextInput
                                keyboardType="email-address"
                                style={[styles.firstrowInput,this.state.showerror && this.state.email==''?{borderColor:'red'}:{}]}

                                value={this.state.email}
                                onChangeText={(value)=>this.handleInputChange('email',value)}
                            />
                        </View>
                    </View>


                    <View style={styles.secondrowVw}>
                        <View>
                            <Text>{value.contact.field_title}</Text>
                            <View style={styles.flagVw}>

                                <FlagButton
                                    onOpen={() => this.setState({ isVisible: !this.state.isVisible })}
                                    withEmoji={true}
                                    countryCode={this.state.countryCode}
                                    withCallingCodeButton={true}
                                    containerButtonStyle={styles.flagBtnContainer}
                                    
                                />
                                <TextInput
                                    style={[styles.phoneInput,this.state.showerror && this.state.phone==''?{borderColor:'red',borderLeftWidth:1}:{}]}
                                    keyboardType="phone-pad"
                                    value={this.state.phone}
                                    onChangeText={(value)=>this.handleInputChange('phone',value)}
                                />
                                <CountryPicker
                                    withFilter={true}
                                    visible={this.state.isVisible}
                                    withFlag={true}
                                    withFlagButton={true}
                                    onSelect={(country) => this.onSelect(country)}
                                    withCallingCode={true}
                                    withAlphaFilter={true}
                                    withCountryNameButton={true}
                                    renderFlagButton={() => {
                                        return (
                                            null
                                        )
                                    }}
                                />
                            </View>
                        </View>

                    </View>

                    <View style={styles.secondrowVw}>
                        <View>
                            <Text>{value.msg.field_title}</Text>
                            <TextInput
                                multiline
                                blurOnSubmit
                                style={[styles.message,this.state.showerror && this.state.message==''?{borderColor:'red'}:{}]}
                                value={this.state.message}
                                onChangeText={(value)=>this.handleInputChange('message',value)}
                            />
                        </View>
                    </View>

                    <TouchableOpacity
                        style={styles.mainBtn}
                        onPress={() => this.contactSeller()}>

                        {
                            this.state.loading?[
                            <Helpers.Lumper lumper={true} color={Helpers.Theme.light} />

                            ]:[
                                <Text style={[styles.btnTxt]}>{value.btn_text}</Text>
                            ]
                        } 
                    </TouchableOpacity>


                </View>




            </View>
        );
    }
}




const mapStateToProps = (state) => {
    return {
        settings: state.intro.settings,

    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ContactSeller);