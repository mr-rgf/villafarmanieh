import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet, FlatList, TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import { WP as wp } from '../../../helpers/Exporter'
import { AirbnbRating } from 'react-native-ratings';

import { styles } from './ReviewRating.style'


class ReviewRating extends Component {


    constructor(props) {
        super(props)
        this.state = {
            reviews: [
                {
                    id: 0,
                    name: 'Johnson',
                    title: 'Lovely place to live',
                    description: 'Lovely place to live jjd skksksks ksdjksjdks kjsdjk j kdjskdjs kjsdkjskdj ksjdksjkdsjdks dksj dkjs kdjsk dskdjksjdksjkdjskdjskdj jksdjksjdkjs kdjskdjskd',
                    img: Helpers.Images.filetypedoc,
                    likes: 1,

                    authorResponse: {
                        date: '29 May, 2020',
                        response: 'Hi Johnson. skksksks ksdjksjdks kjsdjk j kdjskdjs kjsdkjskdj ksjdksjkdsjdks dksj dkjs kdjsk dskdjksjdksjkdjskdjskdj'
                    },
                    filesize: '51 KB'
                },


                {
                    id: 0,
                    name: 'Johnson',
                    title: 'Lovely place to live',
                    description: 'Lovely place to live jjd skksksks ksdjksjdks kjsdjk j kdjskdjs kjsdkjskdj ksjdksjkdsjdks dksj dkjs kdjsk dskdjksjdksjkdjskdjskdj jksdjksjdkjs kdjskdjskd',
                    img: Helpers.Images.filetypedoc,
                    likes: 1,

                    authorResponse: {
                        date: '29 May, 2020',
                        response: 'Hi Johnson. skksksks ksdjksjdks kjsdjk j kdjskdjs kjsdkjskdj ksjdksjkdsjdks dksj dkjs kdjsk dskdjksjdksjkdjskdjskdj'
                    },
                    filesize: '51 KB'
                },


                {
                    id: 0,
                    name: 'Johnson',
                    title: 'Lovely place to live',
                    description: 'Lovely place to live jjd skksksks ksdjksjdks kjsdjk j kdjskdjs kjsdkjskdj ksjdksjkdsjdks dksj dkjs kdjsk dskdjksjdksjkdjskdjskdj jksdjksjdkjs kdjskdjskd',
                    img: Helpers.Images.filetypedoc,
                    likes: 1,

                    authorResponse: {
                        date: '29 May, 2020',
                        response: 'Hi Johnson. skksksks ksdjksjdks kjsdjk j kdjskdjs kjsdkjskdj ksjdksjkdsjdks dksj dkjs kdjsk dskdjksjdksjkdjskdjskdj'
                    },
                    filesize: '51 KB'
                },

            ]
        }
    }


    render() {

        let { value, title } = this.props.route.params

        return (
            <View style={[Helpers.GLOBAL_SHEET.maxHWC, styles.container]}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}

                />
                <View style={styles.listparentVw}>
                    <FlatList
                        data={value}
                        keyExtractor={(item) => item.id}
                        renderItem={({ item, index }) => {

                            return (
                                <View style={styles.listitemVw}>
                                    <View style={styles.listitemVw_topVw}>

                                        <Image
                                            source={{ uri: item.review_dp }}
                                            style={styles.dp}
                                            borderRadius={wp(7)}
                                        />
                                        <View style={styles.margin2left}>
                                            <Text>{item.review_user}</Text>
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                <Image
                                                    source={Helpers.Images.clock}
                                                    style={styles.clock}
                                                />
                                                <Text style={[styles.margin1left, Helpers.Typography.three,{color:Helpers.Theme.darkgrey}]}>{item.review_date}</Text>

                                            </View>
                                        </View>
                                        <View style={styles.ratingVw}>
                                            <AirbnbRating
                                                count={5}
                                                defaultRating={parseFloat(item.review_stars)}
                                                size={wp(4)}
                                                showRating={false}
                                                starContainerStyle={{ marginTop: wp(2) }}
                                            />

                                        </View>

                                    </View>

                                    <View style={styles.padding4left}>
                                        <Text style={{ fontWeight: Helpers.Typography.weightBold }}>{item.review_title}  </Text>
                                        <Text style={[Helpers.Typography.three, styles.mainres_txt2]}>{item.review_desc}</Text>


                                        {/* Author response */}
                                        {
                                            item.has_reply?[
                                                <View style={styles.authorresponseVw}>
                                                <Text style={styles.authorresponseVw_txt1}>Author responded on 29 May, 2020  </Text>
                                                <Text style={[Helpers.Typography.three, styles.authorresponseVw_txt2]}>Booked a hotel through sunshine and all I can say is they have the worst customer service I've ever seen in my entire life. No running water for 4 days. I've contacted the management and they were silent</Text>
                                            </View>
                                            ]:[]
                                        }
                                      
                                        {/* Author response end */}



                                    </View>

                                    {/* Footer */}
                                    <View style={styles.footerVw}>
                                        <TouchableOpacity style={styles.likeBtn}>
                                            <Image
                                                source={Helpers.Images.like}
                                                style={styles.likeImg}
                                            />
                                            <Text style={[Helpers.Typography.three, styles.ml1mt1,{color:Helpers.Theme.darkgrey}]}>Like (1)</Text>
                                        </TouchableOpacity>


                                        <TouchableOpacity style={styles.loveBtn}>
                                            <Image
                                                source={Helpers.Images.heartfilled}
                                                style={styles.loveImg}
                                            />
                                            <Text style={[Helpers.Typography.three, styles.margin1left,{color:Helpers.Theme.darkgrey}]}>{item.review_love}</Text>
                                        </TouchableOpacity>


                                        <TouchableOpacity style={styles.dislikeBtn}>
                                            <Image
                                                source={Helpers.Images.dislike}
                                                style={styles.dislikeImg}
                                            />
                                            <Text style={[Helpers.Typography.three, styles.margin1left]}>{item.review_dislike}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )


                        }}
                    />
                </View>




            </View>
        );
    }
}





const mapStateToProps = (state) => {
    return {}

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ReviewRating);