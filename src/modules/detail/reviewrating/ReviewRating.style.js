import {StyleSheet} from 'react-native'
import * as Helpers from '../../../helpers/Exporter'
import {WP as wp} from '../../../helpers/Exporter'

export const styles = StyleSheet.create({
    container: {
        backgroundColor: Helpers.Theme.listingBG
    },

    likeBtn:{ flexDirection: "row", alignItems: 'center' },
    loveBtn:{ flexDirection: "row", alignItems: 'center' ,marginLeft:wp(3)},
    dislikeBtn:{ flexDirection: "row", alignItems: 'center' ,position:'absolute',right:wp(4)},

    likeImg:{ height: wp(5), width: wp(5), tintColor: 'orange', resizeMode: 'contain' },
    dislikeImg:{ height: wp(5), width: wp(5), tintColor: Helpers.Theme.gryBack, resizeMode: 'contain' },
    loveImg:{ height: wp(5.5), width: wp(5.5), tintColor: Helpers.Theme.red, resizeMode: 'contain' },

    ml1mt1: { marginLeft: wp(1), marginTop: wp(1) },

    footerVw:{
        height: wp(12), width: '100%',
        borderTopColor: Helpers.Theme.blueBtnBorder, borderTopWidth: 1
        , flexDirection: "row", alignItems: 'center', paddingHorizontal: wp(5)
    },


    margin2left:{
        marginLeft:wp(2)
    },


    margin1left:{
        marginLeft:wp(1)
    },
    padding4left:{ padding: wp(4) },

    ratingVw:{ position: 'absolute', right: wp(3), },

    listparentVw:{ marginTop: wp(5),marginBottom:wp(10) },
    listitemVw:{
        width: wp(90), backgroundColor: Helpers.Theme.light,marginBottom:wp(4),
        borderWidth: 1, borderRadius: wp(1), borderColor: Helpers.Theme.blueBtnBorder
        ,
    },
    listitemVw_topVw:{
        height: wp(20), width: '100%',
        paddingHorizontal: wp(3), alignItems: 'center',
        flexDirection: 'row', backgroundColor: Helpers.Theme.introBg,
        borderBottomWidth: 1, borderBottomColor: Helpers.Theme.blueBtnBorder
    },
    dp:{ height: wp(14), width: wp(14) },
    clock:{ height: wp(3), width: wp(3) },
    authorresponseVw:{ backgroundColor: Helpers.Theme.introBg, width: wp(80), marginTop: wp(3), padding: wp(3) },
    authorresponseVw_txt1:{ fontWeight: Helpers.Typography.weightBold} ,
    authorresponseVw_txt2:{ marginTop: wp(1), color: Helpers.Theme.darkgrey },


    mainres_txt2: { marginTop: wp(1), color: Helpers.Theme.darkgrey },

  
})
