import {StyleSheet} from 'react-native'
import * as Helpers from '../../helpers/Exporter'
import {WP as wp} from '../../helpers/Exporter'

export const styles = StyleSheet.create({
    container: {
        backgroundColor: Helpers.Theme.listingBG
    },


    bottonbtnsVw:{
        backgroundColor: Helpers.Theme.introBg,
        borderTopWidth: wp(0.3),
        borderTopColor: Helpers.Theme.blueBtnBorder,
        height: wp(20), width: '100%', marginTop: wp(5), alignItems: 'center',
        flexDirection: 'row', justifyContent: 'space-evenly'

    },
    bottonbtnsVw_mailBtn:{
        height: wp(10), width: wp(28), borderColor: Helpers.Theme.blueBtnBg,
        borderRadius: wp(1), borderWidth: wp(0.3),
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'

    },
    bottonbtnsVw_mailBtn_Img:{ height: wp(4), width: wp(4), resizeMode: 'contain', tintColor: Helpers.Theme.blueBtnBg },
    bottonbtnsVw_mailBtn_txt:{ color: Helpers.Theme.blueBtnBg, marginLeft: wp(1) },

    bottonbtnsVw_chatBtn:{
        height: wp(10), width: wp(45), borderColor: Helpers.Theme.whatsapp,
        borderRadius: wp(1), borderWidth: wp(0.3),
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'

    },
    bottonbtnsVw_chatBtn_Img:{ height: wp(4), width: wp(4), resizeMode: 'contain' },
    bottonbtnsVw_chatBtn_txt:{ color: Helpers.Theme.whatsapp, marginLeft: wp(1) },

    bottonbtnsVw_callBtn:{
        height: wp(10), width: wp(45), borderColor: Helpers.Theme.blueBtnBg,
        backgroundColor: Helpers.Theme.blueBtnBg,
        borderRadius: wp(1), borderWidth: wp(0.3),
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center'

    },
    bottonbtnsVw_callBtn_Img:{ height: wp(4), width: wp(4), resizeMode: 'contain', tintColor: Helpers.Theme.light },
    bottonbtnsVw_callBtn_txt:{ color: Helpers.Theme.light, marginLeft: wp(1) },

    contactpVw:{
        backgroundColor: Helpers.Theme.introBg,
        borderColor: Helpers.Theme.blueBtnBorder,
        borderRadius: wp(1), borderWidth: 1, height: wp(40), width: wp(95),
        marginTop: wp(5), paddingVertical: wp(3), paddingHorizontal: wp(1)
    },
    contactpVw_ImgVw:{ height: wp(18), width: wp(18), borderRadius: wp(9) },
    contactpVw_Img:{ height: '100%', width: '100%', resizeMode: 'cover' },
    contactpVw_txtVw:{ flexDirection: 'row', marginTop: wp(1) },
    contactpVw_pinpontImg:{ height: wp(3), width: wp(3), tintColor: Helpers.Theme.blueBtnBg, resizeMode: 'contain' },
    contactpVw_addressTxt:{ marginLeft: wp(1), width: wp(58) },
    contactpVw_addressTxt_underline:{ textDecorationLine: 'underline', color: Helpers.Theme.blueBtnBg },
    contactpVw_callImg:{ height: wp(3), width: wp(3), tintColor: Helpers.Theme.blueBtnBg, resizeMode: 'contain' },
    marginLeft1:{ marginLeft: wp(1) },
    contactpVw_BTN:{ height: wp(10), width: wp(90), borderRadius: wp(1),
         justifyContent: 'center', alignItems: 'center', position: 'absolute', bottom: wp(3), 
    alignSelf: 'center', backgroundColor: Helpers.Theme.blueBtnBg },



    detailslistVw: { marginTop: wp(5), width: wp(100) },
    marginTop3: { marginTop: wp(3) },
    detailslistVw_listVw: {
        flexDirection: 'row',
        paddingLeft: wp(5), paddingVertical: wp(4), alignItems: 'center',
        borderBottomWidth: wp(0.3),
        borderBottomColor: Helpers.Theme.blueBtnBorder
    },
    detailslistVw_listVw_Img:{ height: wp(10), width: wp(10), resizeMode: 'contain' },
    detailslistVw_listVw_nameTxt: { marginLeft: wp(3), textAlign: 'center' },
    detailslistVw_listVw_nextBtnVw:{
        height: wp(8), width: wp(8)
        , borderRadius: wp(4)
        , backgroundColor: Helpers.Theme.light,
        alignItems: 'center', justifyContent: 'center',
        position: 'absolute', right: wp(5)
    },
    detailslistVw_listVw_nextBtnVw_Img:{ height: wp(4), width: wp(4), resizeMode: 'contain' },

    listingaddressVw: { marginTop: wp(5), width: wp(100) },
    listingaddressVw_heading: { marginLeft: wp(4), fontWeight: '600' },
    listingaddressVw_rowVw: {  marginTop: wp(5), },
    listingaddressVw_rowVw_firstVw: { flexDirection: 'row', alignItems: "center", paddingLeft: wp(5), },
    listingaddressVw_rowVw_firstVw_ImgVw: {
        height: wp(10), width: wp(10),
        justifyContent: 'center', alignItems: 'center', borderRadius: wp(6)
        , backgroundColor: Helpers.Theme.roundIconBG
    },
    listingaddressVw_rowVw_firstVw_Img: {
        height: wp(4), width: wp(4),
        resizeMode: 'contain',
        tintColor: Helpers.Theme.roundIcon
    },

    marginleft2: { marginLeft: wp(2) },

    line: { height: wp(0.3), marginTop: wp(3), width: '100%', backgroundColor: Helpers.Theme.blueBtnBorder },



    overviewVw: { marginTop: wp(5), width: wp(93) },
    overviewVw_listouterVw: {
        marginTop: wp(3), padding: wp(3),
        backgroundColor: Helpers.Theme.introBg,
        borderRadius: wp(1), borderWidth: 1,
        borderColor: Helpers.Theme.blueBtnBorder
    },
    overviewVw_listVw: { width: wp(45), alignItems: 'flex-start' },
    overviewVw_listVw_titleTxt: { textAlign: 'center', marginTop: wp(2) },
    overviewVw_listVw_valueTxt: { color: Helpers.Theme.darkgrey, textAlign: 'center', marginTop: wp(2) },




    featuresVw: { marginTop: wp(5), width: wp(93) },
    featureslistouterVw: { marginTop: wp(3) },
    headingbold: {
        fontWeight: Helpers.Typography.weight600
    },
    featuresVw_list_moreVw: {
        height: wp(22),
        marginBottom: wp(1),
        borderRadius: wp(1), borderWidth: 1,
        borderColor: Helpers.Theme.blueBtnBorder,
        marginRight: wp(1), width: wp(30),
        backgroundColor: Helpers.Theme.blueBtnBg,
        justifyContent: 'center', alignItems: 'center'
    },
    featuresVw_list_Vw: {
        height: wp(22), marginBottom: wp(1),
        borderRadius: wp(1), borderWidth: 1,
        borderColor: Helpers.Theme.blueBtnBorder,
        marginRight: wp(1), width: wp(30),
        backgroundColor: Helpers.Theme.introBg,
        justifyContent: 'center', alignItems: 'center'
    },

    featuresVw_list_Vw_Img: { height: wp(6), width: wp(6), resizeMode: 'contain' },
    featuresVw_list_Vw_txt: { textAlign: 'center', marginTop: wp(2) },


    descriptionVw: { marginTop: wp(5), height: wp(15), width: wp(93) },
    descriptionWebVw: {
        marginTop: wp(1),
        maxHeight: wp(50),
        width: wp(94),
        backgroundColor: 'transparent',
        flex: 1
    },
    lastupdatedVw: {
        height: wp(12), width: '100%',
        marginTop: wp(5), justifyContent: 'center', alignItems: 'center', flexDirection: "row",
        borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder, backgroundColor: Helpers.Theme.introBg
    },
    lastupdatedVw_Img: { height: wp(4), width: wp(4), resizeMode: 'contain', marginRight: wp(2) },


    sliderVw: {
        height: wp(70), width: "100%"
    },
    sliderItem_Vw: {
        height: wp(70),

    },
    sliderRightbtns_Vw: {
        flexDirection: 'row',
        position: 'absolute', top: wp(5), right: wp(3)
    },
    abs: {
        position: 'absolute'
    },
    details_Vw: { width: '100%', marginTop: wp(5), paddingHorizontal: wp(4) },
    address_Txt: {
        marginLeft: wp(1), color: Helpers.Theme.darkgrey
    },
    tagsrow_Vw: {
        flexDirection: 'row', marginTop: wp(3)
    },
    tag1_btn: {
        paddingVertical: wp(1), paddingHorizontal: wp(3),
        backgroundColor: 'red', borderRadius: wp(1)
    },
    tag2_btn: {
        paddingVertical: wp(1), marginLeft: wp(2),
        paddingHorizontal: wp(3), backgroundColor: 'green', borderRadius: wp(1)
    },

    tag3_btn: {
        paddingVertical: wp(1), marginLeft: wp(2),
        paddingHorizontal: wp(3), backgroundColor: 'brown', borderRadius: wp(1)
    },
    address_Vw: {
        marginTop: wp(2), flexDirection: 'row', alignItems: 'center'
    },
    address_Img: {
        resizeMode: 'contain', height: wp(3), width: wp(3), tintColor: Helpers.Theme.gryBack
    },

    tagCam_btn: {
        paddingVertical: wp(1), marginLeft: wp(2),
        paddingHorizontal: wp(3),
        alignItems: 'center', backgroundColor: 'black',
        flexDirection: 'row', borderRadius: wp(1)
    },
    tagCam_btn_Img: {
        height: wp(3), width: wp(3), tintColor: Helpers.Theme.light, marginRight: wp(1), resizeMode: 'contain'
    },

    tagsTxt: {
        color: Helpers.Theme.light, fontWeight: '600'
    },

    sliderBackBtn: {
        position: 'absolute', left: 0, top: wp(5)
    },

    header_backBtn: {
        backgroundColor: Helpers.Theme.light, marginLeft: wp(2), justifyContent: 'center',
        borderRadius: wp(5), height: wp(10), width: wp(10), alignSelf: "center",
        alignItems: 'center'
    },
    header_backImg: {
        height: wp(5), width: wp(5), resizeMode: 'contain'
    },
    heartfilled: {
        height: wp(5), width: wp(5), resizeMode: 'contain', tintColor: Helpers.Theme.gryBack
    },

    stickyheader: {
        height: wp(18), width: wp(94), marginTop: wp(-8)
        , borderRadius: wp(2)
        , backgroundColor: Helpers.Theme.light,
        flexDirection: 'row',
        alignItems: 'center'
    },
    stickyheader_Vw: {
        alignItems: 'center', width: wp(31)
    },
    stickyheader_Img: {
        height: wp(8), width: wp(8),
        resizeMode: 'contain'
    },
    stickyheader_txt1: {
        color: Helpers.Theme.gryBack, marginTop: wp(1)
    },
    stickyheader_txt2: {
        color: Helpers.Theme.black
    }
})
