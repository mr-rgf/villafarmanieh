import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet, TextInput, TouchableOpacity
} from 'react-native'

import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import { WP as wp } from '../../../helpers/Exporter'

import RNPickerSelect from 'react-native-picker-select';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Collapsible from 'react-native-collapsible';
import Tooltip, { TooltipChildrenContext } from 'react-native-walkthrough-tooltip';

import { styles } from './MortgageCalculator.style'
import Toast from 'react-native-root-toast';

class MortgageCalculator extends Component {
    constructor(props) {
        super(props)
        this.state = {
            mortgage: [
                { label: 'Years', value: 'Years' },
                { label: 'Months', value: 'Months' },
            ],

            isCollapsed: true,

            tooltipIndex: 0,
            tooltipVisile: false,

            currentInterest: 'Years',
            currentLoan: 'Years',

            initialvalue: this.props.route.params.value.initial_value.value,
            downpayment: '',
            interestrate: '',
            mortgagetermduration: '',

            showerror: false,


            remainingdept: '',
            monthlyrate: '',
            paymentmethods: '',
            monthlypayment: '',
            totalpayment: '',
            totalinterestpayment: ''

        }


    }

    updatePrice = () => {
        let { price } = this.props.route.params
        let dummy = price.split(" ")
        let newdummy=dummy[0]

        if(dummy[0].includes(','))
        {
            newdummy=dummy[0].split(',')
        }
        return newdummy[0]+""+newdummy[1]
    }

    toggleReport = () => {
        this.setState({ isCollapsed: !this.state.isCollapsed })
    }

    handleInputChange = (key, value) => {
        this.setState({ [key]: value })
    }
    validate = () => {
        if (this.state.initialvalue == "") {
            return false
        }

        if (this.state.downpayment == "") {
            return false
        }

        if (this.state.interestrate == "") {
            return false
        }

        if (this.state.mortgagetermduration == "") {
            return false
        }

        return true
    }


    calculateMortgage = () => {
        var isValid = this.validate()
        var { initialvalue, downpayment, interestrate, mortgagetermduration, currentInterest, currentLoan } = this.state

        // console.log('this.state',this.state)
        // console.log('parseFloat(initialvalue)',parseFloat(initialvalue))
        // console.log('parseFloat(downpayment)',parseFloat(downpayment))
        // console.log('currentLoan',this.state.currentLoan)

        var l = ""
        var r = ""
        var n = ""
        var p = ""

        var monthlyrate = ""
        var paymentperiods = ""
        var monthlypayment = ""
        var totalpayment = ""
        var totalinterest = ""

        if (isValid) {
            l = parseFloat(initialvalue) - parseFloat(downpayment)

            if (currentInterest == 'Years') {
                r = (parseFloat(interestrate) / 100) / 12
            } else {
                r = (parseFloat(interestrate) / 100)

            }
            monthlyrate = (r * 100 * 100) / 100

            if (currentLoan == "Years") {
                n = parseFloat(mortgagetermduration) * 12
            } else {
                n = parseFloat(mortgagetermduration)
            }

            paymentperiods = n

            p = (l * r) / (1 - Math.pow(1 + r, -n))
            monthlypayment = ((p * 100) / 100)

            totalpayment = ((p * n * 100) / 100)

            totalinterest = ((p * n - l) * 100) / 100


            this.setState({
                isCollapsed: false,
                remainingdept: l,
                monthlyrate: monthlyrate.toFixed(2),
                paymentmethods: paymentperiods.toFixed(2),
                monthlypayment: monthlypayment.toFixed(2),
                totalpayment: totalpayment.toFixed(2),
                totalinterestpayment: totalinterest.toFixed(2)
            })

            // console.log('first', l)
            // console.log('2nd', monthlyrate)
            // console.log('3rd', paymentperiods)
            // console.log('4th', monthlypayment)
            // console.log('5th', totalpayment)
            // console.log('6th', totalinterest)



        } else {
            Helpers.showToast(this.props.settings.required_fields)
            this.setState({ showerror: true })
        }
    }


    render() {
        let { value, title } = this.props.route.params

        return (
            <View style={[Helpers.GLOBAL_SHEET.maxHWC, styles.container]}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}

                />


                <KeyboardAwareScrollView style={styles.scrollVw}>

                    {/* Main image start */}
                    <Image
                        source={Helpers.Images.mortgagecal}
                        style={styles.mainmortgageImg}
                    />
                    {/* Main image end */}


                    {/* first long intput start */}
                    <View >
                        <View style={styles.inputitemrowVw}>
                            <Text>{value.initial_value.heading}</Text>

                            <Tooltip
                                isVisible={this.state.tooltipIndex == 1 && this.state.tooltipVisile}
                                content={<Text>{value.initial_value.hint}</Text>}
                                placement="top"
                                onClose={() => this.setState({ tooltipVisile: false })}
                            >
                                <TouchableOpacity
                                    onPress={() => this.setState({ tooltipVisile: true, tooltipIndex: 1 })}
                                >
                                    <Image
                                        source={Helpers.Images.question}
                                        style={styles.questionImg}
                                    />
                                </TouchableOpacity>
                            </Tooltip>



                        </View>
                        <View style={[styles.inputitemrowVw_innerVw, this.state.showerror && this.state.initialvalue == "" ? { borderColor: 'red' } : {}]}>
                            <View style={styles.dollarVw}>
                                <Text>{value.currency}</Text>
                            </View>

                            <TextInput
                                style={[styles.longInput]}
                                keyboardType="number-pad"

                                value={this.state.initialvalue}
                                onChangeText={(value) => this.handleInputChange('initialvalue', value)}
                            />
                        </View>
                    </View>
                    {/* first long intput end */}


                    {/* second long intput end */}

                    <View style={styles.marginTop5} >
                        <View style={styles.inputitemrowVw}>
                            <Text>{value.down_payment.heading}</Text>

                            <Tooltip
                                isVisible={this.state.tooltipIndex == 2 && this.state.tooltipVisile}
                                content={<Text>{value.down_payment.hint}</Text>}
                                placement="top"
                                onClose={() => this.setState({ tooltipVisile: false })}
                            >
                                <TouchableOpacity
                                    onPress={() => this.setState({ tooltipVisile: true, tooltipIndex: 2 })}
                                >
                                    <Image
                                        source={Helpers.Images.question}
                                        style={styles.questionImg}
                                    />
                                </TouchableOpacity>
                            </Tooltip>




                        </View>
                        <View style={[styles.inputitemrowVw_innerVw, this.state.showerror && this.state.downpayment == "" ? { borderColor: 'red' } : {}]}>
                            <View style={styles.dollarVw}>
                                <Text>{value.currency}</Text>
                            </View>

                            <TextInput
                                style={[styles.longInput]}
                                keyboardType="number-pad"
                                value={this.state.downpayment}
                                onChangeText={(value) => this.handleInputChange('downpayment', value)}
                            />
                        </View>
                    </View>
                    {/* first long intput end */}


                    {/* first picker-intput row start */}

                    <View style={styles.marginTop5} >
                        <View style={styles.inputitemrowVw}>
                            <Text>{value.interest_rate.heading}</Text>
                            <Tooltip
                                isVisible={this.state.tooltipIndex == 3 && this.state.tooltipVisile}
                                content={<Text>{value.interest_rate.hint}</Text>}
                                placement="top"
                                onClose={() => this.setState({ tooltipVisile: false })}
                            >
                                <TouchableOpacity
                                    onPress={() => this.setState({ tooltipVisile: true, tooltipIndex: 3 })}
                                >
                                    <Image
                                        source={Helpers.Images.question}
                                        style={styles.questionImg}
                                    />
                                </TouchableOpacity>
                            </Tooltip>

                        </View>

                        <View style={styles.pickerinputmainVw}>
                            <View style={styles.pickerandinputparentVw}>
                                <View
                                    style={styles.pickerparentVw}>

                                    <RNPickerSelect
                                        onValueChange={value => {
                                            this.setState({
                                                currentInterest: value,
                                            });
                                        }}
                                        items={this.state.mortgage}
                                        placeholder={{}}
                                        useNativeAndroidPickerStyle={false}
                                        style={
                                            {
                                                inputIOSContainer: styles.pickerVw,
                                                inputAndroidContainer: styles.pickerVw,


                                            }}
                                        // Icon={<></>}
                                        value={this.state.currentInterest}


                                    />
                                </View>
                            </View>

                            <Text> - </Text>

                            <View style={[styles.halfinputVw, this.state.showerror && this.state.interestrate == "" ? { borderColor: 'red' } : {}]}>

                                <TextInput
                                    style={[styles.halfInput]}
                                    keyboardType="number-pad"
                                    placeholder={"Enter Percentage"}

                                    value={this.state.interestrate}
                                    onChangeText={(value) => this.handleInputChange('interestrate', value)}

                                />
                            </View>
                        </View>

                    </View>
                    {/* first picker-intput row end */}




                    {/* second picker-intput row start */}

                    <View style={styles.marginTop5} >
                        <View style={styles.inputitemrowVw}>
                            <Text>{value.mortgage_term.heading}</Text>
                            <Tooltip
                                isVisible={this.state.tooltipIndex == 4 && this.state.tooltipVisile}
                                content={<Text>{value.mortgage_term.hint}</Text>}
                                placement="top"
                                onClose={() => this.setState({ tooltipVisile: false })}
                            >
                                <TouchableOpacity
                                    onPress={() => this.setState({ tooltipVisile: true, tooltipIndex: 4 })}
                                >
                                    <Image
                                        source={Helpers.Images.question}
                                        style={styles.questionImg}
                                    />
                                </TouchableOpacity>
                            </Tooltip>
                        </View>

                        <View style={styles.pickerinputmainVw}>
                            <View style={styles.pickerandinputparentVw}>

                                <View
                                    style={styles.pickerparentVw}>

                                    <RNPickerSelect
                                        onValueChange={value => {
                                            this.setState({
                                                currentLoan: value,
                                            });
                                        }}
                                        items={this.state.mortgage}
                                        placeholder={{}}
                                        useNativeAndroidPickerStyle={false}
                                        style={
                                            {
                                                inputIOSContainer: styles.pickerVw,
                                                inputAndroidContainer: styles.pickerVw,


                                            }}
                                        // Icon={}

                                        value={this.state.currentLoan}


                                    />

                                </View>
                            </View>

                            <Text> - </Text>

                            <View style={[styles.halfinputVw, this.state.showerror && this.state.mortgagetermduration == "" ? { borderColor: 'red' } : {}]}>

                                <TextInput
                                    placeholder={"Enter Duration"}
                                    style={[styles.halfInput]}
                                    keyboardType="number-pad"

                                    value={this.state.mortgagetermduration}
                                    onChangeText={(value) => this.handleInputChange('mortgagetermduration', value)}
                                />
                            </View>
                        </View>

                    </View>
                    {/* first picker-intput row end */}



                    <TouchableOpacity
                        onPress={() => this.calculateMortgage()}
                        style={styles.searchBtn}>
                        <Text style={{ color: Helpers.Theme.light }}>{value.button}</Text>
                    </TouchableOpacity>

                    <Collapsible collapsed={this.state.isCollapsed}>
                        <View style={styles.collapseitemVw}>
                            <View style={Helpers.GLOBAL_SHEET.flexrow}>
                                <Text style={styles.reportItemtxt}>{value.remaning_dept}: </Text>
                                <Text>$ {this.state.remainingdept}</Text>
                            </View>

                            <View style={styles.reporttxtVw}>
                                <Text style={styles.reportItemtxt}>{value.monthly_rate}: </Text>
                                        <Text>$ {this.state.monthlyrate}</Text>
                            </View>

                            <View style={styles.reporttxtVw}>
                                <Text style={styles.reportItemtxt}>{value.payment_periods}: </Text>
                                <Text>{this.state.paymentmethods} {value.remaning_txt}</Text>
                            </View>

                            <View style={styles.reporttxtVw}>
                                <Text style={styles.reportItemtxt}>{value.total_monthly_payments}: </Text>
                                        <Text>$ {this.state.monthlypayment}</Text>
                            </View>


                            <View style={styles.reporttxtVw}>
                                <Text style={styles.reportItemtxt}>{value.total_payments}: </Text>
                                <Text>$ {this.state.totalpayment}</Text>
                            </View>


                            <View style={styles.reporttxtVw}>
                                <Text style={styles.reportItemtxt}>{value.total_intrest_payment}: </Text>
                                        <Text>$ {this.state.totalinterestpayment}</Text>
                            </View>


                        </View>
                    </Collapsible>

                </KeyboardAwareScrollView>


            </View>
        );
    }
}





const mapStateToProps = (state) => {
    return {
        settings: state.intro.settings,

    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MortgageCalculator);