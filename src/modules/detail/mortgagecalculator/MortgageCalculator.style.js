import {StyleSheet} from 'react-native'
import * as Helpers from '../../../helpers/Exporter'
import {WP as wp} from '../../../helpers/Exporter'

export const styles = StyleSheet.create({
    container: {
        backgroundColor: Helpers.Theme.listingBG
    },
    pickerandinputparentVw: { height: wp(12), width: wp(43), flexDirection: 'row', backgroundColor: Helpers.Theme.introBg, borderRadius: wp(1), borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder },
    halfInput: { height: wp(12), width: wp(43), paddingHorizontal: wp(2) },

    halfinputVw: { height: wp(12), width: wp(43), flexDirection: 'row', backgroundColor: Helpers.Theme.introBg, borderRadius: wp(1), borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder },

    pickerparentVw: { height: wp(12), width: wp(43), paddingLeft: wp(2), alignItems: 'center', justifyContent: 'center' },
    pickerVw: {
        height: wp(10), justifyContent: 'center',width:wp(43),
        alignItems:'center'

    },
    pickerinputmainVw: { flexDirection: 'row', marginTop: wp(2), alignItems: 'center' },

    longInput: { height: wp(12), width: wp(78), paddingHorizontal: wp(2) },
    dollarVw: { height: wp(12), width: wp(12), alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder },

    inputitemrowVw_innerVw: {
        height: wp(12), width: wp(90),
        flexDirection: 'row',
        backgroundColor: Helpers.Theme.introBg,
        borderRadius: wp(1),
        borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        marginTop: wp(2)
    },
    questionImg: { height: wp(4), width: wp(4), resizeMode: 'contain', marginLeft: wp(1) },

    scrollVw: { width: '100%', paddingHorizontal: wp(5) },
    inputitemrowVw: { flexDirection: 'row', alignSelf: 'flex-start' },

    mainmortgageImg: { height: wp(55), width: wp(60), resizeMode: 'contain', alignSelf: 'center' },


    marginTop5: {
        marginTop: wp(5)
    },

    reporttxtVw: { flexDirection: 'row', marginTop: wp(1.5) },
    reportItemtxt: { color: Helpers.Theme.darkgrey, width: wp(40) },

    collapseitemVw: {
        paddingVertical: wp(5),
        paddingHorizontal: wp(5),
        backgroundColor: Helpers.Theme.introBg, width: wp(90),
        borderRadius: wp(1), borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        marginBottom: wp(3)
    },

    searchBtn: {
        height: Helpers.WP(12),
        width: '100%',
        borderRadius: wp(1),
        marginTop: wp(5), marginBottom: wp(5),
        alignSelf: 'center',
        backgroundColor: Helpers.Theme.blueBtnBg,
        alignItems: 'center', justifyContent: 'center'
    },
})
