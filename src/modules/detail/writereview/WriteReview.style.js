import {StyleSheet} from 'react-native'
import * as Helpers from '../../../helpers/Exporter'
import {WP as wp} from '../../../helpers/Exporter'

export const styles = StyleSheet.create({
    container: {
        padding: wp(5),
        backgroundColor: Helpers.Theme.listingBG
    },
    title: {
        height: wp(11), width: wp(90), backgroundColor: Helpers.Theme.introBg,
        borderWidth: 1, borderRadius: wp(1), borderColor: Helpers.Theme.blueBtnBorder,
        marginTop: wp(2), paddingHorizontal: wp(2)
    },

    review: {
        height: wp(40), width: wp(90), backgroundColor: Helpers.Theme.introBg,
        borderWidth: 1, borderRadius: wp(1), borderColor: Helpers.Theme.blueBtnBorder,
        marginTop: wp(2), paddingHorizontal: wp(2)
    },
    mainBtn:{
        height:wp(11),width:wp(90),
        marginTop:wp(5),
        backgroundColor:Helpers.Theme.blueBtnBg,
        borderRadius:wp(1),
        justifyContent:'center',alignItems:'center'
    },
    btnTxt:{
        color:Helpers.Theme.light
    }
 
})
