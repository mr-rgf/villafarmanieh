import React, { Component } from 'react';
import {
    View, Text, TouchableOpacity, TextInput
} from 'react-native'
import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import { WP as wp } from '../../../helpers/Exporter'
import { styles } from './WriteReview.style'
import { AirbnbRating } from 'react-native-ratings';


class WriteReview extends Component {


    render() {
        let { value, title } = this.props.route.params

        return (
            <View style={Helpers.GLOBAL_SHEET.maxHW}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}

                />
                <View style={[Helpers.GLOBAL_SHEET.maxHW, styles.container]}>
                    <View style={{ alignSelf: 'flex-start' }}>
                        <Text>{value.review_rating.field_title}</Text>
                        <AirbnbRating
                            count={5}
                            defaultRating={1}
                            size={wp(6)}
                            showRating={false}
                            starContainerStyle={{ marginTop: wp(2) }}
                        />
                    </View>

                    <View style={{ alignSelf: 'flex-start', marginTop: wp(5) }}>
                        <Text>{value.review_title.field_title}</Text>
                        <TextInput
                            style={styles.title}
                        />
                    </View>

                    <View style={{ alignSelf: 'flex-start', marginTop: wp(5) }}>
                        <Text>{value.review_msg.field_title}</Text>
                        <TextInput
                            style={styles.review}
                        />
                    </View>


                    <TouchableOpacity
                        style={[styles.mainBtn,{backgroundColor:this.props.settings.primary_clr}]}
                    >
                        <Text style={[styles.btnTxt]}>{value.btn_text}</Text>
                    </TouchableOpacity>

                </View>




            </View>
        );
    }
}




const mapStateToProps = (state) => {
    return {
        settings: state.intro.settings,

    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WriteReview);