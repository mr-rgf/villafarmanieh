import React, { Component } from 'react';
import {
    View
} from 'react-native'
import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import { WebView } from 'react-native-webview';

class VirtualTour extends Component {
    constructor(props) {
        super(props)
        this.state = { visible: true };


    }
    hideSpinner() {
        this.setState({ visible: false });
    }
    render() {
        let { value: iframe,title } = this.props.route.params

        return (
            <View style={Helpers.GLOBAL_SHEET.maxHWCC}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}
                />

                <WebView
                    scalesPageToFit={true}
                    bounces={false}
                    javaScriptEnabled
                    style={[Helpers.GLOBAL_SHEET.wp100]}
                    source={{
                        html: `${iframe}`,
                    }}
                    startInLoadingState={true}
                    renderLoading={() => {
                        return (
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                                <Helpers.Lumper lumper={true} color={this.props.settings.primary_clr} />
                            </View>

                        )
                    }}
                    mediaPlaybackRequiresUserAction={false}


                />


            </View>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        settings: state.intro.settings,

    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(VirtualTour);