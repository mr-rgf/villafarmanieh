import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet,FlatList,TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import { WP as wp } from '../../../helpers/Exporter'
import {styles} from './Attachments.style'

class Attachments extends Component {

    constructor(props) {
        super(props)
        this.state = {
            attachments: [
                {
                    id: 0,
                    name: 'Home Attachment Report',
                    img: Helpers.Images.filetypedoc,
                    filesize: '51 KB'
                },
                {
                    id: 1,
                    name: 'Map Location',
                    img: Helpers.Images.filetypejpg,
                    filesize: '51 KB'
                },
                {
                    id: 2,
                    name: 'Home Documents',
                    img: Helpers.Images.filetypepdf,
                    filesize: '51 KB'
                },
                {
                    id: 3,
                    name: 'Home Attachment Report',
                    img: Helpers.Images.filetypedoc,
                    filesize: '51 KB'
                }
            ]
        }
    }



    render() {
        let { value,title } = this.props.route.params

        return (
            <View style={[Helpers.GLOBAL_SHEET.maxHWC,styles.container]}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}

                />

                <View style={styles.detailslistVw}>
                    <View style={styles.marginTop3}>
                        <FlatList
                            data={value}
                            keyExtractor={(item) => item.id}
                            renderItem={({ item, index }) => {

                                return (
                                    <View style={styles.detailslistVw_listVw}>
                                        <Image
                                            source={{uri:item.attachment_img}}
                                            style={styles.detailslistVw_listVw_Img}
                                        />
                                        <View>
                                            <View style={{marginLeft:wp(2)}}>
                                            <Text style={[Helpers.Typography.fourPointFive]}>{item.attachment_title}</Text>
                                            <Text style={[Helpers.Typography.three,styles.filesizeTxt]}>File size: {item.attachment_size}</Text>

                                            </View>

                                        </View>

                                        <TouchableOpacity
                                            // onPress={() => this.opendetails(item)}
                                            style={styles.detailslistVw_listVw_nextBtnVw}>
                                            <Image
                                                source={Helpers.Images.cloud}
                                                style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                            />
                                        </TouchableOpacity>

                                    </View>
                                )


                            }}
                        />
                    </View>

                </View>

            </View>
        );
    }
}


const mapStateToProps = (state) => {
    return {}

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Attachments);