import {StyleSheet} from 'react-native'
import * as Helpers from '../../../helpers/Exporter'
import {WP as wp} from '../../../helpers/Exporter'

export const styles = StyleSheet.create({
    container: {
        // padding: wp(5),
        backgroundColor: Helpers.Theme.listingBG
    },
    filesizeTxt:{
        color:Helpers.Theme.darkgrey
    },
    detailslistVw: { width: wp(100) },
    marginTop3: { marginTop: wp(3) },
    detailslistVw_listVw: {
        flexDirection: 'row',
        paddingLeft: wp(5), paddingVertical: wp(4), alignItems: 'center',
        borderBottomWidth: wp(0.3),
        borderBottomColor: Helpers.Theme.blueBtnBorder
    },
    detailslistVw_listVw_Img:{ height: wp(10), width: wp(10), resizeMode: 'contain' },
    detailslistVw_listVw_nameTxt: { marginLeft: wp(3), textAlign: 'center' },
    detailslistVw_listVw_nextBtnVw:{
        height: wp(6), width: wp(6),
        alignItems: 'center', justifyContent: 'center',
        position: 'absolute', right: wp(5)
    },
    detailslistVw_listVw_nextBtnVw_Img:{ height:'100%', width: '100%', resizeMode: 'contain' },

  
})
