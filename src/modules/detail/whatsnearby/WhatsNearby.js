import React, { Component } from 'react';
import {
    View, Text, Image, StyleSheet, FlatList, TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import { WP as wp } from '../../../helpers/Exporter'
import { AirbnbRating } from 'react-native-ratings';
import {styles} from './WhatsNearby.style'

class WhatsNearby extends Component {
    constructor() {
        super()
        this.state = {
            nearby: [
                {
                    id: 0,
                    title: 'Automotive',
                    sublist: [
                        {
                            id: 0,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'
                        },
                        {
                            id: 1,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'

                        },
                        {
                            id: 2,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 4.5,
                            distance: '6.26 km'

                        },
                        {
                            id: 3,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'

                        }
                    ]
                },
                {
                    id: 1,
                    title: 'Hotels & Travel',

                },
                {
                    id: 2,
                    title: 'Restuarants',
                    sublist: [
                        {
                            id: 0,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'
                        },
                        {
                            id: 1,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'

                        },
                        {
                            id: 2,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 4.5,
                            distance: '6.26 km'

                        },
                        {
                            id: 3,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'

                        }
                    ]
                },

                {
                    id: 3,
                    title: 'Shopping',
                    sublist: [
                        {
                            id: 0,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'
                        },
                        {
                            id: 1,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'

                        },
                        {
                            id: 2,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 4.5,
                            distance: '6.26 km'

                        },
                        {
                            id: 3,
                            title: 'Mastercraft Auto Body Shop',
                            rating: 5,
                            distance: '6.26 km'

                        }
                    ]
                },
            ]
        }
    }





    render() {
        let { value,title } = this.props.route.params

        return (
            <View style={[Helpers.GLOBAL_SHEET.maxHWC, styles.container]}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}
                    rightsection={
                        <View style={styles.rightsectionVw}>
                            <Text style={[Helpers.Typography.twoPointFive, styles.rightsectionTxt]}>Powered by</Text>
                            <Image
                                source={Helpers.Images.yelp}
                                style={styles.rightsectionImg}
                            />
                        </View>
                    }
                />

                <View style={styles.listparentVw}>
                    <FlatList
                        data={value}
                        keyExtractor={(item) => item.section_title}
                        renderItem={({ item, index }) => {

                            return (
                                <View style={[styles.listVw]}>

                                    <Text style={[Helpers.Typography.fourPointFive, { fontWeight: Helpers.Typography.weightBold }]}>{item.section_title}</Text>
                                    {
                                        item.nearby_list != undefined ?
                                            <FlatList
                                                data={item.nearby_list}
                                                keyExtractor={(item) => item.title}
                                                renderItem={({ item: sublistitem, index }) => {

                                                    return (
                                                        <View style={[styles.sublistVw,item.nearby_list.length-1==index?{}:styles.sublistVwNormal]}>

                                                            <Text style={[Helpers.Typography.four, styles.sublisttitleTxt]}>{sublistitem.title}</Text>
                                                            <Text style={[Helpers.Typography.three,styles.sublisttxt2]}>({sublistitem.distance})</Text>


                                                            <View style={styles.sublistratingVw}>

                                                                <AirbnbRating
                                                                    count={5}
                                                                    defaultRating={parseFloat(sublistitem.stars)}
                                                                    size={wp(4)}
                                                                    showRating={false}
                                                                    starContainerStyle={styles.margintop2}
                                                                />
                                                            </View>
                                                        </View>

                                                    )
                                                }}
                                            /> : null
                                    }
                                </View>

                            )
                        }}
                    />
                </View>









            </View>
        );
    }
}






const mapStateToProps = (state) => {
    return {}

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WhatsNearby);