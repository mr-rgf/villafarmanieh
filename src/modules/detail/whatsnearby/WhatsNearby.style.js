import {StyleSheet} from 'react-native'
import * as Helpers from '../../../helpers/Exporter'
import {WP as wp} from '../../../helpers/Exporter'

export const styles = StyleSheet.create({

    container: {
        backgroundColor: Helpers.Theme.listingBG
    },

    listparentVw:{  marginBottom: wp(10) },

    sublistVw:{
        width: wp(90),
      
    },
    margintop2:{ marginTop: wp(2) },
    sublistratingVw:{ position: 'absolute', right: wp(1), top: wp(1), },

    sublisttxt2: { color: Helpers.Theme.darkgrey, width: wp(40) },

    sublisttitleTxt:{ fontWeight: Helpers.Typography.weight600, width: wp(60) },

    sublistVwNormal:{
        paddingVertical: wp(3),
        borderBottomWidth: 1, borderBottomColor: Helpers.Theme.blueBtnBorder,
    },

    listVw:{
        width: wp(100), paddingHorizontal: wp(5),
        borderBottomWidth: 1, borderBottomColor: Helpers.Theme.blueBtnBorder,
        paddingVertical: wp(5)
    },

    rightsectionVw: {
        flexDirection: 'row', alignItems: 'center'
    },
    rightsectionTxt: {
        marginTop: wp(2), marginRight: wp(1)
    },
    rightsectionImg: {
        height: wp(8), width: wp(15), resizeMode: 'contain'
    }



  
})
