import {StyleSheet} from 'react-native'
import * as Helpers from '../../../helpers/Exporter'
import {WP as wp} from '../../../helpers/Exporter'

export const styles = StyleSheet.create({
    container: {
        padding: wp(5),
        backgroundColor: Helpers.Theme.listingBG
    },

    firstrowVw: { flexDirection: 'row', justifyContent: 'space-between' },
    firstrowInput: {
        height: wp(11), width: wp(43), backgroundColor: Helpers.Theme.introBg,
        borderWidth: 1, borderRadius: wp(1), borderColor: Helpers.Theme.blueBtnBorder,
        marginTop: wp(2), paddingHorizontal: wp(2),alignItems:'center',justifyContent:'center'
    },


    secondrowInput: {
        height: wp(11), width: wp(90), backgroundColor: Helpers.Theme.introBg,
        borderWidth: 1, borderRadius: wp(1), borderColor: Helpers.Theme.blueBtnBorder,
        marginTop: wp(2), paddingHorizontal: wp(2)
    },

    flagVw: { marginTop: wp(3), flexDirection: "row" },
    flagBtnContainer: {
        height: wp(11),
        minWidth: wp(22),
        maxWidth: wp(22),
        backgroundColor: Helpers.Theme.introBg,
        borderWidth: 1,
        borderRadius: wp(1), borderColor: Helpers.Theme.blueBtnBorder
        , justifyContent: "center", alignItems: 'center'
    },

    phoneInput: {
        height: wp(11), width: wp(68.5), backgroundColor: Helpers.Theme.introBg,
        borderColor: Helpers.Theme.blueBtnBorder,
        paddingHorizontal: wp(2),


        borderRightWidth: 1,
        borderTopWidth: 1, borderBottomWidth: 1,
    },

    message:{
        height: wp(40), width: wp(90), backgroundColor: Helpers.Theme.introBg,
        borderWidth: 1, borderRadius: wp(1), borderColor: Helpers.Theme.blueBtnBorder,
        marginTop: wp(3), paddingHorizontal: wp(2),
        paddingVertical:wp(2)
    },

    secondrowVw: { marginTop: wp(5), flexDirection: 'row' },


    mainBtn:{
        height:wp(11),width:wp(90),
        marginTop:wp(5),
        backgroundColor:Helpers.Theme.blueBtnBg,
        borderRadius:wp(1),
        justifyContent:'center',alignItems:'center'
    },
    btnTxt:{
        color:Helpers.Theme.light
    }
})
