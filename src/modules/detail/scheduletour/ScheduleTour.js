import React, { Component } from 'react';
import {
    View, Text, TouchableOpacity, TextInput, Platform
} from 'react-native'
import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import { styles } from './ScheduleTour.style'
import Api from '../../../network/Api'

import DateTimePicker from '@react-native-community/datetimepicker';


class ScheduleTour extends Component {

    constructor() {
        super()
        this.state = {
            // showdatepicker: false,
            showtimepicker: false,
            date: new Date(Date.now()),
            mode: 'time',

            datepicked: '',
            timepicked: '',

            name: '',
            email: '',
            phone: '',
            message: '',
            showerror: false,
            loading: false
        }
    }

    handleInputChange = (field, value) => {
        this.setState({ [field]: value })
    }

    onChange = (event, selectedDate) => {
        const currentDate = selectedDate || this.state.date;

        if (this.state.mode == 'date') {
            var newdate = this.convertDate(selectedDate + "")

            this.setState({ datepicked: newdate, showdatepicker: Platform.OS === 'ios' })

        } else {
            var newtime = this.convertTime(selectedDate + "")

            this.setState({ timepicked: newtime, showdatepicker: Platform.OS === 'ios' })

        }
    };

    convertDate = (datex) => {
        var res = datex.split(" ");
        var newmonth = Helpers.convertMonthToDate(res[1])
        var newformat = res[2] + "/" + newmonth + "/" + res[3]
        return newformat
    }

    convertTime = (datex) => {
        var res = datex.split(" ");
        return res[4]

    }

    showTimepicker = (mode) => {
        this.setState({ mode: mode, showdatepicker: true });
    };

    scheduleTour = async () => {
        var isvalid = this.validate()
        var response

        var { property_id } = this.props.route.params
        if (isvalid) {
            let param = {
                "property_id": property_id,
                "tour-date": this.state.datepicked,
                "schedule_time": this.state.timepicked,
                "your-name": this.state.name,
                "tour-contact": this.state.phone,
                "tour-email": this.state.email,
                "tour-msg": this.state.message
            }
            this.setState({ loading: true })

            response = await Api.post("schedule-tour", param)
            // console.log('res',response.data.message)
            Helpers.showToast(response.data.message+"")
           
            this.setState({ loading: false,datepicked:'',timepicked:'',name:'',email:'',phone:'',message:'' })


        } else {
            this.setState({ showerror: true })
            
            Helpers.showToast(this.props.settings.required_fields)
        }
    }

    validate = () => {
        if (this.state.datepicked == '') {
            return false
        }
        if (this.state.timepicked == '') {
            return false
        }
        if (this.state.name == '') {
            return false
        }
        if (this.state.email == '') {
            return false
        }
        if (this.state.phone == '') {
            return false
        }
        if (this.state.date == '') {
            return false
        }
        if (this.state.message == '') {
            return false
        }
        return true
    }



    render() {
        let { value, title } = this.props.route.params
        return (
            <View style={Helpers.GLOBAL_SHEET.maxHWC}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}

                />

                <View style={[Helpers.GLOBAL_SHEET.maxHW, styles.container]}>

                    <View style={styles.firstrowVw}>
                        <View>
                            <Text>{value.date.field_title}</Text>
                            <TouchableOpacity
                                onPress={() => this.showTimepicker('date')}
                                style={[styles.firstrowInput, this.state.showerror && this.state.datepicked == "" ? { borderColor: 'red' } : {}]}
                            ><Text>{this.state.datepicked}</Text></TouchableOpacity>
                        </View>

                        <View>
                            <Text>{value.timings.field_title}</Text>
                            <TouchableOpacity
                                onPress={() => this.showTimepicker('time')}

                                style={[styles.firstrowInput, this.state.showerror && this.state.timepicked == "" ? { borderColor: 'red' } : {}]}
                            ><Text>{this.state.timepicked}</Text></TouchableOpacity>
                        </View>
                    </View>


                    <View style={[styles.firstrowVw, styles.secondrowVw]}>
                        <View>
                            <Text>{value.name.field_title}</Text>
                            <TextInput
                                style={[styles.firstrowInput, this.state.showerror && this.state.name == "" ? { borderColor: 'red' } : {}]}
                                value={this.state.name}
                                onChangeText={(val) => this.handleInputChange('name', val)}
                            />
                        </View>

                        <View>
                            <Text>{value.phone.field_title}</Text>
                            <TextInput
                                keyboardType="phone-pad"
                                value={this.state.phone}
                                style={[styles.firstrowInput, this.state.showerror && this.state.phone == "" ? { borderColor: 'red' } : {}]}
                                onChangeText={(val) => this.handleInputChange('phone', val)}

                            />
                        </View>
                    </View>


                    <View style={styles.secondrowVw}>
                        <View>
                            <Text>{value.email.field_title}</Text>
                            <TextInput
                                value={this.state.email}
                                keyboardType="email-address"
                                style={[styles.secondrowInput, this.state.showerror && this.state.email == "" ? { borderColor: 'red' } : {}]}
                                onChangeText={(val) => this.handleInputChange('email', val)}

                            />
                        </View>

                    </View>

                    <View style={styles.secondrowVw}>
                        <View>
                            <Text>{value.message.field_title}</Text>
                            <TextInput
                                multiline
                                blurOnSubmit
                                value={this.state.message}
                                style={[styles.message, , this.state.showerror && this.state.message == "" ? { borderColor: 'red' } : {}]}
                                onChangeText={(val) => this.handleInputChange('message', val)}

                            />
                        </View>
                    </View>

                    <TouchableOpacity
                        style={styles.mainBtn}
                        onPress={() => this.scheduleTour()}
                    >
                        {
                            this.state.loading ? [
                                <Helpers.Lumper lumper={true} color={Helpers.Theme.light} />

                            ] : [
                                    <Text style={[styles.btnTxt]}>{value.btn_text}</Text>
                                ]
                        }
                    </TouchableOpacity>

                    {this.state.showdatepicker &&
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={this.state.date}
                            mode={this.state.mode}
                            is24Hour={true}
                            style={{ flex: 1 }}

                            onChange={(event, value) => this.onChange(event, value)}


                        />}
                </View>





            </View>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        settings: state.intro.settings,

    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ScheduleTour);