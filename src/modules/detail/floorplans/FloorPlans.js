import React, { Component } from 'react';
import {
    View, Text, Image, FlatList, TouchableOpacity
} from 'react-native'
import { connect } from 'react-redux'
import * as Helpers from '../../../helpers/Exporter'
import { styles } from './FloorPlans.style'
import Collapsible from 'react-native-collapsible';

class FloorPlans extends Component {

    constructor(props) {
        super(props)
        this.state = {
            plans: [
                {
                    id: 0,
                    title: 'Ground floor',
                    bath: '3',
                    bed: '4',
                    size: '1500 Sq ft',
                    img: Helpers.Images.floorplanimg,
                    description: 'The floor starts with excellent view of main hall. After that you have two rooms at left with attached bath. On the right you have 2 more rooms with attached baths',
                    price: '2000 per month',
                    isCollapsed: true
                },
                {
                    id: 1,
                    title: 'Second floor',
                    bath: '3',
                    bed: '4',
                    size: '1500 Sq ft',
                    description: 'The floor starts with excellent view of main hall. After that you have two rooms at left with attached bath. On the right you have 2 more rooms with attached baths',
                    price: '2000 per month',
                    img: Helpers.Images.floorplanimg,
                    isCollapsed: false
                },


                {
                    id: 1,
                    title: 'Third floor',
                    bath: '3',
                    bed: '4',
                    price: '2000 per month',
                    size: '1500 Sq ft',
                    description: 'The floor starts with excellent view of main hall. After that you have two rooms at left with attached bath. On the right you have 2 more rooms with attached baths',
                    img: Helpers.Images.floorplanimg,
                    isCollapsed: true
                },

                {
                    id: 1,
                    title: 'Third floor',
                    bath: '3',
                    bed: '4',
                    price: '2000 per month',
                    description: 'The floor starts with excellent view of main hall. After that you have two rooms at left with attached bath. On the right you have 2 more rooms with attached baths',
                    size: '1500 Sq ft',
                    img: Helpers.Images.floorplanimg,
                    isCollapsed: true
                },
            ],
            loading: true,
            plans: [],


        }


    }

    componentDidMount() {
        let dummyplans = []
        this.props.route.params.value.forEach((item,index) => {
            let itemx = item
            if(index==0){
                itemx.isCollapsed = false

            }else{
                itemx.isCollapsed = true

            }
            dummyplans.push(itemx)
        })
        this.setState({ loading: false, plans: dummyplans })
    }

    toggleItem = (index) => {
        let dummylist = this.state.plans

        dummylist[index].isCollapsed = !dummylist[index].isCollapsed

        this.setState({ plans: dummylist })


    }


    render() {
        let { value, title } = this.props.route.params

        return (
            <View style={[Helpers.GLOBAL_SHEET.maxHWC, styles.container]}>
                <Helpers.Header
                    navigation={this.props.navigation}
                    title={title}

                />

                <View style={styles.mainlistVw}>
                    {
                        this.state.loading ? [
                            <View style={{ position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 }}>
                                <Helpers.Lumper lumper={true} color={this.props.settings.primary_clr} />
                            </View>

                        ] : [
                                <FlatList
                                    data={this.state.plans}
                                    keyExtractor={item => item.id}
                                    renderItem={({ item, index }) => {
                                        return (
                                            <>
                                                <View style={styles.listitemVw}>
                                                    <View style={styles.listitemVw_txtcontainerVw}>
                                                        <Text>{item.floor_name}</Text>
                                                        <View style={styles.txtsVw}>
                                                            <Text style={styles.firsttxt}>{item.floor_rooms.key}: </Text>
                                                            <Text>{item.floor_rooms.value}</Text>

                                                            <Text style={styles.txts}>{item.floor_bath.key}: </Text>
                                                            <Text>{item.floor_bath.value}</Text>


                                                            <Text style={styles.txts}>{item.floor_size.key}: </Text>
                                                            <Text>{item.floor_size.value}</Text>
                                                        </View>
                                                    </View>

                                                    <TouchableOpacity
                                                        activeOpacity={1}
                                                        onPress={() => this.toggleItem(index)}
                                                        style={[styles.collapseBtn, !item.isCollapsed ? styles.collapseBtnShown : {}]}>
                                                        <Image
                                                            source={!item.isCollapsed ? Helpers.Images.downArrow : Helpers.Images.next}
                                                            style={[styles.collapseBtn_Img, !item.isCollapsed ? styles.collapseBtn_ImgShown : {tintColor:Helpers.Theme.dark}]}
                                                        />

                                                    </TouchableOpacity>
                                                </View>

                                                <Collapsible collapsed={item.isCollapsed}>
                                                    <View style={styles.collapseitemVw}>
                                                        <Image
                                                            source={{ uri: item.floor_img }}
                                                            style={styles.collapseitemVw_Img}
                                                        />

                                                        <Text style={[styles.collapseitemVw_firstTxt, Helpers.Typography.four]}>{item.floor_price.key}: <Text style={styles.collapseitemVw_priceTxt}>{item.floor_price.value}</Text></Text>
                                                        <Text style={styles.collapseitemVw_descTxt}>{item.floor_desc}</Text>
                                                    </View>
                                                </Collapsible>
                                            </>
                                        )
                                    }}

                                />
                            ]
                    }

                </View>



            </View>
        );
    }
}






const mapStateToProps = (state) => {
    return {
        settings: state.intro.settings,

    }

}
const mapDispatchToProps = (dispatch) => {
    return {
        // saveUser: params => dispatch(Actions.saveUser(params))
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(FloorPlans);