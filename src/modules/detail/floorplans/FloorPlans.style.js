import {StyleSheet} from 'react-native'
import * as Helpers from '../../../helpers/Exporter'
import {WP as wp} from '../../../helpers/Exporter'

export const styles = StyleSheet.create({
    container: {
        backgroundColor: Helpers.Theme.listingBG
    },

    mainlistVw: {
        marginTop: wp(5), marginBottom: wp(10)
    },
    listitemVw: {
        borderWidth: 1, backgroundColor: Helpers.Theme.introBg,
        borderColor: Helpers.Theme.blueBtnBorder,
        height: wp(16), width: wp(90),
        marginBottom: wp(3), flexDirection: 'row'
    },
    listitemVw_txtcontainerVw: {
        height: wp(16), width: wp(80), paddingLeft: wp(3), justifyContent: 'center',
       

    },
    txtsVw: {
        flexDirection: 'row', marginTop: wp(1),
        
    },
    firsttxt:{ color: Helpers.Theme.darkgrey },
    txts:{
        color: Helpers.Theme.darkgrey, marginLeft: wp(2)
    },

    collapseBtn:{
        height: wp(16), width: wp(10),
        borderWidth: 1, borderColor: Helpers.Theme.blueBtnBorder,
        alignItems: 'center', justifyContent: 'center'
    },
    collapseBtnShown:{ backgroundColor: Helpers.Theme.blueBtnBg },
    collapseBtn_Img:{ height: wp(3), width: wp(3) },
    collapseBtn_ImgShown:{ tintColor: Helpers.Theme.light },
    collapseitemVw:{ paddingVertical: wp(2), width: wp(90), marginBottom: wp(3) },
    collapseitemVw_Img:{ height: wp(50), width: wp(90), resizeMode: 'contain' },
    collapseitemVw_firstTxt:{ marginTop: wp(3), color: Helpers.Theme.darkgrey },
    collapseitemVw_descTxt:{ marginTop: wp(1) },
    collapseitemVw_priceTxt:{ color: Helpers.Theme.black }  
})
