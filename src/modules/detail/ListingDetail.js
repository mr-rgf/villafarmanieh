import React, {Component} from 'react';
import {
    View, Text, TouchableOpacity,
    FlatList, Image, Share, ScrollView, Linking, BackHandler,StyleSheet
} from 'react-native';

import * as Actions from '../../store/actions/allActions';
import * as Helpers from '../../helpers/Exporter';
import {WP as wp} from '../../helpers/Exporter';
import Api from '../../network/Api';

import {styles} from './ListingDetail.style';
import {connect} from 'react-redux';

import Carousel, {Pagination} from 'react-native-snap-carousel';
import {WebView} from 'react-native-webview';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import Strings from '../../assets/string';
import HTMLView from 'react-native-htmlview';


const icons=[];
icons['کد ملک']=Helpers.Images.house_code;
icons['قیمت']=Helpers.Images.pieـchart;
icons['اتاق خواب']=Helpers.Images.bed2;
icons['پارکینگ']=Helpers.Images.parking2;
icons['حمام']=Helpers.Images.bathroom2;
icons['سال ساخت']=Helpers.Images.calendar_variant;
icons['واحد پولی']=Helpers.Images.money2;
icons['مساحت زمین']=Helpers.Images.square;
icons['متراژ ملک']=Helpers.Images.area_select;
icons['نوع ملک']=Helpers.Images.menu;


const webStyles = StyleSheet.create({
    p: {  
      fontFamily: "IRANSansWeb(FaNum)"
    },
    pre : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    h1 : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    h2 : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    h3 : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    h4 : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    h5 : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    h6 : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    strong : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    em : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    b : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    mark : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    small : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    del : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    del : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    sub : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    span : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    sup : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
    a : {  
        fontFamily: "IRANSansWeb(FaNum)"
    },
});

class ListingDetail extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeIndex: 0,
            carouselItems: [
                {
                    title: 'Item 1',
                    text: 'Text 1',
                    img: Helpers.Images.dummylistItem_one,
                },
                {
                    title: 'Item 2',
                    text: 'Text 2',
                    img: Helpers.Images.dummylistItem_one,

                },
                {
                    title: 'Item 3',
                    text: 'Text 3',
                    img: Helpers.Images.dummylistItem_one,
                },
                {
                    title: 'Item 4',
                    text: 'Text 4',
                    img: Helpers.Images.dummylistItem_one,
                },
                {
                    title: 'Item 5',
                    img: Helpers.Images.dummylistItem_one,
                    text: 'Text 5',
                },
            ],

            features: [
                {
                    id: 0,
                    name: 'Air conditioning',
                    img: Helpers.Images.cooler,

                },
                {
                    id: 1,
                    name: 'Balcony',
                    img: Helpers.Images.balcony,

                },
                {
                    id: 2,
                    name: 'Air conditioning',
                    img: Helpers.Images.firealarm,

                },
                {
                    id: 3,
                    name: 'Balcony',
                    img: Helpers.Images.cooler,

                },
                {
                    id: 4,
                    name: 'Air conditioning',
                    img: Helpers.Images.firealarm,

                },
                {
                    id: 5,
                    name: 'Balcony',
                    img: Helpers.Images.balcony,

                },
                {
                    id: 6,
                    name: 'Balcony',
                    img: Helpers.Images.firealarm,


                },
            ],

            overview: [
                {
                    id: 0,
                    title: 'Property ID',
                    value: 'PT-223-AXZR',
                },

                {
                    id: 1,
                    title: 'Property Type',
                    value: 'Shop',
                },

                {
                    id: 2,
                    title: 'Price',
                    value: '90,000$ /Per year',
                },

                {
                    id: 3,
                    title: 'Currency Type',
                    value: 'AUD ($)',
                },

                {
                    id: 4,
                    title: 'Bedrooms',
                    value: '28',
                },

                {
                    id: 5,
                    title: 'Bathrooms',
                    value: '28',
                },


                {
                    id: 6,
                    title: 'Garages',
                    value: '4',
                },

                {
                    id: 7,
                    title: 'Year Built',
                    value: 'August 2019',
                },

                {
                    id: 8,
                    title: 'Property Size',
                    value: '30,000 Sq ft',
                },

                {
                    id: 9,
                    title: 'Land Area',
                    value: '40,000 Sq ft',
                },

                {
                    id: 10,
                    title: 'Property Id',
                    value: 'PT-223-AXZR',
                },


            ],


            detailList: [
                {
                    id: 0,
                    name: 'Location Map',
                    img: Helpers.Images.locationmap,
                },
                {
                    id: 1,
                    name: '360 Virtual Tour',
                    img: Helpers.Images.virtualtour,
                },

                {
                    id: 2,
                    name: 'Whats Nearby',
                    img: Helpers.Images.nearby,
                },
                {
                    id: 3,
                    name: 'Attachments',
                    img: Helpers.Images.attachments,
                },
                {
                    id: 4,
                    name: 'Floor Plans',
                    img: Helpers.Images.floorplan,
                },
                {
                    id: 5,
                    name: 'Mortgage Calculator',
                    img: Helpers.Images.calculator,
                },

                {
                    id: 6,
                    name: 'Schedule a tour',
                    img: Helpers.Images.schedule,
                },
                {
                    id: 7,
                    name: '5.0 Average Based on Ratings',
                    img: Helpers.Images.rating,
                },
                {
                    id: 8,
                    name: 'Write a Review',
                    img: Helpers.Images.writereview,
                },
            ],

            currentDetail: null,

            showmore: false,
            isImageViewVisle: false,
            imageindex: 0,
        };
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount() {
        this.fetchData(this.props.route.params.listingId);
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    handleBackButtonClick() {
        if (this.state.isImageViewVisle) {
            this.setState({
                isImageViewVisle: false,
            });
        } else {
            this.props.navigation.goBack(null);
        }
        return true;
    }

    fetchData = async (id) => {
        var response;
        this.props.setloading(true);

        let param = {
            property_id: id,
        };
        response = await Api.post('property-detial', param);

        if (response.success == true) {
            let dummyres = response.data.final_content.listing_detial;
            let dummygallery = [];
            dummyres.gallery.forEach((item) => {
                dummygallery.push({'uri': item.url});
            });
            dummyres.galleryuris = dummygallery;
            this.setState({currentDetail: dummyres});
        }
        this.props.setloading(false);
    };

    opendetails = (item) => {
        switch (item.id) {
            case 0:
                this.props.navigation.navigate('locationmap', {value: item.value, title: item.title});
                return;
            case 1:
                this.props.navigation.navigate('virtualtour', {value: item.value, title: item.title});
                return;

            case 2:
                this.props.navigation.navigate('whatsnearby', {value: item.value, title: item.title});
                return;

            case 3:
                this.props.navigation.navigate('attachments', {value: item.value, title: item.title});
                return;


            case 4:
                this.props.navigation.navigate('floorplans', {value: item.value, title: item.title});
                return;


            case 5:
                this.props.navigation.navigate('mortgagecalculator', {
                    value: item.value,
                    title: item.title,
                    price: item.price,
                });
                return;

            case 6:
                this.props.navigation.navigate('scheduletour', {
                    value: item.value,
                    title: item.title,
                    property_id: item.property_id,
                });
                return;

            case 7:
                this.props.navigation.navigate('reviewrating', {value: item.value, title: item.title});
                return;

            case 8:
                this.props.navigation.navigate('writereview', {value: item.value, title: item.title});
                return;

            default:
                return;
        }
    };

    openExternalApp = (which, val) => {
        switch (which) {
            case 'whatsapp':
                Linking.openURL(`whatsapp://send?phone=${val}`);
                return;
            case 'phone':
                Linking.openURL(`tel:${val}`);
                return;
            default:
                return;
        }

    };
    onShare = async () => {
        try {
            const result = await Share.share({
                title: 'Propertya',
                message: 'Find this exciting property ',
                // url: 'https://play.google.com/store/apps/details?id=nic.goi.aarogyasetu&hl=en'
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };


    handleLoadmore = () => {
        this.setState({showmore: true});
    };
    handleImageClick = (index) => {
        // console.log('here')
        this.setState({isImageViewVisle: true, imageindex: index});
    };
    _renderItem = ({item, index}) => {
        return (
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => this.handleImageClick(index)}
                style={styles.sliderItem_Vw}>

                <Image
                    source={{uri: item.url}}
                    style={Helpers.GLOBAL_SHEET.maxHW}
                />
                <Image
                    source={Helpers.Images.shadow}
                    style={[styles.abs, Helpers.GLOBAL_SHEET.maxHW]}
                />

                <View style={styles.sliderBackBtn}>
                    <Helpers.BackButtonRound navigation={this.props.navigation}/>
                </View>

                <View style={styles.sliderRightbtns_Vw}>
                    <TouchableOpacity
                        onPress={() => this.onShare()}
                        style={[styles.header_backBtn, Helpers.GLOBAL_SHEET.boxWithShadow]}
                    >
                        <Image
                            source={Helpers.Images.share}
                            style={styles.header_backImg}
                        />
                    </TouchableOpacity>

                </View>
            </TouchableOpacity>

        );
    };
    
     

    render() {
        let {currentDetail} = this.state;
        if (this.props.loading) {
            return (
                <SkeletonPlaceholder style={Helpers.GLOBAL_SHEET.maxHWCC}>
                    <SkeletonPlaceholder.Item width={wp(100)} height={wp(70)} borderWidth={1} borderColor={'white'}>

                        <SkeletonPlaceholder.Item alignSelf={'center'} height={wp(10)} width={wp(10)}
                                                  position={'absolute'} top={Helpers.WP(5)} left={Helpers.WP(5)}
                                                  borderRadius={wp(5)} borderColor="white" borderWidth={1}
                        />
                        <SkeletonPlaceholder.Item alignSelf={'center'} height={wp(10)} width={wp(10)}
                                                  position={'absolute'} top={Helpers.WP(5)} right={Helpers.WP(3)}
                                                  borderRadius={wp(5)} borderColor="white" borderWidth={1}
                        />


                    </SkeletonPlaceholder.Item>
                    <SkeletonPlaceholder.Item alignSelf={'center'} height={wp(18)} width={wp(94)} marginTop={wp(-8)}
                                              borderRadius={wp(2)} borderColor="white" borderWidth={1}
                    />

                    <SkeletonPlaceholder.Item marginTop={wp(5)} marginLeft={wp(5)}>
                        <SkeletonPlaceholder.Item
                            width={wp(25)}
                            height={wp(5)}
                            borderRadius={wp(1)} borderColor="white" borderWidth={1}/>
                        <SkeletonPlaceholder.Item
                            marginTop={wp(3)}
                            width={wp(60)}
                            height={wp(8)}
                            borderRadius={wp(1)} borderColor="white" borderWidth={1}
                        />
                        <SkeletonPlaceholder.Item
                            marginTop={wp(3)}
                            width={wp(60)}
                            height={wp(5)}
                            borderRadius={wp(1)} borderColor="white" borderWidth={1}
                        />

                    </SkeletonPlaceholder.Item>


                    <SkeletonPlaceholder.Item height={wp(12)} width={wp(100)} marginTop={wp(10)}
                                              borderRadius={wp(1)} borderColor="white" borderWidth={1}
                    />
                </SkeletonPlaceholder>
            );
        }
        return (
            <View style={[Helpers.GLOBAL_SHEET.maxHWC, styles.container]}>
                <ScrollView style={{flex: 1}}>

                    {
                        this.props.loading ? [] : [
                            currentDetail == null ? [] : [
                                <View style={{alignItems: 'center'}}>

                                    {/* Slider start */}
                                    {
                                        currentDetail != null ? [
                                            <View style={styles.sliderVw}>
                                                <Carousel
                                                    layout={'default'}
                                                    ref={ref => this.carousel = ref}
                                                    data={currentDetail.gallery}
                                                    sliderWidth={wp(100)}
                                                    itemWidth={wp(100)}
                                                    renderItem={this._renderItem}
                                                    onSnapToItem={index => this.setState({activeIndex: index})}/>
                                            </View>,
                                        ] : []
                                    }

                                    {/* Slider end */}


                                    {/* Sticky header start */}
                                    <View style={[styles.stickyheader, Helpers.GLOBAL_SHEET.boxWithShadow]}>
                                        <View style={styles.stickyheader_Vw}>
                                            <Image
                                                source={Helpers.Images.bedDetail}
                                                style={styles.stickyheader_Img}
                                            />
                                            <Text style={[styles.stickyheader_txt1]}>
                                                <Text style={[styles.stickyheader_txt2]}>{currentDetail.rooms}</Text></Text>
                                            {/* <Text style={[styles.stickyheader_txt1]}>06<Text style={[styles.stickyheader_txt2]}> Beds</Text></Text> */}
                                        </View>

                                        <View style={styles.stickyheader_Vw}>
                                            <Image
                                                source={Helpers.Images.bathDetail}
                                                style={styles.stickyheader_Img}
                                            />
                                            <Text style={[styles.stickyheader_txt1]}><Text
                                                style={[styles.stickyheader_txt2]}>{currentDetail.baths}</Text></Text>

                                        </View>

                                        <View style={styles.stickyheader_Vw}>
                                            <Image
                                                source={Helpers.Images.garageDetail}
                                                style={styles.stickyheader_Img}
                                            />
                                            <Text style={[styles.stickyheader_txt1]}><Text
                                                style={[styles.stickyheader_txt2]}>{currentDetail.garage}</Text></Text>

                                        </View>
                                    </View>
                                    {/* Sticky header end */}

                                    {/* Details start */}
                                    <View style={styles.details_Vw}>
                                        
                                    <Text style={[Helpers.Typography.five, {marginTop: wp(1)}]}>
                                            {currentDetail.title}
                                        </Text>
                                        <View style={{
                                                    backgroundColor: '#fccacf',
                                                    alignSelf: 'flex-start',
                                                    paddingRight: wp(2),
                                                    paddingLeft: wp(2),
                                                    paddingBottom: wp(.5),
                                                    marginTop:wp(2),
                                                    borderRadius:5
                                                }}>
                                        <Text style={[Helpers.Typography.fourPointFive, styles.headingbold]}>
                                            {currentDetail.main_price}
                                        </Text>

                                
                                        </View>
                                        <View style={styles.address_Vw}>
                                            <Image
                                                source={Helpers.Images.pinpoint}
                                                style={styles.address_Img}
                                            />
                                            <Text
                                                style={[Helpers.Typography.three, styles.address_Txt]}>{currentDetail.address}</Text>

                                        </View>

                                        {/* Tags row start */}
                                        <View style={styles.tagsrow_Vw}>
                                            {
                                                currentDetail.offer != null && currentDetail.offer != '' ?
                                                    <TouchableOpacity
                                                        style={[styles.tag1_btn, {backgroundColor: currentDetail.offer_color}]}>
                                                        <Text
                                                            style={[Helpers.Typography.three, styles.tagsTxt]}>{currentDetail.offer}</Text>
                                                    </TouchableOpacity> : []
                                            }

                                            {
                                                currentDetail.label_name != null && currentDetail.label_name != '' ?
                                                    [
                                                        <TouchableOpacity
                                                            style={[styles.tag2_btn, {backgroundColor: currentDetail.label_color}]}>
                                                            <Text
                                                                style={[Helpers.Typography.three, styles.tagsTxt]}>{currentDetail.label_name}</Text>
                                                        </TouchableOpacity>,
                                                    ] : []

                                            }

                                            {
                                                currentDetail.is_featured != null && currentDetail.is_featured == '1' ?
                                                    <TouchableOpacity
                                                        style={styles.tag3_btn}>
                                                        <Text
                                                            style={[Helpers.Typography.three, styles.tagsTxt]}>{currentDetail.featured_txt}</Text>
                                                    </TouchableOpacity> : []
                                            }


                                            <TouchableOpacity
                                                style={styles.tagCam_btn}>
                                                <Image
                                                    source={Helpers.Images.camera}
                                                    style={styles.tagCam_btn_Img}
                                                />
                                                <Text
                                                    style={[Helpers.Typography.three, styles.tagsTxt]}>{currentDetail.gallery_count}</Text>
                                            </TouchableOpacity>
                                        </View>
                                        {/* Tags row end */}

                                    </View>
                                    {/* Details end */}

                                    {/* Last updated start */}

                                    {/* Last updated end */}


                                    {/* Description section start */}
                                    {
                                        currentDetail.description != null && currentDetail.description != '' ? [
                                            <View  style={styles.overviewVw}>  
                                            <Text
                                                    style={[Helpers.Typography.fourPointFive, styles.headingbold]}>{currentDetail.description_heading}</Text>
                                            <View>
                                               <HTMLView
                                                   value={currentDetail.description}
                                                   stylesheet={webStyles}
                                                />
                                            </View>
                                            </View>,
                                        ] : []
                                    }

                                    {/* Description section end */}

                                    {/* Features/amenties section start */}
                                    {
                                        currentDetail.features_enable ? [
                                            <View style={styles.featuresVw}>
                                                <Text
                                                    style={[Helpers.Typography.fourPointFive, styles.headingbold]}>{currentDetail.features_heading}</Text>
                                                <View style={styles.featureslistouterVw}>
                                                    <FlatList
                                                        data={currentDetail.features}
                                                        numColumns={3}
                                                        keyExtractor={(item) => item.id}
                                                        renderItem={({item, index}) => {
                                                            if (!this.state.showmore) {
                                                                if (index <= 5) {
                                                                    if (index == 5) {
                                                                        return (

                                                                            <TouchableOpacity
                                                                                onPress={() => this.handleLoadmore()}
                                                                                style={styles.featuresVw_list_moreVw}>
                                                                                <Text
                                                                                    style={[{color: Helpers.Theme.light}, Helpers.Typography.five]}>+</Text>
                                                                                <Text
                                                                                    style={[{color: Helpers.Theme.light}, Helpers.Typography.four]}>{currentDetail.features.length - 5} امکان
                                                                                    دیگر</Text>
                                                                            </TouchableOpacity>
                                                                        );
                                                                    } else {
                                                                        return (

                                                                            <View style={styles.featuresVw_list_Vw}>
                                                                                <Image
                                                                                    source={{uri: item.url}}
                                                                                    style={styles.featuresVw_list_Vw_Img}
                                                                                />
                                                                                <Text
                                                                                    style={[Helpers.Typography.three, styles.featuresVw_list_Vw_txt]}>{item.name}</Text>

                                                                            </View>
                                                                        );
                                                                    }
                                                                }


                                                            } else {
                                                                return (
                                                                    <View style={styles.featuresVw_list_Vw}>
                                                                        <Image
                                                                            source={{uri: item.url}}
                                                                            style={styles.featuresVw_list_Vw_Img}
                                                                        />
                                                                        <Text
                                                                            style={[Helpers.Typography.three, styles.featuresVw_list_Vw_txt]}>{item.name}</Text>

                                                                    </View>
                                                                );

                                                            }

                                                        }}
                                                    />
                                                </View>

                                            </View>,

                                        ] : []
                                    }

                                    {/* Features/amenties section end */}

                                    {/* Overview section start */}
                                    {
                                        currentDetail.overview_enable ? [
                                            <View style={styles.overviewVw}>
                                                <Text
                                                    style={[Helpers.Typography.fourPointFive, styles.headingbold]}>{currentDetail.overview_heading}</Text>
                                                <View style={[styles.overviewVw_listouterVw]}>
                                                    <FlatList
                                                        data={currentDetail.overview}
                                                        numColumns={2}
                                                        keyExtractor={(item) => item.id}
                                                        renderItem={({item, index}) => {
                                                            return (
                                                                <View style={[styles.overviewVw_listVw,{marginTop:wp(2),marginBottom:wp(2)}]}>
                                                                     <View style={{display:'flex',flexDirection:'row',alignItems:'center'}}>
                                                                     <Image
                                                                        source={icons[item.heading]}
                                                                        style={{ height: wp(6), width: wp(6),
                                                                            resizeMode: 'contain',tintColor:this.props.settings.primary_clr}}
                                                                     />
                                                                     <View style={{marginLeft:wp(2)}}>
                                                                
                                                                      <Text
                                                                        style={{fontSize:wp(4)}}>{item.heading}</Text>
                                                                        <Text
                                                                        style={[{color:Helpers.Theme.darkgrey,textAlign:'left',fontSize:wp(3.5)}]}>{item.value}</Text>
                                                                        </View>
                                                                    </View>        
                                                                </View>
                                                            );
                                                        }}
                                                    />
                                                </View>

                                            </View>,

                                        ] : []
                                    }

                                    {/* Overview section end */}

                                    {/* Listing address section start */}
                                    {
                                        currentDetail.locations_enable ? [
                                            <View style={styles.listingaddressVw}>
                                                <Text
                                                    style={[Helpers.Typography.fourPointFive, styles.listingaddressVw_heading]}>{currentDetail.locations.text}</Text>
                                                <FlatList
                                                    data={currentDetail.locations.locs}
                                                    keyExtractor={(item) => item.id}
                                                    renderItem={({item, index}) => {

                                                        return (

                                                            <View style={styles.listingaddressVw_rowVw}>

                                                                <View style={styles.listingaddressVw_rowVw_firstVw}>

                                                                    <View
                                                                        style={styles.listingaddressVw_rowVw_firstVw_ImgVw}>
                                                                        <Image source={Helpers.Images.globe}
                                                                               style={styles.listingaddressVw_rowVw_firstVw_Img}
                                                                        />
                                                                    </View>

                                                                    <View style={styles.marginleft2}>
                                                                        <Text>{item.level}: <Text
                                                                            style={[Helpers.Typography.three, styles.overviewVw_listVw_valueTxt]}>{item.name}</Text></Text>
                                                                    </View>
                                                                </View>

                                                                <View style={styles.line}/>
                                                            </View>


                                                        );


                                                    }
                                                    }/>
                                                {/* {
                                                    currentDetail.locations.locs.map((item, index) => {
                                                        return (
                                                            <>
                                                                <View style={styles.listingaddressVw_rowVw}>

                                                                    <View style={styles.listingaddressVw_rowVw_firstVw}>

                                                                        <View style={styles.listingaddressVw_rowVw_firstVw_ImgVw}>
                                                                            <Image source={Helpers.Images.globe}
                                                                                style={styles.listingaddressVw_rowVw_firstVw_Img}
                                                                            />
                                                                        </View>

                                                                        <View style={styles.marginleft2}>
                                                                            <Text>City:</Text>
                                                                            <Text>Brooklyn</Text>
                                                                        </View>
                                                                    </View>
                                                                    {
                                                                        index == 1 || index == 3 ? [
                                                                            <View style={styles.listingaddressVw_rowVw_firstVw}>

                                                                                <View style={styles.listingaddressVw_rowVw_firstVw_ImgVw}>
                                                                                    <Image source={Helpers.Images.navigate}
                                                                                        style={styles.listingaddressVw_rowVw_firstVw_Img}
                                                                                    />
                                                                                </View>

                                                                                <View style={styles.marginleft2}>
                                                                                    <Text>State:</Text>
                                                                                    <Text>New York</Text>
                                                                                </View>
                                                                            </View>

                                                                        ] : []
                                                                    }

                                                                </View>
                                                            </>

                                                        )

                                                    })
                                                } */}


                                                {/* <View style={styles.listingaddressVw_rowVw}>


                                                    <View style={styles.listingaddressVw_rowVw_firstVw}>

                                                        <View style={styles.listingaddressVw_rowVw_firstVw_ImgVw}>
                                                            <Image source={Helpers.Images.globe}
                                                                style={styles.listingaddressVw_rowVw_firstVw_Img}
                                                            />
                                                        </View>

                                                        <View style={styles.marginleft2}>
                                                            <Text>City:</Text>
                                                            <Text>Brooklyn</Text>
                                                        </View>
                                                    </View>

                                                    <View style={styles.listingaddressVw_rowVw_firstVw}>

                                                        <View style={styles.listingaddressVw_rowVw_firstVw_ImgVw}>
                                                            <Image source={Helpers.Images.hotel}
                                                                style={styles.listingaddressVw_rowVw_firstVw_Img}
                                                            />
                                                        </View>

                                                        <View style={styles.marginleft2}>
                                                            <Text>Country:</Text>
                                                            <Text>USA</Text>
                                                        </View>
                                                    </View>

                                                </View> */}


                                            </View>,

                                        ] : []
                                    }

                                    {/* Listing address section end */}


                                    {/* DETAILS LIST section start */}

                                    <View style={styles.detailslistVw}>
                                        <View style={styles.marginTop3}>
                                            {
                                                currentDetail.map_enable ? [
                                                    <View style={styles.detailslistVw_listVw}>
                                                        <Image
                                                            source={Helpers.Images.locationmap}
                                                            style={styles.detailslistVw_listVw_Img}
                                                        />
                                                        <Text
                                                            style={[Helpers.Typography.fourPointFive, styles.detailslistVw_listVw_nameTxt]}>{Strings.map_address}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => this.opendetails({
                                                                id: 0,
                                                                value: currentDetail.maps,
                                                                title: currentDetail.map_heading,
                                                            })}
                                                            style={styles.detailslistVw_listVw_nextBtnVw}>
                                                            <Image
                                                                source={Helpers.Images.next}
                                                                style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                                            />
                                                        </TouchableOpacity>

                                                    </View>,
                                                ] : []
                                            }

                                            {
                                                currentDetail.tour_enable ? [
                                                    <View style={[styles.detailslistVw_listVw, {borderBottomWidth: 0}]}>
                                                        <Image
                                                            source={Helpers.Images.virtualtour}
                                                            style={styles.detailslistVw_listVw_Img}
                                                        />
                                                        <Text
                                                            style={[Helpers.Typography.fourPointFive, styles.detailslistVw_listVw_nameTxt]}>{currentDetail.tour_heading}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => this.opendetails({
                                                                id: 1,
                                                                value: currentDetail.tour_iframe,
                                                                title: currentDetail.tour_heading,
                                                            })}
                                                            style={styles.detailslistVw_listVw_nextBtnVw}>
                                                            <Image
                                                                source={Helpers.Images.next}
                                                                style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                                            />
                                                        </TouchableOpacity>

                                                    </View>,
                                                ] : []
                                            }


                                            {
                                                currentDetail.nearby_enable ? [
                                                    <View style={styles.detailslistVw_listVw}>
                                                        <Image
                                                            source={Helpers.Images.nearby}
                                                            style={styles.detailslistVw_listVw_Img}
                                                        />
                                                        <Text
                                                            style={[Helpers.Typography.fourPointFive, styles.detailslistVw_listVw_nameTxt]}>{currentDetail.nearby_heading}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => this.opendetails({
                                                                id: 2,
                                                                value: currentDetail.nearby,
                                                                title: currentDetail.nearby_heading,
                                                            })}
                                                            style={styles.detailslistVw_listVw_nextBtnVw}>
                                                            <Image
                                                                source={Helpers.Images.next}
                                                                style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                                            />
                                                        </TouchableOpacity>

                                                    </View>,
                                                ] : []
                                            }

                                            {
                                                currentDetail.attachments_enable ? [
                                                    <View style={styles.detailslistVw_listVw}>
                                                        <Image
                                                            source={Helpers.Images.attachments}
                                                            style={styles.detailslistVw_listVw_Img}
                                                        />
                                                        <Text
                                                            style={[Helpers.Typography.fourPointFive, styles.detailslistVw_listVw_nameTxt]}>{currentDetail.attachments_heading}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => this.opendetails({
                                                                id: 3,
                                                                value: currentDetail.attachments,
                                                                title: currentDetail.attachments_heading,
                                                            })}
                                                            style={styles.detailslistVw_listVw_nextBtnVw}>
                                                            <Image
                                                                source={Helpers.Images.next}
                                                                style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                                            />
                                                        </TouchableOpacity>

                                                    </View>,
                                                ] : []
                                            }


                                            {
                                                currentDetail.plans_enable ? [
                                                    <View style={styles.detailslistVw_listVw}>
                                                        <Image
                                                            source={Helpers.Images.floorplan}
                                                            style={styles.detailslistVw_listVw_Img}
                                                        />
                                                        <Text
                                                            style={[Helpers.Typography.fourPointFive, styles.detailslistVw_listVw_nameTxt]}>{currentDetail.plans_heading}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => this.opendetails({
                                                                id: 4,
                                                                value: currentDetail.plans,
                                                                title: currentDetail.plans_heading,
                                                            })}
                                                            style={styles.detailslistVw_listVw_nextBtnVw}>
                                                            <Image
                                                                source={Helpers.Images.next}
                                                                style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                                            />
                                                        </TouchableOpacity>

                                                    </View>,
                                                ] : []
                                            }


                                            {
                                                currentDetail.reviews_enable ? [
                                                    <View style={[styles.detailslistVw_listVw]}>
                                                        <Image
                                                            source={Helpers.Images.rating}
                                                            style={styles.detailslistVw_listVw_Img}
                                                        />
                                                        <Text
                                                            style={[Helpers.Typography.fourPointFive, styles.detailslistVw_listVw_nameTxt]}>{currentDetail.reviews_heading}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => this.opendetails({
                                                                id: 7,
                                                                value: currentDetail.reviews,
                                                                title: currentDetail.reviews_heading,
                                                            })}
                                                            style={styles.detailslistVw_listVw_nextBtnVw}>
                                                            <Image
                                                                source={Helpers.Images.next}
                                                                style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                                            />
                                                        </TouchableOpacity>

                                                    </View>,
                                                ] : []
                                            }
                                            {/*
                                            {
                                                currentDetail.form_enable ? [
                                                    <View style={styles.detailslistVw_listVw}>
                                                        <Image
                                                            source={Helpers.Images.writereview}
                                                            style={styles.detailslistVw_listVw_Img}
                                                        />
                                                        <Text style={[Helpers.Typography.fourPointFive, styles.detailslistVw_listVw_nameTxt]}>{currentDetail.form_heading}</Text>

                                                        <TouchableOpacity
                                                            onPress={() => this.opendetails({ id: 8, value: currentDetail.form, title: currentDetail.form_heading })}
                                                            style={styles.detailslistVw_listVw_nextBtnVw}>
                                                            <Image
                                                                source={Helpers.Images.next}
                                                                style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                                            />
                                                        </TouchableOpacity>

                                                    </View>
                                                ] : []
                                            } */}

                                            {/* <FlatList
                                                data={this.state.detailList}
                                                keyExtractor={(item) => item.id}
                                                renderItem={({ item, index }) => {

                                                    return (
                                                        <View style={styles.detailslistVw_listVw}>
                                                            <Image
                                                                source={item.img}
                                                                style={styles.detailslistVw_listVw_Img}
                                                            />
                                                            <Text style={[Helpers.Typography.fourPointFive, styles.detailslistVw_listVw_nameTxt]}>{item.name}</Text>

                                                            <TouchableOpacity
                                                                onPress={() => this.opendetails(item)}
                                                                style={styles.detailslistVw_listVw_nextBtnVw}>
                                                                <Image
                                                                    source={Helpers.Images.next}
                                                                    style={styles.detailslistVw_listVw_nextBtnVw_Img}
                                                                />
                                                            </TouchableOpacity>

                                                        </View>
                                                    )


                                                }}
                                            /> */}
                                        </View>
                                    </View>
                                    {/* DETAILS LIST section end */}


                                    {/* Bottom buttons start */}
                                    <View style={styles.bottonbtnsVw}>
                                        {/* <TouchableOpacity
                                            style={styles.bottonbtnsVw_mailBtn}
                                        >
                                            <Image
                                                source={Helpers.Images.mail}
                                                style={styles.bottonbtnsVw_mailBtn_Img}
                                            />
                                            <Text style={styles.bottonbtnsVw_mailBtn_txt}>Mail</Text>
                                        </TouchableOpacity> */}


                                        <TouchableOpacity
                                            onPress={() => this.openExternalApp('whatsapp', currentDetail.author_detail.whatsapp_number.number)}

                                            style={styles.bottonbtnsVw_chatBtn}
                                        >
                                            <Image
                                                source={Helpers.Images.whatsapp}
                                                style={styles.bottonbtnsVw_chatBtn_Img}
                                            />
                                            <Text
                                                style={styles.bottonbtnsVw_chatBtn_txt}>{Strings.whatsapp}</Text>
                                        </TouchableOpacity>


                                        <TouchableOpacity
                                            onPress={() => this.openExternalApp('phone', currentDetail.author_detail.mobile_number.number)}

                                            style={styles.bottonbtnsVw_callBtn}
                                        >
                                            <Image
                                                source={Helpers.Images.phonecall}
                                                style={styles.bottonbtnsVw_callBtn_Img}
                                            />
                                            <Text
                                                style={styles.bottonbtnsVw_callBtn_txt}>{Strings.call}</Text>
                                        </TouchableOpacity>

                                    </View>
                                    {/* Bottom buttons end */}

                                </View>,
                            ],
                        ]
                    }


                </ScrollView>

                {this.state.isImageViewVisle &&
                <View
                    style={{
                        position: 'absolute',
                        width: '100%',
                        height: '100%',
                        backgroundColor: '#000',
                        zIndex: 99999,
                        display: 'flex',
                        justifyContent: 'center',
                    }}
                >
                    <TouchableOpacity style={{
                        width: 35,
                        height: 35,
                        position: 'absolute',
                        top: 20,
                        right: 12,
                        backgroundColor: '#FFF',
                        borderRadius: wp(50),
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }} onPress={() => {
                        this.setState({isImageViewVisle: false});
                    }}>
                        <Image style={{width: 15, height: 15}} source={Helpers.Images.closeIcon}/>
                    </TouchableOpacity>

                    {
                        currentDetail != null ? [
                            <View style={{height: wp(100), width: '100%'}}>
                                <Carousel
                                    firstItem={(currentDetail.gallery.length - this.state.imageindex) - 1}
                                    layout={'default'}
                                    ref={ref => this.carousel = ref}
                                    data={currentDetail.gallery}
                                    sliderWidth={wp(100)}
                                    itemWidth={wp(100)}
                                    renderItem={({item, index}) => {
                                        return (
                                            <View style={{height: wp(100)}}>
                                                <Image
                                                    source={{uri: item.url}}
                                                    style={Helpers.GLOBAL_SHEET.maxHW}
                                                />
                                            </View>
                                        );
                                    }}
                                    onSnapToItem={index => this.setState({activeIndex: index})}/>
                            </View>,
                        ] : []
                    }
                </View>
                }

            </View>
        );
    }

}


const mapStateToProps = (state) => {
    return {
        loading: state.intro.loading
        ,
        settings: state.intro.settings
        ,
        searchlist: state.search.searchlist
        ,
        searchresponse: state.search.searchresponse
        ,
    };

};

const mapDispatchToProps = (dispatch) => {
        return {
            setloading: params => dispatch(Actions.loading(params)),

            updateListingDetail: params => dispatch(Actions.updateListingDetail(params)),
        };
    }
;
export default connect(mapStateToProps, mapDispatchToProps)(ListingDetail);
