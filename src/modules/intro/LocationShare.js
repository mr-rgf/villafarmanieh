import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image, PermissionsAndroid } from 'react-native'
import { connect } from 'react-redux'
import * as Actions from '../../store/actions/allActions'
import * as Helpers from '../../helpers/Exporter'
import Geolocation from 'react-native-geolocation-service';
class LocationShare extends Component {


  navigate = (where) => {
    this.props.navigation.reset({
      routes: [{ name: where }]
    })
  }
  sharelocation = async () => {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
    )
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      ////
      Geolocation.getCurrentPosition(
        position => {
          const initialPosition = JSON.stringify(position);
          // this.setState({initialPosition});

          this.navigate('searchlist')

        },
        error => {
          this.navigate('searchlist')

        },
        { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
      );
      // this.watchID = Geolocation.watchPosition(position => {
      //   const lastPosition = JSON.stringify(position);
      //   this.setState({ lastPosition });
      // });
    } else {
      this.navigate('searchlist')

    }



  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topView}>
          <TouchableOpacity
            onPress={() => this.navigate('searchlist')}
            style={styles.skipBtn}>
            <Text style={[styles.skipTxt, Helpers.Typography.four, { color: this.props.settings.primary_clr }]}>{this.props.settings.skip}</Text>
          </TouchableOpacity>
        </View>

        <Image
          source={Helpers.Images.appIntroBlueHome}
          style={styles.mainImg}
        />
        <View style={styles.helpView}>
          <Text style={[styles.helpTxt, Helpers.Typography.six]}>{this.props.settings.nearby.tagline}</Text>
          <Text style={[styles.simpletxt, Helpers.Typography.four]}>{this.props.settings.nearby.desc}</Text>
        </View>



        <View style={styles.marginTop}>
          <TouchableOpacity
            onPress={() => this.sharelocation()}
            style={[styles.blueBtn, { backgroundColor: this.props.settings.primary_clr }]}>
            <Text style={styles.blueBtnTxt}>{this.props.settings.nearby.btn}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Helpers.Theme.introBg
  },

  topView: {
    height: Helpers.WP(10),
    width: '100%', justifyContent: 'center'
  },

  skipBtn: {
    alignSelf: 'flex-end',
    marginRight: Helpers.WP(4)
  },
  skipTxt: {
    color: Helpers.Theme.blueBtnBg,
    fontWeight: '500'
  },

  mainImg: {
    height: '55%', width: Helpers.WP(100),
    marginTop: Helpers.WP(5),
    resizeMode: 'contain'
  },

  helpView: {
    marginTop: Helpers.WP(10),
    alignItems: 'center'
  },
  helpTxt: {
    fontWeight: '700'
  },
  simpletxt: {
    textAlign: "center", marginTop: Helpers.WP(2)
  },



  blueBtn: {
    height: Helpers.WP(12), width: Helpers.WP(90),
    backgroundColor: Helpers.Theme.blueBtnBg,
    alignItems: 'center', justifyContent: "center",
    borderRadius: Helpers.WP(1),
    borderWidth: Helpers.WP(0.1), borderColor: Helpers.Theme.blueBtnBorder,
  },
  blueBtnTxt: {
    color: Helpers.Theme.light,
    fontWeight: '600'
  },

  marginTop: {
    marginTop: Helpers.WP(5)
  },





})

const mapStateToProps = (state) => {
  return {
    settings: state.intro.settings
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    saveUser: params => dispatch(Actions.saveUser(params))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(LocationShare);