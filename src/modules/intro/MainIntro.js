import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native'
import { connect } from 'react-redux'
import * as Actions from '../../store/actions/allActions'
import * as Helpers from '../../helpers/Exporter'
import Storage from '../../storage/Storage'
import  AsyncStorage  from "@react-native-community/async-storage"

class Main extends Component {

  navigate =async (where, type) => {
    await AsyncStorage.setItem('firsttime','true')

    let dummyfilter = this.props.filters
    let dummySP = this.props.searchparams
    if (type != undefined) {
      this.props.settings.offertype.option_dropdown.forEach((item, index) => {
        if (item.offer_slug == type) {
          dummyfilter.selectedIndex = index
          dummySP["offer-type"] = item.offer_slug

        }
      })

      this.props.savefilers(dummyfilter)
      this.props.updateSP(dummySP)
    }
    if(where=='searchlist'){
      this.props.navigation.reset({
        routes: [{ name: where }]
      });
    }else{
      this.props.navigation.navigate(where, { type: type })

    }

  }


  render() {
    return (
      <View style={styles.container}>
        <View style={styles.topView}>
          <TouchableOpacity
            onPress={() => this.navigate('searchlist')}
            style={styles.skipBtn}>
            <Text style={[styles.skipTxt, Helpers.Typography.four, { color: this.props.settings.primary_clr }]}>{this.props.settings.skip}</Text>
          </TouchableOpacity>
        </View>

        <Image
          source={Helpers.Images.appIntroHands}
          style={styles.mainImg}
        />
        <View style={styles.helpView}>
          <Text style={[styles.helpTxt, Helpers.Typography.six]}>{this.props.settings.looking_for.tagline}</Text>
        </View>

        <View style={styles.buttonsContainer}>

          <TouchableOpacity
            onPress={() => this.navigate('locationShare', this.props.settings.looking_for.status[0].offer_slug)}
            style={[styles.blueBtn, { backgroundColor: this.props.settings.primary_clr }]}>
            <Text style={styles.blueBtnTxt}>{this.props.settings.looking_for.status[0].offer}</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.navigate('locationShare', this.props.settings.looking_for.status[1].offer_slug)}
            style={[styles.blueBtn, styles.marginLeft, { backgroundColor: this.props.settings.primary_clr }]}>
            <Text style={styles.blueBtnTxt}>{this.props.settings.looking_for.status[1].offer}</Text>
          </TouchableOpacity>

        </View>

        <View style={styles.marginTop}>
          <TouchableOpacity
            onPress={() => this.navigate('searchlist')}
            style={styles.whiteBtn}>
            <Text>{this.props.settings.looking_for.bottom_btn}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: Helpers.Theme.introBg,

  },

  topView: {
    height: Helpers.WP(10),
    width: '100%', justifyContent: 'center'
  },

  skipBtn: {
    alignSelf: 'flex-end',
    marginRight: Helpers.WP(4)
  },
  skipTxt: {
    color: Helpers.Theme.blueBtnBg,
    fontWeight: '500'

  },

  mainImg: {
    height: '56%', width: Helpers.WP(100),
    marginTop: Helpers.WP(5)
  },

  helpView: {
    marginTop: Helpers.WP(10)
  },
  helpTxt: {
    fontWeight: '700'
  },

  buttonsContainer: {
    width: '100%',
    justifyContent: 'center', flexDirection: 'row',
    paddingHorizontal: Helpers.WP(4),
    marginTop: Helpers.WP(5),
  },


  blueBtn: {
    height: Helpers.WP(10), width: Helpers.WP(43),
    backgroundColor: Helpers.Theme.introBg,
    alignItems: 'center', justifyContent: "center",
    borderRadius: Helpers.WP(1),
    borderWidth: Helpers.WP(0.1),
    borderColor: Helpers.Theme.blueBtnBorder,
  },
  blueBtnTxt: {
    color: Helpers.Theme.light,
    fontWeight: '600'
  },
  marginLeft: {
    marginLeft: Helpers.WP(4)
  },
  marginTop: {
    marginTop: Helpers.WP(5)
  },

  whiteBtn: {
    height: Helpers.WP(12), width: Helpers.WP(90),
    backgroundColor: Helpers.Theme.light,
    borderWidth: Helpers.WP(0.1), borderColor: Helpers.Theme.blueBtnBorder,
    borderRadius: Helpers.WP(1),
    alignItems: 'center', justifyContent: "center",
  }



})

const mapStateToProps = (state) => {
  return {
    settings: state.intro.settings,
    filters: state.filters.filtersresponse,

    searchparams: state.filters.searchparams
    
  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    saveUser: params => dispatch(Actions.saveUser(params)),
    savefilers: params => dispatch(Actions.updateFilters(params)),
    updateSP: params => dispatch(Actions.updateSearchParams(params))

  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Main);