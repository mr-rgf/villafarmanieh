import React, {Component} from 'react';
import {View, StyleSheet, Image, Text} from 'react-native';
import {connect} from 'react-redux';
import * as Actions from '../../store/actions/allActions';
import * as Helpers from '../../helpers/Exporter';
import {WP as wp} from '../../helpers/Exporter';
import Api from '../../network/Api';
import Strings from '../../assets/string';

class Splash extends Component {

    async componentDidMount() {
        this.fetchData();
    }

    fetchData = async () => {
        this.props.setloading(true);
        let response = await Api.post('settings');
        if (response.success == true) {
            let dummycurrency = [];
            let dummyfeatures = response.data.final_content.features.option_dropdown;
            response.data.final_content.currency.option_dropdown.forEach((item, index) => {
                if (index != 0) {
                    dummycurrency.push({label: item.currency, value: item.currency_slug});
                } else {
                    response.data.final_content.currency.placeholder = {label: item.location};
                }
            });
            response.data.final_content.features.option_dropdown.forEach((item, index) => {
                dummyfeatures[index].checked = false;
            });
            response.data.final_content.currency.option_dropdown = dummycurrency;
            response.data.final_content.features.option_dropdown = dummyfeatures;
            this.props.savesettings(response.data.final_content);
            this.updatefilters(response.data.final_content);
            this.reset('searchlist');
        }
        this.props.setloading(false);
    };

    updatefilters = (data) => {
        let param = {
            selectedIndex: 0,


            subCategoryList: [],
            currencyTypes: [],
            locationsArray: data.location.option_dropdown,
            showsubcategory: false,
            selectedPropertyIndex: null,
            selectedSubPropertyIndex: null,
            selectedPropertyStatusIndex: null,

            subcategoryLoading: false,

            minprice: data.price.min_price,
            maxprice: data.price.max_price,
            minimumLowValue: data.price.min_price,
            maximumHighValue: data.price.max_price,

            minarea: data.area.min_area_unit,
            maxarea: data.area.max_area_unit,
            minimumLowValueArea: data.area.min_area_unit,
            maximumHighValueArea: data.area.max_area_unit,


            featureFilter: data.features.option_dropdown,

            selectedRoomId: 0,
            selectedBathId: 0,

            loadingLocations: false,
            currentCurreny: undefined,

            shownearme: false,

            searchtxt: '',
            minrange: 0,
            maxrange: 500,

            currentLocation: '',
        };

        this.props.savefilters(param);
    };

    navigate = (where) => {
        this.props.navigation.navigate(where);
    };

    reset = (route) => {
        this.props.navigation.reset({
            routes: [{name: route}],
        });
    };

    render() {
        return (
            <View style={styles.container}>

                <Image
                    source={Helpers.Images.splashLogo}
                    style={{height: wp(40), width: wp(40), resizeMode: 'contain'}}
                />

                <Text style={{fontSize:18,marginTop:wp(10)}}>{Strings.appname}</Text>
                <Text style={{marginTop:wp(5)}}>{Strings.slogan}</Text>
            


                <View style={{position: 'absolute', bottom: wp(30)}}>
                    {
                        this.props.loading ?
                            <Helpers.Lumper lumper={true} color={Helpers.Theme.blueBtnBg}/>
                            : []
                    }
                </View>

                <Image
                    source={Helpers.Images.splashBottom}
                    style={{height: wp(18), position: 'absolute', width: wp(100), bottom: 0, resizeMode: 'contain'}}
                />

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: Helpers.Theme.introBg,
        justifyContent: 'center',

    },

    topView: {
        height: Helpers.WP(10),
        width: '100%', justifyContent: 'center',
    },

    skipBtn: {
        alignSelf: 'flex-end',
        marginRight: Helpers.WP(4),
    },
    skipTxt: {
        color: Helpers.Theme.blueBtnBg,
        fontWeight: '500',

    },

    mainImg: {
        height: '56%', width: Helpers.WP(100),
        marginTop: Helpers.WP(5),
    },

    helpView: {
        marginTop: Helpers.WP(10),
    },
    helpTxt: {
        fontWeight: '700',
    },

    buttonsContainer: {
        width: '100%',
        justifyContent: 'center', flexDirection: 'row',
        paddingHorizontal: Helpers.WP(4),
        marginTop: Helpers.WP(5),
    },


    blueBtn: {
        height: Helpers.WP(10), width: Helpers.WP(43),
        backgroundColor: Helpers.Theme.blueBtnBg,
        alignItems: 'center', justifyContent: 'center',
        borderRadius: Helpers.WP(1),
        borderWidth: Helpers.WP(0.1), borderColor: Helpers.Theme.blueBtnBorder,
    },
    blueBtnTxt: {
        color: Helpers.Theme.light,
        fontWeight: '600',
    },
    marginLeft: {
        marginLeft: Helpers.WP(4),
    },
    marginTop: {
        marginTop: Helpers.WP(5),
    },

    whiteBtn: {
        height: Helpers.WP(12), width: Helpers.WP(90),
        backgroundColor: Helpers.Theme.light,
        borderWidth: Helpers.WP(0.1), borderColor: Helpers.Theme.blueBtnBorder,
        borderRadius: Helpers.WP(1),
        alignItems: 'center', justifyContent: 'center',
    },


});

const mapStateToProps = (state) => {
    return {
        loading: state.intro.loading,
    };

};
const mapDispatchToProps = (dispatch) => {
    return {
        savesettings: params => dispatch(Actions.saveSettings(params)),
        setloading: params => dispatch(Actions.loading(params)),
        savefilters: params => dispatch(Actions.updateFilters(params)),
    };
};
export default connect(mapStateToProps, mapDispatchToProps)(Splash);