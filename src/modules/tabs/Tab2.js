import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import { connect } from 'react-redux'
import * as Actions from '../../store/actions/allActions'
import * as Helpers from '../../helpers/Exporter'

class Tab2 extends Component {

  render() {
    return (
      <View style={styles.container}>
       
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor:'#d8d8d8'
  },
  
})

const mapStateToProps = (state) => {
  return {

  }

}
const mapDispatchToProps = (dispatch) => {
  return {
    saveUser: params => dispatch(Actions.saveUser(params))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Tab2);