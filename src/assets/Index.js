export const Images = {

    splashLogo: require('./images/splash.png'),
    splashBottom: require('./images/splashbottom.png'),


    appIntroHands: require('./images/appintro_hands.png'),
    appIntroBlueHome: require('./images/appintro_bluehome.png'),
    backArrow: require('./images/left_arrow.png'),
    downArrow: require('./images/down_arrow.png'),

    bathtub: require('./images/bathtub.png'),
    area: require('./images/area.png'),
    bed: require('./images/bed.png'),
    gear: require('./images/gear.png'),
    home: require('./images/home.png'),
    pinpoint: require('./images/pinpoint.png'),
    search: require('./images/search.png'),
    tag: require('./images/tag.png'),

    appartments: require('./images/appartments.png'),
    factory: require('./images/factory.png'),
    office: require('./images/office.png'),
    warehouse: require('./images/warehouse.png'),
    park: require('./images/park.png'),
    tick: require('./images/tick.png'),

    propertyaLogo: require('./images/logo-header3.png'),
    list: require('./images/list.png'),

    heartfilled: require('./images/heart_filled.png'),
    heartEmpty: require('./images/heart_empty.png'),

    dummylistItem_one: require('./images/dummylistitem_one.png'),
    dummylistItem_two: require('./images/dummylistitem_two.png'),
    dummylistItem_three: require('./images/dummylistitem_three.png'),


    bedsSearchList: require('./images/bed_searchlist.png'),
    bathsSearchList: require('./images/bathtub_searchlist.png'),
    featuredBadge: require('./images/featured_badge.png'),
    sqft: require('./images/sqft.png'),
    whatsapp: require('./images/whatsapp.png'),
    phonecall: require('./images/phonecall.png'),
    map: require('./images/map.png'),

    customMarker: require('./images/custommarker.png'),
    share: require('./images/shares.png'),
    shadow: require('./images/shadow.png'),
    bedDetail: require('./images/bedroom.png'),
    bathDetail: require('./images/bathroom.png'),
    garageDetail: require('./images/parking.png'),
    camera: require('./images/camera.png'),
    clock: require('./images/clock.png'),

    firealarm: require('./images/firealarm.png'),
    balcony: require('./images/balcony.png'),
    cooler: require('./images/cooler.png'),

    globe: require('./images/globe.png'),
    navigate: require('./images/navigate.png'),
    hotel: require('./images/hotel.png'),

    attachments: require('./images/attachments.png'),
    calculator: require('./images/calculator.png'),
    floorplan: require('./images/floorplan.png'),
    locationmap: require('./images/locationmap.png'),
    nearby: require('./images/nearby.png'),
    rating: require('./images/rating.png'),
    schedule: require('./images/schedule.png'),
    statistic: require('./images/statistic.png'),
    virtualtour: require('./images/virturaltour.png'),
    writereview: require('./images/writereview.png'),

    next: require('./images/next.png'),

    person: require('./images/person.jpeg'),
    mail: require('./images/mail.png'),
    yelp: require('./images/yelpicon.png'),

    filetypedoc: require('./images/filetypedoc.png'),
    filetypepdf: require('./images/filetypepdf.png'),
    filetypejpg: require('./images/filetypejpg.png'),
    cloud: require('./images/cloud.png'),

    floorplanimg: require('./images/floorplanimg.jpeg'),

    mortgagecal: require('./images/mortgagecal.png'),
    question: require('./images/question.png'),

    like: require('./images/like.png'),
    dislike: require('./images/dislike.png'),

    closeIcon: require('./images/close-button.png'),

    menu: require('./images/menu_2.png'),
    map_flag: require('./images/maps-and-flags.png'),
    area_select:require('./images/area.png'),
    instagram:require('./images/instagram.png'),
    handphone:require('./images/handphone.png'),

    modalLogo:require('./images/modaal.png'),

    callICon:require('./images/call_icon.png'),
    sortIcon:require('./images/sort_icon.png'),
    locationIcon:require('./images/location_icon.png'),
    instagramIcon:require('./images/instagram_icon.png'),
    ContactSupport:require('./images/support.png'),
    manager:require('./images/manager.png'),
    phone:require('./images/phone.png'),
    house_code:require('./images/house_code.png'),
    pieـchart:require('./images/pie-chart.png'),
    bed2:require('./images/bed2.png'),
    parking2:require('./images/parking2.png'),
    bathroom2:require('./images/bathroom2.png'),
    calendar_variant:require('./images/calendar_variant.png'),
    money2:require('./images/money2.png'),
    square:require('./images/square.png'),

};

