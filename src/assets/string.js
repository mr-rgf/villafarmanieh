const Strings = {
    address: 'مازندران، چمستان، ورازده علیا',
    filter: 'جستجو',
    sorte: 'مرتب سازی',
    map_address: 'آدرس نقشه',
    call: 'تماس تلفنی',
    whatsapp: 'پشتیبانی whatsapp',
    ContactSales:'تماس با واحد فروش',
    ContactSupport:'تماس با واحد پشتیبانی',
    ContactAdmin:'تماس با واحد مدیریت',
    instagram:'اینستاگرام فرمانیه',
    appname:'املاک فرمانیه',
    slogan:'خرید و فروش ویلا و زمین در شمال ایران'
};

export default Strings;
